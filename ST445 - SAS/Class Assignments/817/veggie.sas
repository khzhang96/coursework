/*This is the original code shown in the slides*/
*This is another way to comment;

/*To "run" this one piece of code, highlight it and then press the button of the little man 
running at the top of the program window*/

DATA WORK.Veg;
	*INFILE "C:/ST445/Lectures/Lecture 1/Data/veggies.txt";
	*INFILE "S:/Documents//Teaching/2016 Spring/ST445/Lectures/Lecture 1/Data/veggies.txt";
	*INFILE "//stat.ad.ncsu.edu/Redirect/jwstalli/Documents/Teaching/2016 Spring/ST445/Lectures/Lecture 1/Data/veggies.txt";
	*INFILE "//stat.ad.ncsu.edu/Redirect/jwstalli/Documents/Teaching/2016 Spring/ST445/
				Lectures/Lecture 1/Data/veggies.txt";
	*INFILE "/folders/myfolders/ST445/Lectures/Lecture 1/Data/veggies.txt";
	INPUT Food $ Code $ Days Number Price;
	*CostPerSeed=Price/Number;
RUN;
*Check the log once you have run the code;

/*One way to view the data is to go to the Explorer tab -> Libraries -> Work*/
/*Don't forget to close the data set after you have viewed it!  This is another reason why your code might not work*/

/******************************************************/

/* Here is the same code as before, but with a semicolon missing after WORK.Veg; */

DATA WORK.Veg;
	INFILE "S:/Documents/Teaching/2016 Spring/ST445/Lectures/Lecture 1/Data/veggies.txt";
	INPUT Food $ Code $ Days Number Price;
	*CostPerSeed=Price/Number;
RUN;

/*Note that it says that SAS stopped processing the step because of errors.  In fact, it never even executed the code*/


/*****************************************************/

/*Assuming I have all semicolons in place, I can do some pretty ugly things with the code and it will still run*/

dAtA Work.Veg; infile "S:/Documents/Teaching/2016 Spring/ST445/Lectures/Lecture 1/Data/veggies.txt"; INPUT fOOD $ code $ 
	DAYS numBer PRICE; 
	*CostPerSeed=Price/Number; 
Run;

/*Note that even though we changed how we named Price and Number the CostPerSeed assignment statement still works*/


/******************************************************/

/*Try to identify the naming violations*/

DATA WORK.VegetableStatisticsAndDataForYear2015;
	INFILE "S:/Documents/Teaching/2016 Spring/ST445/Lectures/Lecture 1/Data/veggies.txt";
	INPUT Food $ Code $ _Days_ Number Price($);
	CostPerSeed=Price/Number;
RUN;






