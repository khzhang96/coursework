libname ICE18 '\\tsclient\D\ST445\Class Assignments\ICE18\';

data StoreStats;
set 'sales.sas7bdat';

retain prev_sale;
call missing(prev_sale);
total = Sale + prev_sale;
output;
prev_sale = total;
run;

proc print data = StoreStats;
run;

