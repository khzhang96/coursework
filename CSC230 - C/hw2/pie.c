/**
@file pie.c
@author Kevin Zhang

This program prints a pie shape
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>

/** Max y value*/
#define MAX_Y 100
/** Max x value*/
#define MAX_X 100
/** Max colour*/
#define MAX_C 255

/** Center location*/
#define CENTER 49.5
/** radius for circle*/
#define RAD 45
/** border width*/
#define BORDER 3

/** max color*/
#define MAX 255
/** min color*/
#define MIN 0

/** fail status */
#define FAILST 100

/**
@param x x coord
@param y y corrd
@param rad radius
@return bool success

Checks if point is inside circle */
bool insideCircle( int x, int y, int rad ) {

    //distance between point and center
    double distance;
    distance = sqrt( pow( ( x - CENTER ), 2 ) + pow( ( y - CENTER ), 2 ) );
    
    //compares distance
    if ( distance < rad ) {
        return true;
    } else {
        return false;
    }
}

/**
@param x x coord
@param y y coord
@param color color
@param rAngle red Angle
@param gAngle green Angle

Checks if point is inside slice
*/
bool insideSlice( int x, int y, char color, double rAngle, double gAngle ) {
    //angle length of point to x axis
    double angle;
    if (atan2( y - CENTER, x - CENTER ) > 0 ) {
        angle = M_PI + atan2( y - CENTER, x - CENTER );
    } else {
        angle = -1 * atan2( y - CENTER, x - CENTER );
        angle = M_PI - angle;
    }

    if ( color == 'r' && angle <= rAngle ) {
        return true;
    } else if ( color == 'g' && angle > rAngle && angle <= gAngle) {
        return true;
    } else if ( color == 'b' && angle > gAngle && angle <= 2 * M_PI) {
        return true;
    } else {
        return false;
    }
}

/**
@return error status

Gets in three values and then draws a pie chart
*/
int main() {
    //first input
    double prop1 = 0;
    //second input
    double prop2 = 0;
    //third input
    double prop3 = 0;

    //checks for invalid input
    if ( !scanf("%lf", &prop1) ) {
        printf("Invalid input\n");
        return FAILST;
    }
    if (!scanf("%lf", &prop2)) {
        printf("Invalid input\n");
        return FAILST;
    }
    if (!scanf("%lf", &prop3)) {
        printf("Invalid input\n");
        return FAILST;
    }

    if ( prop1 < 0 || prop2 < 0 || prop3 < 0 ) {
        printf("Invalid input\n");
        return FAILST;
    }

    if ( prop1 == 0 && prop2 == 0 && prop3 == 0 ) {
        printf("Invalid input\n");
        return FAILST;
    }

    //red angle
    double rAngle = ( ( prop1 / ( prop1 + prop2 + prop3 ) ) ) * 2 * M_PI;
    //green angle
    double gAngle = ( ( ( prop1 + prop2 ) / ( prop1 + prop2 + prop3 ) ) ) * 2 * M_PI;

    printf("P3\n");
    printf("%d %d\n", MAX_Y, MAX_X);
    printf("%d\n", MAX_C);
    
    //traverses through the points
    for (int coordY = 0; coordY < MAX_Y; coordY ++ ) {
        for (int coordX = 0; coordX < MAX_X; coordX ++ ) {
            //checks if its within both the radius and border
            if ( insideCircle(coordX, coordY, RAD + BORDER) ) {
                //checks if its within the radius
                if ( insideCircle(coordX, coordY, RAD) ) {
                    //sees what color to color the pixel
                    if ( insideSlice(coordX, coordY, 'r', rAngle, gAngle ) ) {
                        printf("%3d %3d %3d ", MAX, MIN, MIN);
                    } else if ( insideSlice( coordX, coordY, 'g', rAngle, gAngle ) ) {
                        printf("%3d %3d %3d ", MIN, MAX, MIN);
                    } else {
                        printf("%3d %3d %3d ", MIN, MIN, MAX);
                    }
                } else {
                        printf("%3d %3d %3d ", MIN, MIN, MIN);
                }
            } else {
                printf("%3d %3d %3d ", MAX, MAX, MAX);
            }
        }
        printf("\n");
    }
    
    return 0;
}
