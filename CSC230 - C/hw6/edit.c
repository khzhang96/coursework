#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "document.h"
#include "edit.h"

/**
Pointless function that isn't being used
@param edit Edit to be destroyed
*/
void destoryEdit( Edit *edit ) {

}

/**
Inserts ch at location
@param row row num
@param col col num
@param new_ch character to be added
@param doc document
*/
void insertAt( int row, int col, int new_ch, Document *doc )
{

    char *newLine = (char *) malloc( doc->lines[row]->cap + doc->lines[row]->len + 1 );
    newLine[0] = '\0';

    strncpy( newLine, doc->lines[row]->text, col );
    newLine[col] = new_ch;
    strcpy( newLine + col + 1, doc->lines[row]->text + col );

    free( doc->lines[row]->text );

    doc->lines[row]->text = newLine;
    doc->lines[row]->len = strlen(newLine);

    doc->cRow = row;
    doc->cCol = col + 1;
}

/**
Remove ch at location
@param row row num
@param col col num
@param doc document
*/
void removeAt( int row, int col, Document *doc )
{

    char *newLine = (char *)malloc( doc->lines[row]->cap );
    newLine[0] = '\0';
    strncpy(newLine, doc->lines[row]->text, col);

    strcpy(newLine + col, doc->lines[row]->text + col + 1);

    free(doc->lines[row]->text);

    doc->lines[row]->text = newLine;
    doc->lines[row]->len = strlen(newLine);

    doc->cRow = row;
    doc->cCol = col;

}

/**
Deletes a line
@param doc Document
@param row rownum
*/
void deleteLineAt(Document *doc, int row)
{

    char *lineCur = doc->lines[row]->text;
    char *lineBef = doc->lines[row - 1]->text;

    char *lineNew = (char *) malloc( strlen(lineCur) + strlen(lineBef) );
    lineNew[0] = '\0';

    strcat( lineNew, lineBef );
    strcat( lineNew, lineCur );

    doc->cCol = strlen(lineBef);

    doc->lines[row - 1]->text = lineNew;
    doc->lines[row - 1]->len = strlen(lineNew);

    free(lineBef);

    removeLine(doc, row);
    doc->cRow--;
}

/**
adds a line
@param doc Document
@param row rownum
@param col column number
*/
void insertLineAt(Document *doc, int row, int col)
{

    char *lineCur = doc->lines[row]->text;
    char *lineNew = malloc( strlen(lineCur) - col + 1 );
    lineNew[0] = '\0';

    strcpy(lineNew, lineCur + col);
    doc->lines[row]->text[col] = '\0';
    doc->lines[row]->len = strlen(doc->lines[row]->text);

    insertLine(doc, row + 1, lineNew);

    doc->cRow++;
    doc->cCol = 0;

}

/**
Insert function
@param edit Edit struct
@param doc Document
*/
void insert(Edit *edit, Document *doc)
{

    if ( edit->ch != '\n' ) {
        insertAt( edit->row, edit->col, edit->ch, doc );
    } else {
        insertLineAt( doc, edit->row, edit->col );
    }
}

/**
Delete function
@param edit Edit struct
@param doc Document
*/
void deleteC (Edit *edit, Document *doc)
{

    int row = edit->row;
    int col = edit->col;

    if ( col != -1 ) {
        edit->ch = doc->lines[row]->text[col];
        removeAt( row, col, doc );
    } else {
        if ( row == 0 ) {
            return;
        } else {
            edit->ch = '\n';
            edit->row = row - 1;
            edit->col = strlen( doc->lines[row - 1]->text );
            deleteLineAt(doc, row);
        }
    }
}

/**
Undos an insert
@param edit Edit struct
@param doc Document
*/
void undoInsert(Edit *edit, Document *doc)
{
    int row = edit->row;
    int col = edit->col;
    if (edit->ch != '\n') {
        removeAt(row, col, doc);
    } else {
        // Handle new line
        // Merge line row+1 contents to line row
        // remove line row+1
        deleteLineAt(doc, row + 1 );
        //since it is deleting next line. We need to adjust doc->cRow back
        doc->cRow = row;
    }


}

/**
Undos a delete
@param edit Edit struct
@param doc Document
*/
void undoDelete(Edit *edit, Document *doc) {

    if ( edit->ch == '\n' ) {
        insertLineAt( doc, edit->row, edit->col );
    } else {
        insertAt( edit->row, edit->col, edit->ch, doc );
    }
}

/**
Frees the redo stack
@param hist History struct
*/
void clearRedoStack(History *hist) {

    for (int i = 0; i < hist->redo_len; i++) {
        if (hist->redo_stack[i] != NULL)
        {
            free(hist->redo_stack[i]);
        }
    }
    hist->redo_len = 0;
}

/**
Pushes edit to the undo stack
@param hist History struct
@param edit edit "object"
*/
void pushToUndo(History *hist, Edit *edit) {
    if (hist->undo_len < HIST_SIZE) {
        hist->undo_stack[hist->undo_len] = edit;
        hist->undo_len++;
    }
    else {
        for (int i = 0; i < HIST_SIZE - 1; i++)
        {
            hist->undo_stack[i] = hist->undo_stack[i + 1];
        }
        hist->undo_stack[HIST_SIZE - 1] = edit;
    }
}

/**
Pushes edit to redo stack
@param hist History struct
@param edit edit "object"
*/
void pushToRedo(History *hist, Edit *edit) {
    if (hist->redo_len < HIST_SIZE) {
        hist->redo_stack[hist->redo_len] = edit;
        hist->redo_len++;
    }
    else {
        for (int i = 0; i < HIST_SIZE - 1; i++)
        {
            hist->redo_stack[i] = hist->redo_stack[i + 1];
        }
        hist->redo_stack[HIST_SIZE - 1] = edit;
    }
}

/**
Pop up from the stack
@param hist History struct
@return edit Struct
*/
Edit *popupUndo(History *hist) {

    if (hist->undo_len > 0) {

        Edit *popupEdit = hist->undo_stack[hist->undo_len - 1];
        hist->undo_len--;
        pushToRedo(hist, popupEdit);

        return popupEdit;
    }
    return NULL;
}

/**
Pop up from stack
@param hist History stack
@return edit struct for specific edit
*/
Edit *popupRedo(History *hist) {

    if (hist->redo_len > 0) {

        Edit *popupEdit = hist->redo_stack[hist->redo_len - 1];
        hist->redo_len--;

        pushToUndo(hist, popupEdit);

        return popupEdit;
    }
    return NULL;
}

/**
    Make an edit operation to insert the given character into the document
    at the current cursor position.  This doesn't actually change the document,
    but it returns an object that can make the change or undo it.
    @param doc Document to insert a character in.
    @param ch Character to insert in the document.
    @return New, edit operation to insert the character.
 */
Edit *makeInsert(Document *doc, int ch) {

    Edit *edit = (Edit *)malloc(sizeof(Edit));
    edit->ch = ch; // save char
    edit->row = doc->cRow;
    edit->col = doc->cCol;

    edit->apply = insert;
    edit->undo = undoInsert;
    edit->destroy = destoryEdit;

    return edit;
}

/**
    Make an edit operation to delete the character before the current cursor
    position it the document.
    @param doc Document to modify with the edit operation.
    @return New, edit operation to delelete a character, or NULL if there's
            nothing before the current cursor position.
 */
Edit *makeDelete(Document *doc) {

    if ( doc->cRow == 0 && doc->cCol == 0 ) {
        return NULL;
    }
    Edit *edit = (Edit *)malloc(sizeof(Edit));
    edit->row = doc->cRow;
    edit->col = doc->cCol - 1;

    edit->apply = deleteC;
    edit->undo = undoDelete;
    edit->destroy = destoryEdit;

    return edit;
}

/**
    Initialize the given history to have empty undo and redo stack.
    @param hist The history to initialize.
 */
void initHistory(History *hist) {

    //initialize undo stack
    hist->undo_len = 0;
    hist->undo_stack = (Edit **)malloc(sizeof(Edit *) * HIST_SIZE * 1);

    // Init redo stack
    hist->redo_len = 0;
    hist->redo_stack = (Edit **)malloc(sizeof(Edit *) * HIST_SIZE * 1);

}

/**
    Discard and free any edits in the history.  This can be used
    at exit time to keep those edits from looking like memory leaks.
    @param hist The history to initialize.
 */
void clearHistory(History *hist) {
    if ( hist != NULL ) {
        if ( hist->undo_stack != NULL ) {
            free( hist->undo_stack );
        }

        if ( hist->redo_stack != NULL ) {
            free( hist->redo_stack );
        }
    }
}

/** Apply the given edit to the given document and add it to history
    of undoable operations.
    This will discard any recently undone edits on the undo stack of
    the history.
    @param hist The History this edit should be saved to, for undo support.
    @param doc The document the edit should be applied to.
    @param edit The the edit that should be applied to the given document.
 */
void applyEdit(History *hist, Document *doc, Edit *edit) {

    if ( edit == NULL ) {
        return;
    }

    clearRedoStack(hist);

    pushToUndo(hist, edit);
    edit->apply(edit, doc);

}
/** Undo the edit on the top of the undo stack, and push it to the redo stack.
    Return true if there was an edit to undo.
    @param hist the history containing the undo-able edit.
    @param doc the document we're supposed to undo the command in.
    @return true if there was an edit to undo
*/
bool undoEdit(History *hist, Document *doc) {

    if ( hist->undo_len > 0 ) {
        Edit *edit = popupUndo( hist );

        edit->undo( edit, doc );

        if ( edit->undo == undoInsert ) {
            edit->undo = undoDelete;
        } else {
            edit->undo = undoInsert;
        }
        return true;
    }
    return false;
}

/** Redo the edit on the top of the redo stack, and push it to the undo
    stack.
    Return true if there was an edit to redo.
    @param hist the history containing the redo-able edit.
    @param doc the document we're supposed to redo the command in.
    @return true if there was an edit to redo
*/
bool redoEdit(History *hist, Document *doc) {

    if ( hist->redo_len > 0 ) {
        Edit *edit = popupRedo(hist);

        edit->undo(edit, doc);
        if (edit->undo == undoInsert) {
            edit->undo = undoDelete;
        } else {
            edit->undo = undoInsert;
        }

        return true;
    }
    return false;
}
