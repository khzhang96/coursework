/**
@author Kevin Zhang
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    // Space for our words
    char word1[ 12 ];
    char word2[ 12 ];
    char word3[ 12 ];

    char *wordA[ 3 ]; //array of words
    char tempWord[ 12 ];

    

    //Read words from the user
    printf( "Enter three words (at most 10 characters each).\n");
    if (scanf("%11s", word1) != 1) {
        printf("Invalid Input");
        return 1;
    } else if (scanf("%11s", word2) != 1) {
        printf("Invalid Input");
        return 1;
    } else if (scanf("%11s", word3) != 1) {
        printf("Invalid Input");
        return 1;
    }

    wordA[0] = word1;
    wordA[1] = word2;
    wordA[2] = word3;

    //int status = scanf("%s%s%s", word1, word2, word3);

    for ( int counter1 = 1; counter1 < 3; counter1++ ) { //goes through the 3 words
        for ( int counter2=1; counter2 < 3; counter2++ ) { // compares the word to each of the other word
            if (strcmp(wordA[counter2 - 1], wordA[counter2]) > 0) {
                strcpy(tempWord, wordA[counter2 - 1]);
                strcpy(wordA[counter2 - 1], wordA[counter2]);
                strcpy(wordA[counter2], tempWord);
            }
        }
    }
    
    
    
    // ...

    // Then, print them out in sorted order.
    printf( "In sorted order: \n%s\n%s\n%s\n", word1, word2, word3 );

    return 0;
}



