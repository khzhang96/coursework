data info;
input name$ gender$ SBP DBP HR;
datalines;
Chris Male 150 90 76
Amy Female 130 80 60
Ben Male 140 90 70
Justin Male 124 86 72
Caitlin Female 120 70 64
Michelle Female 115 65 58
Kevin Male 135 80 65
;
run;
proc print data = info noobs;
title 'Medical Info';
run;

proc univariate data = info;
var SBP DBP HR;
title;
run;

mean med stdev min max

HR: 66.429 65 6.52 58 76
DBP: 80.142, 80, 9.66, 65, 90
SBP: 130.5714, 130.00, 12.1314, 115, 150

