/*
Author: Jonathan Duggins
Date: 2016-08-22
Purpose: Solution to ICE #2
*/

/*
1. (d) Permanent data sets must be stored in a 
       library other than WORK. This means the
       SET statement must include a reference
       to a library other than WORK (either 
       explicitly or implicitly).

2. They are global statements - this means they
   don't need to be part of a DATA or PROC step.

3. Alphabetical.
*/


*4;

*(a);
libname st445 "S:\Documents\CURRENT COURSES\ST 445\Lectures\Lecture 2\Data\";

*(b);
data blood_temp;
   set st445.blood;
   /*
   Remember - SET statements are for SAS data sets
            - Use your library to access the permanent data set
   */ 
run;

*(c);
proc contents data = blood_temp;
run;

/*
(i) 1000 observations and 7 variables
(ii) First: Gender, Last: Chol (not Cholesterol!)
(iii) AgeGroup, Char, 5, Age Group
*/

*(d);
*Use OPTIONS statement to suppress date and numbers;
options nodate nonumber;

*Ensure temporary data set is printed with titles;
*Note that while they don't have to be in a step, they can be.
   - It doesn't change that they are global though!;
proc print data = blood_temp;
   title1 'Blood Data';
   title2 'All Observations';
run;

*GPP - use a null TITLE statement to clear my previous titles;
title;
