/*
Author: Kevin Zhang
Date: 2016-12-2
Purpose: 
Graphing SG plot
*/

libname HW9 '\\tsclient\D\work\ST445\HW9\';
/*libname libMFish excel '\\tsclient\D\work\ST445\HW9\morefish.sas7bdat';*/


data HW9.moreFish;
	set moreFish;
run;

proc means data = HW9.moreFish;
run;

proc print data = HW9.moreFish;
run;

proc sgplot data = HW9.moreFish;
histogram hg;
run;

