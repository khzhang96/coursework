libname ICE10 "\\tsclient\D\ST445\Class Assignments\ICE10";

filename BloodV2 "\\tsclient\D\ST445\Class Assignments\ICE10\blood v2.txt";

data ICE10.BloodV2;
infile BloodV2 dsd;

input Subject Sex $ BloodGroup $ AgeGroup $ WBC RBS Chol;

run;

data ICE10.BloodV2;
rename WBC = RedBC;
rename RBC = WhiteBC;

keep AgeGroup, WBC;
where AgeGroup = 'Old' & WhiteBC < 6500;
run;

data Blood_AB;
set ICE10.BloodV2;
where Bloodgroup = 'AB';
run;

data Blood_A;
set ICE10.BloodV2;
where Bloodgroup = 'A';
run;

data Blood_B;
set ICE10.BloodV2;
where Bloodgroup = 'B';
run;

data Blood_O;
set ICE10.BloodV2;
where Bloodgroup = 'O';
run;


