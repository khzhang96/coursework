package edu.ncsu.csc316.security_manager.attacktree;

/**
 * @author khzhang
 *
 */
public class AttackNode {
    
    private double probability;

    private double cost;

    private double impact;

    private String description;

    private String type;
    
    AttackNodeList children;

    /**
     * @param type
     * @param probability
     * @param cost
     * @param impact
     * @param description
     */
    public AttackNode(String type, double probability, double cost, double impact, String description) {
        this.type = type;
        this.probability = probability;
        this.cost = cost;
        this.impact = impact;
        this.description = description;
        
        this.children = new AttackNodeList();
    }

    /**
     * @param type
     * @param description
     */
    public AttackNode(String type, String description) {
        this.type = type;
        this.description = description;

        this.children = new AttackNodeList();
    }

    /**
     *
     * @param element
     */
    public void addChild(AttackNode element) {
        this.children.add(element);
    }

    /**
     * @param cmp
     * @return
     */
    public boolean equals(AttackNode cmp) {

        if (this.probability == cmp.probability && this.cost == cmp.cost && this.impact == cmp.impact
                && this.description.equals(cmp.description) && this.type.equals(cmp.type)) {
            return true;
        }
        
        return false;
    }

    /**
     * @return
     *
     */
    public AttackNodeList getChildren() {
        return this.children;
    }
    
    /**
     * @return the type
     */
    public String getType() {
        return this.type;
    }

    public void propogate() {
        this.probability = this.propogateProbability();
        // this.cost = this.propogateCost();
        this.impact = this.propogateImpact();
    }

    /**
     * @return
     */
    public double propogateImpact() {
        if (this.children.getSize() == 0) {
            return this.impact;
        }
        double tempImpact = 0;

        if (this.getChildren().getList()[0].getType().equals("A")) {
            tempImpact = 1;
            for (int i = 0; i < this.children.getSize(); i++) {
                tempImpact *= (10 - this.children.getList()[i].propogateImpact());
            }
            
            tempImpact = (Math.pow(10, this.children.getSize()) - tempImpact)
                    / Math.pow(10, this.children.getSize() - 1);
            
        } else if (this.getChildren().getList()[0].getType().equals("O")) {
            tempImpact = Double.MIN_VALUE;
            for (int i = 0; i < this.children.getSize(); i++) {
                tempImpact = Math.max(tempImpact, this.children.getList()[i].propogateImpact());
            }

        }
        this.impact = tempImpact;
        return this.impact;
    }

    public double propogateProbability() {
        if (this.children.getSize() == 0) {
            return this.probability;
        }
        double tempProb = 1;
        if (this.getChildren().getList()[0].getType().equals("A")) {
            for (int i = 0; i < this.children.getSize(); i++) {
                tempProb *= this.children.getList()[i].propogateProbability();
            }
        } else if (this.getChildren().getList()[0].getType().equals("O")) {
            for (int i = 0; i < this.children.getSize(); i++) {
                tempProb *= (1 - this.children.getList()[i].propogateProbability());
            }
            tempProb = 1 - tempProb;
        }
        this.probability = tempProb;
        return this.probability;
    }
    
    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        if (this.type.equals("O")) {
            buf.append("OR");
        } else if (this.type.equals("A")) {
            buf.append("AND");
        } else if (this.type.equals("GOAL")) {
            buf.append("GOAL");
        }
        
        buf.append(" Step[");
        buf.append(this.description);
        buf.append(", C=");
        buf.append(this.cost);
        buf.append(", P=");
        buf.append(this.probability);
        buf.append(", I=");
        buf.append(this.impact);
        buf.append("]");
        
        return buf.toString();
        
    }
}
