/*
Author: Kevin Zhang
Date: 2016-11-21
Purpose: 
Uses arrays and loops
*/


libname HW8 '\\tsclient\D\work\ST445\HW8\';
filename rep1 '\\tsclient\D\work\ST445\workHW8\report1.rtf';
filename rep2 '\\tsclient\D\work\ST445\HW8\report2.rtf';
filename rep3 '\\tsclient\D\work\ST445\HW8\report3.pdf';
filename rep4 '\\tsclient\D\work\ST445\HW8\report4.rtf';
filename blood '\\tsclient\D\work\ST445\HW8\blood.sas7bdat';

data HW8.blooddata;
	set '\\tsclient\D\work\ST445\HW8\blood.sas7bdat';	 
run;
proc report data = HW8.blooddata(obs = 5) nowd;
	columns Subject WBC RBC;
	define Subject / display width = 7;
	define WBC / display width = 6;
	define RBC / display width = 5;
 
	ods rtf file = "\\tsclient\D\work\ST445\HW8\report1.rtf";
run;

proc report data = HW8.blooddata nowd;
	columns Gender WBC WBC=WBC1 RBC RBC=RBC1;
	define Gender / group;

	define WBC / analysis mean;

	define WBC1 / analysis median;

	define RBC / analysis mean;

	define RBC1 / analysis median;
	ods rtf file = "\\tsclient\D\work\ST445\HW8\report2.rtf";
run;

proc report data = HW8.blooddata nowd;
	columns Subject WBC RBC;
	define Subject / display width = 7;
	define WBC / display width = 6;
	define RBC / display width = 5;

	ods pdf file = "\\tsclient\D\work\ST445\HW8\report3.pdf";
run;
