#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

typedef enum { first, last, total } Rule;

int select1( Rule rule, ... ) {
    va_list ap;

    va_start( ap, rule );
    int i = 0;
    int total = 0;
    int last = 0;

    i = va_arg( ap, int );
    //printf("%d", rule);
    if ( rule == 0 ) {
        va_end( ap );
        return i;
    } else if ( rule == 1 ) {
        while ( i != -1 ) {
            last = i;
            i = va_arg( ap, int );
            //printf("%d", i);
        }

        va_end( ap );
        //printf("\n");
        return last;

    } else if ( rule == 2 ) {
        while ( i != -1 ) {
            total += i;
            i = va_arg( ap, int );
            //printf("%d", i);
        }

        va_end( ap );
        return total;

    } else {
        printf("---------");
    }

}

int main( int argc, char *argv[] )
{
    int v;

    // Don't change anything in main.  I'll probably use a script to make small changes in the
    // following calls (to make sure your funciton works even with different parameters)

    // First round of tests.

    // Select the first value in a list.
    v = select1( first, 27, 18, 92, 7, 3, 48, 7, -1 );
    printf( "First: %d\n", v );

    // Select the last value in a list.
    v = select1( last, 27, 18, 92, 7, 3, 48, 7, -1 );
    printf( "Last: %d\n", v );

    // Compute the total
    v = select1( total, 27, 18, 92, 7, 3, 48, 7, -1 );
    printf( "Total: %d\n", v );

    // Another round of tests

    // Select the first value in a list.
    v = select1( first, 3, 19, 42, 71, 6, 45, 11, 2, 15, 92, 22, -1 );
    printf( "First: %d\n", v );

    // Select the last value in a list.
    v = select1( last, 3, 19, 42, 71, 6, 45, 11, 2, 15, 92, 22, -1 );
    printf( "Last: %d\n", v );

    // Compute the total
    v = select1( total, 3, 19, 42, 71, 6, 45, 11, 2, 15, 92, 22, -1 );
    printf( "Total: %d\n", v );

    return EXIT_SUCCESS;
}