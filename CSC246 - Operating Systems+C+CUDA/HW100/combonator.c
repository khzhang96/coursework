
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include "combonator.h"

/** Interface for the jazz combo monitor.  If I was programming in
    C++, this would all be wrapped up in a class.  Since we're using
    C, it's just a collection of functions, with an initialization
    function to init the state of the whole monitor and a destroy
    function to free its resources. */

/** Initialize the monitor. */

typedef struct StageList
{
    char *name1;
    char *name2;
    char *name3;

    int len;

}StageList;

struct StageList *stage;

pthread_cond_t cond1;
//static pthread_cond_t *cond2;
//static pthread_cond_t *listFull;
pthread_mutex_t duplicate;

//gettimeofday
struct timeval t0;
struct timeval t1;
struct timeval first;

long zeroTime;
long oneTime;
long twoTime;
long threeTime;

long interval;




/*Initializes combonator monitor*/
void initCombonator() {
    
    zeroTime = 0;
    oneTime = 0;
    twoTime = 0;
    threeTime = 0;
    

    stage = malloc(sizeof(struct StageList));

    pthread_cond_init(&cond1, NULL);
    //pthread_cond_init(&cond2, NULL);
    //pthread_cond_init(&listFull, NULL);
    pthread_mutex_init(&duplicate, NULL);

    stage->len = 0;
    gettimeofday(&t0, 0);
}

/*prints the instruments on stage*/
void printStage() {
    if (stage->len == 0) {
        printf("Now playing:\n");
    } else if (stage->len == 1) {
        printf("Now playing: %s\n", stage->name1);
    } else if (stage->len == 2) {
        printf("Now playing: %s %s\n", stage->name1, stage->name2);
    } else if (stage->len == 3) {
        printf("Now playing: %s %s %s\n", stage->name1, stage->name2, stage->name3);
    }
}

/** Destroy the monitor, freeing any resources it uses.  When the
    monitor is destroyed, it gvies a summary of how long each type of
    ensemble was playing (either silence, a solo, a duet or a
    trio). */
void destroyCombonator() {
    printStage();
    gettimeofday(&t1, 0);
    if (stage->len == 0) {
        interval = (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
        printf("%ld ms\n", interval);
        zeroTime += (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
    } else if (stage->len == 1) {
        interval = (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
        printf("%ld ms\n", interval);
        oneTime += (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
    } else if (stage->len == 2) {
        interval = (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
        printf("%ld ms\n", interval);
        twoTime += (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
    } else if (stage->len == 3) {
        interval = (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
        printf("%ld ms\n", interval);
        threeTime += (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
    }
    pthread_mutex_destroy(&duplicate);
    pthread_cond_destroy(&cond1);

    printf("Summary\nsilence:\t%ld ms\nsolo:\t%ld ms\nduet: %ld ms\ntrio:\t%ld ms\n", zeroTime, oneTime, twoTime, threeTime);
    //printf("%ld %ld %ld %ld\n", zeroTime, oneTime, twoTime, threeTime);
}

/*checks if instrument has duplicate already on stage*/
int hasDuplicate(char *instrument) {
    int ret = 0;
    if (stage->len == 0) {
        ret = 0;
    } else if (stage->len == 1) {
        if (strcmp(stage->name1, instrument) == 0) {
            ret = 1;
        } else {
            ret = 0;
        }
    } else if (stage->len == 2) {
        if (strcmp(stage->name1, instrument) == 0
        || strcmp(stage->name2, instrument) == 0) {
            ret = 1;
        } else {
            ret = 0;
        }
    } else if (stage->len == 3) {
        if ((strcmp(stage->name1, instrument) == 0)
        || (strcmp(stage->name2, instrument) == 0)
        || (strcmp(stage->name3, instrument) == 0)) {
            ret = 1;
        } else {
            ret = 0;
        }
    }

    return ret;
    
}

/** Called by a thre
 * ad when it wants to start playing.  If the stage
    is full (already has three musicians) or if there's already
    another thread playing the given instrument, then the calling
    thread will wait in this function until it can take the stage. */
void startPlaying( char const *instrument ) {
    char *instrument2 = malloc(100 * sizeof(instrument));
    strcpy(instrument2, instrument);
    
    pthread_mutex_lock(&duplicate);
    
    while (hasDuplicate(instrument2) != 0 || stage->len == 3) {
        pthread_cond_wait(&cond1, &duplicate);
    } //no duplicates
    gettimeofday(&t1, 0);
    printStage();
    if (stage->len == 0) {
        interval = (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
        printf("%ld ms\n", interval);
        zeroTime += (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
    } else if (stage->len == 1) {
        interval = (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
        printf("%ld ms\n", interval);
        oneTime += (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
    } else if (stage->len == 2) {
        interval = (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
        printf("%ld ms\n", interval);
        twoTime += (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
    } else if (stage->len == 3) {
       
    }
    //printStage();
    //printf("Entering: %s\n", instrument2);
    if (stage->len == 0) {
        stage->name1 = instrument2;
    } else if (stage->len == 1) {
        stage->name2 = instrument2;
    } else if (stage->len == 2) {
        stage->name3 = instrument2;
    } else if (stage->len == 3) {
        stage->name3 = instrument2;
    } else {
        printf("Hello. I shouldn't be here\n");
    }
    stage->len++;
    
    
    
    gettimeofday(&t0, 0);
    pthread_mutex_unlock(&duplicate);
    
}

/** Called by a thread when it wants to stop playing and leave the stage. */
void stopPlaying( char const *instrument ) {
    char *instrument2 = malloc(100 * sizeof(instrument));\
    strcpy(instrument2, instrument);
    
    //printStage();
    pthread_mutex_lock(&duplicate);
    //printStage();
    //printf("Leaving: %s\n", instrument2);
    printStage();
    gettimeofday(&t1, 0);
    if (stage->len == 0) {
        
    } else if (stage->len == 1) {
        interval = (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
        printf("%ld ms\n", interval);
        oneTime += (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
    } else if (stage->len == 2) {
        interval = (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
        printf("%ld ms\n", interval);
        twoTime += (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
    } else if (stage->len == 3) {
        interval = (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
        printf("%ld ms\n", interval);
        threeTime += (t1.tv_sec-t0.tv_sec)*1000LL + (t1.tv_usec-t0.tv_usec)/1000;
    }
    if (stage->len == 0) {
        printf("Nothing here\n");
    } else if (stage->len == 1) {
        if (strcmp(stage->name1, instrument2) == 0) {
            stage->name1 = NULL;
        } else {
            stage->len++;
        }
    } else if (stage->len == 2) {
        if (strcmp(stage->name1,instrument2) == 0) {
            strcpy(stage->name1,stage->name2);
            stage->name2= NULL;
        } else if (strcmp(stage->name2,instrument2) == 0) {
            stage->name2 = NULL;
        } else {
            stage->len++;
        }        
    } else if (stage->len == 3) {
        if (strcmp(stage->name1,instrument2) == 0) {
            strcpy(stage->name1,stage->name2);
            strcpy(stage->name2,stage->name3);
            stage->name3 = NULL;
        } else if (strcmp(stage->name2,instrument2) == 0) {
            strcpy(stage->name2,stage->name3);
            stage->name3 = NULL;
        } else if (strcmp(stage->name3,instrument2) == 0) {
            stage->name3 = NULL;
        } else {
            stage->len++;
        }
    }
    stage->len--;
    
    gettimeofday(&t0, 0);
    pthread_cond_broadcast(&cond1);
    pthread_mutex_unlock(&duplicate);
    
    
    
}

