/**
@file dent.c
@author Kevin Zhang
Corrects the indents of text to coding style
*/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

/** Error code for failiure*/
#define FAILST 100

/**
Indents code

@param number of tabs
*/
void indent( int d )
{
    for ( int tabs = 0; tabs < d; tabs++ ) {
            printf( "  " );
    }
}

/**
Checks if its a space
@param character value to check
*/
bool isASpace( char ch )
{
    if ( ch == ' ' || ch == '\t') {
        return true;
    } else {
        return false;
    }
}

/**
main method for program
@return int for error code
*/
int main()
{
    //character for getchar
    char ch;
    //number of tabs
    int tabnum = 0;
    
    ch = getchar();

    //removes spaces
    while ( isASpace( ch ) ) { //gets rid of opening spaces, newlines, and tabs
        ch = getchar();
    }
    
    //checks if character is EOF
    while ( ch != EOF && tabnum >= 0) {
        //checks each possible character
        if (ch == '{') {
            printf("{");
            tabnum++;

            if ( ( ch = getchar() ) == EOF) {
                break;
            }
        } else if (ch == '}') {
            tabnum--;
            if (tabnum < 0) {
                break;
            }
            printf("}");

            if ( ( ch = getchar() ) == EOF) {
                break;
            }
        } else if ( ch == '\n' ) {
            printf("\n");
            
            //gets rid of opening spaces, and tabs
            while ( isASpace( ch = getchar() ) ) {
            }

            //checks for the next nonwhitespace character that isn't brackets
            if (ch == '}') {
                tabnum--;
                indent(tabnum);
                if (tabnum < 0) {
                    break;
                }
                printf("}");
                if ( ( ch = getchar()  ) == EOF) {
                    break;
                }
            } else if (ch == '{') {
                indent(tabnum);
                tabnum++;
                printf("%c", ch);
                if ( ( ch = getchar()  ) == EOF) {
                    break;
                }

            } else if ( ch == '\n' ) {
                printf("\n");
                indent(tabnum);
                while ( isASpace( ch = getchar() ) ) { //gets rid of opening spaces, and tabs
                
                }
            } else  { //if its a character
                if ( ( ch  ) == EOF) {
                    break;
                }
                indent(tabnum);
                printf("%c", ch);
                ch = getchar();
            }
        } else {
            if ( ( ch  ) == EOF) {
                break;
            }

            printf("%c", ch);
            ch = getchar();
        }
    }

    //checks tabnum at the end
    if (tabnum != 0) {
        printf("Unmatched brackets\n");
        return FAILST;
    } else {
        return 0;
    }
    
}
