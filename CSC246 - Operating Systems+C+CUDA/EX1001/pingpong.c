#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>

// Number of iterations for each thread
#define ITERATIONS 500

// Declare two anonymous semaphores, one to let each of the threads
// go next.

sem_t pingNext;
sem_t pongNext;



void *pingThread(void *vargp) {
  for ( int i = 0; i < ITERATIONS; i++ ) {
    sem_wait(&pongNext); //wait til pongnext is free
    //wait until value of semaphore s is greater than 0 decrement the value of semaphore s by 1
    printf("ping\n");
    sem_post(&pingNext);  //pingnext is free
    //increment the value of semaphore s by 1 if there are 1 or more threads waiting, wake 1
  }

  return NULL;
}

void *pongThread(void *vargp) {
  for ( int i = 0; i < ITERATIONS; i++ ) {
    sem_wait(&pingNext); //wait til pingNext is free
    printf("pong\n");
    sem_post(&pongNext); //pongNext is free
  }

  return NULL;
}

// Print out an error message and exit.
static void fail( char const *message ) {
  fprintf( stderr, "%s\n", message );
  exit( 1 );
}

// Start routines for the two threads.

// ...

int main( int argc, char *argv[] ) {

  // Create two semephaores, one to let ping go next and one to let
  // pong go next.

  sem_init(&pingNext, 0, 1); 
  sem_init(&pongNext, 0, 0); 
  // ...

  // Create each of the two threads.

  pthread_t tid; 

  //before

  pthread_create(&tid, NULL, pingThread, NULL);
  pthread_create(&tid, NULL, pongThread, NULL);



  // ...

  // Wait for them both to finish.

  pthread_join(tid, NULL);

  return 0;

  // Destroy the two semaphores.

  sem_destroy(&pingNext); 
  sem_destroy(&pongNext); 


  return 0;
}