/**
@author Kevin zhang
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>

#include "bits.h"
#include "codes.h"

#define ARGNUM 3
#define EXITFLAG 31

/**
*@param argc num of args
*@param *argv[] args
*/
int main( int argc, char *argv[] ) {
    FILE *fpRead = fopen(argv[1], "rb");
    FILE *fpWrite = fopen(argv[2], "w");

    if ( argc != ARGNUM ) {
        fprintf( stderr, "usage: unsqueeze <infile> <outfile>\n");
        exit(EXIT_FAILURE);
    } else if ( fpRead == NULL ) {
        fprintf( stderr, "%s, No such file or directory\n", argv[1]);
        exit(EXIT_FAILURE);
    }
    BitBuffer *buffer = malloc( sizeof(BitBuffer) );
    buffer->bits = 0;
    buffer->bcount = 0;

    int i;
    buffer->bits = 0;
    buffer->bcount = 0;
    i = read5Bits( buffer, fpRead );
    if ( i == 0x1 ) {

        //fread(&buffer->bits, 1, 1, fpRead)
        do {
            i = read5Bits( buffer, fpRead );
            if ( i != -1 ) {
                if ( i == EXITFLAG ) { //check for escape code
                    i = read8Bits( buffer, fpRead ); //read 8 more
                    if ( i != -1 ) {
                        fputc( i, fpWrite ); //print
                    }
                } else {
                    fputc( ( codeToSym( i ) ), fpWrite );
                }
            }
        } while ( i != -1 );
    } else if ( i == 0x0 ) {
        //fread(&buffer->bits, 1, 1, fpRead)
        do {
            i = read8Bits( buffer, fpRead );
            if (i != -1) {
                fputc( i, fpWrite ); //print
            }

        } while ( i != -1 );
    } else {
        fprintf( stderr, "Invalid compressed format\n" );
        exit(EXIT_FAILURE);
    }

    fclose(fpRead);
    fclose(fpWrite);
    return EXIT_SUCCESS;

}
