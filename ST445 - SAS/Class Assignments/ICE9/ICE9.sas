libname ICE9 "\\tsclient\D\ST445\Class Assignments\ICE9";

filename BloodV2 "\\tsclient\D\ST445\Class Assignments\ICE9\blood v2.txt";

data ICE9.BloodV2;
infile BloodV2 dsd;

input Subject Sex $ BloodGroup $ AgeGroup $ WBC RBS Chol;

run;

proc print data=ICE9.BloodV2 (firstobs = 2);
	by Sex descending _ALL_;
run;
