#include <stdio.h>
#include "print.h"

// You'll need to add a global variable.
    int spaceCount = 0;
// ...

// Print the given character, counting spaces as we print and
// replacing them with '-'. The parameter type is char here, since we
// don't expect EOF to be passed to the function, just legal
// characters.
void printChar( char ch)
{
    if ( ch == ' ' ) {
        printf("-");
        spaceCount++;
    } else {
        printf("%d", ch);
    }
}
