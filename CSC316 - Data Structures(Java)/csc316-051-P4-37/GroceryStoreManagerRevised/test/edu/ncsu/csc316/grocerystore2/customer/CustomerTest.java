package edu.ncsu.csc316.grocerystore2.customer;

import org.junit.Assert;
import org.junit.Test;

import edu.ncsu.csc316.grocerystore2.customer.Customer;

/**
 * Customer test class
 *
 * @author khzhang
 *
 */
public class CustomerTest {
    
    /**
     * ID field
     */
    public String id;
    /**
     * company field
     */
    public String company;
    /**
     * state field
     */
    public String state;
    /**
     * zipcode field
     */
    public String zipcode;
    
    /**
     * Test 1
     */
    @Test
    public void test() {
        
        this.id = "C01";
        this.company = "EEKO";
        this.state = "HK";
        this.zipcode = "00000";
        
        Customer testCust = new Customer(this.id, this.company, this.state, this.zipcode);
        
        Assert.assertEquals(this.id, testCust.getId());
        Assert.assertEquals(this.company, testCust.getCompany());
        Assert.assertEquals(this.state, testCust.getState());
        Assert.assertEquals(this.zipcode, testCust.getZipcode());
        
        this.id = "C02";
        this.company = "EEK0";
        this.state = "NC";
        this.zipcode = "11111";
        testCust.setId(this.id);
        testCust.setCompany(this.company);
        testCust.setState(this.state);
        testCust.setZipcode(this.zipcode);
        
        testCust.toString();
        
        Assert.assertEquals(this.id, testCust.getId());
        Assert.assertEquals(this.company, testCust.getCompany());
        Assert.assertEquals(this.state, testCust.getState());
        Assert.assertEquals(this.zipcode, testCust.getZipcode());
    }
    
}
