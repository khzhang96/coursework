#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stdio.h>

using namespace std;

// Read words from cin into the given vector of strings.
void readWords( vector< string > &words )
{
    string str;
    while (cin >> str) {
        words.push_back( str );
    }
}


// Use the index operator ( you know, [] ) to print out the list
// of words in reverse.
void printBackward( vector<string> &words )
{
  cout << "-- Backward --" << endl;

  for ( auto p = words.end(); p != words.begin(); p-- ) {
      cout << *(p - 1) << endl;
  }
}




// Use iteratros to print the list forward.
void printForward( vector< string > &words )
{
    cout << "-- Forward --" << endl;
    for ( auto p = words.begin(); p != words.end(); p++ ) {
        cout << *p << endl;
    }
}


void alphaBetize( vector<string> &words ) {

    /*for (i = 0; i < words.size(); i++) {
        for (j = 0; j < words.size(); j ++ ) {
            if ( strcmp(words[i], words[j]) < 0) {
                string temp = words[i];
                words[i] = words[j];
                words[j] = temp;
            }
        }
    }*/
    sort(words.begin(), words.end());
}

int main()
{
    // You can make a vector of strings.  Why not.
    vector<string> words;
    //words.push_back("abc");
    //words.push_back("def");
    //cout << words[0] << endl;


    //vector< string >::interator p;

    // Read a list of words.
    readWords( words );

    // Use a template algorithm to sort them.

    alphaBetize( words );

    // Print the list backward.
    printBackward( words );
    // Then print it again, forward this time.
    printForward( words );

}