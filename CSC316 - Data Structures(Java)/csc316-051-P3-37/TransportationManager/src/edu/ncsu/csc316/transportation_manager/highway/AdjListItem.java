package edu.ncsu.csc316.transportation_manager.highway;

import edu.ncsu.csc316.transportation_manager.list.HighwayList;

/**
 * Adj list item
 *
 * @author khzhang
 *
 */
public class AdjListItem {
    int city;
    
    HighwayList highwayList;

    /**
     * Constructor for each item
     *
     * @param element
     *            highway key
     * @param cityNum
     *            whether to assign to city1 or 2
     */
    public AdjListItem(Highway element, int cityNum) {
        this.highwayList = new HighwayList();
        if (cityNum == 1) {
            this.city = element.getCity1();
        } else {
            this.city = element.getCity2();
        }
        this.highwayList.add(element);
    }

    /**
     * Gets the city
     *
     * @return the city
     */
    public int getCity1() {
        return this.city;
    }
    
    /**
     * Gets the highway list
     *
     * @return the highwayList
     */
    public HighwayList getHighwayList() {
        return this.highwayList;
    }
    
    /**
     * Returns a string representation of the AdjacencyList
     * in the following format, where (for each city) the Highways are
     * in sorted order by city1, then city2, then cost, then asphalt:
     *
     *
     * City 0: -> Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] ->
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] -> Highway[city1=X, city2=X,
     * cost=X.X, asphalt=X.X]
     *
     *
     * @return the string representation of the AdjacencyLists
     */
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();

        buf.append("City ");
        buf.append(this.city);
        buf.append(":");

        for (int i = 0; i < this.highwayList.getSize(); i++) {
            buf.append(" -> ");
            buf.append(this.highwayList.get(i).toString());
        }
        return buf.toString();
    }
    
}
