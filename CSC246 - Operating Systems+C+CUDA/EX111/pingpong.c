#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

// Number of iterations for each thread
#define ITERATIONS 500

// Who gets to go next.
int nextTurn = 0;

// Print out an error message and exit.
/*static void fail( char const *message ) {
  fprintf( stderr, "%s\n", message );
  exit( 1 );
}*/

// Start routines for the two threads.

void *pingThread(void *vargp) {
  for ( int i = 0; i < ITERATIONS; i++ ) {
    while ( nextTurn != 0 );
    nextTurn = 1;
    printf("ping\n");
  }

  return NULL;
}

void *pongThread(void *vargp) {
  for ( int i = 0; i < ITERATIONS; i++ ) {
    while ( nextTurn != 1 );
    nextTurn = 0;
    printf("pong\n");
  }

  return NULL;
}

int main( int argc, char *argv[] ) {

  // Create each of the two threads.

  pthread_t tid; 

  //before

  pthread_create(&tid, NULL, pingThread, NULL);
  pthread_create(&tid, NULL, pongThread, NULL);



  // ...

  // Wait for them both to finish.

  pthread_join(tid, NULL);

  return 0;
}
