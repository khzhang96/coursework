#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/**struct for time*/
typedef struct TimeTag {
    /*hour*/
    int hour;
    /*minute*/
    int minute;
} Time;

/**struct for activity*/
typedef struct ActivityTag {
    /*title/description of activity*/
    char *title;
    /*leader of activity*/
    char *leader;
    /*startTime of activity*/
    Time *startTime;
    /*endTime of activity*/
    Time *endTime;
    /*ID of activity*/
    int ID;
} Activity;


/*empty function for reading an activity*/
Activity *readActivity() {
    /*test activity*/
    Activity *act;
    act = NULL;
    return act;
}

/*frees activities*/
void freeActivity(Activity *act) {
    free( act->title );
    free( act->leader );
    free( act->startTime );
    free( act->endTime );
    free( act );
}
