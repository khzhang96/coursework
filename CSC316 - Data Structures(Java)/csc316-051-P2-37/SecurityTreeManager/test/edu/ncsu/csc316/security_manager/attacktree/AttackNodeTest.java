package edu.ncsu.csc316.security_manager.attacktree;

import org.junit.Test;

public class AttackNodeTest {

    @Test
    public void test() {
        AttackNode goalNode = new AttackNode("GOAL", "Use DDoS Attack to Disrupt All Users");
        AttackNode methodNode = new AttackNode("O", "Attack Servers");
        AttackNode probNode = new AttackNode("O", .5, 6, 5000, "\"RENT\" existing BOTNET");

        goalNode.addChild(methodNode);
        methodNode.addChild(probNode);
    }

}
