package edu.ncsu.csc316.security_manager.manager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import edu.ncsu.csc316.security_manager.attacktree.AttackNode;
import edu.ncsu.csc316.security_manager.attacktree.AttackNodeList;
import edu.ncsu.csc316.security_manager.attacktree.AttackTree;

public class SecurityTreeManager {
    
    AttackNodeList pre;
    AttackNodeList post;
    
    AttackTree tree;

    /**
     * Constructs a new SecurityTreeManager object with the given path
     * to the file that contains the log entries.
     *
     * @param filePath
     *            - the path to the log entry file
     */
    public SecurityTreeManager(String filePath) {
    }

    /**
     * Constructs a new SecurityTreeManager object with the given paths
     * to the preOrder and postOrder traversal files.
     *
     * @param preOrder
     *            - the path to the preOrder traversal file
     * @param postOrder
     *            - the path to the postOrder traversal file
     */
    public SecurityTreeManager(String preOrder, String postOrder) {

        this.pre = this.StringReader(preOrder);
        this.post = this.StringReader(postOrder);
    }

    /**
     * Returns the level order traversal of the Attack Tree
     * as a string in the format (where each "node" is indented 3 spaces):
     *
     * LevelOrder[
     * GOAL Step[Use DDoS Attack to Disrupt All Users, C=0.00, P=0.000, I=0.00]
     * OR Step[Attack Servers, C=0.00, P=0.000, I=0.00]
     * OR Step[Attack Comm Infrastructure, C=0.00, P=0.000, I=0.00]
     * ]
     *
     * THE LEVEL ORDER TRAVERSAL MUST NOT RETURN ANY OF THE PROPAGATED VALUES!
     * Why? So you can earn credit for having a correct traversal,
     * even if you have incorrect functions for propagating values.
     *
     * @return the level order traversal (as a string) of the attack tree
     */
    public String getAttackTreeLevelOrder() {
        this.tree = new AttackTree(this.pre, this.post);
        return this.tree.levelOrder();
    }

    /**
     * Returns (as a string, sorted in increasing order) the log entries
     * for the given date in the format:
     *
     * LogEntry[timestamp=2015/07/17 15:49:38, user=user4, description=print
     * calendar]
     * LogEntry[timestamp=2015/07/17 15:55:25, user=user8, description=save
     * immunizations]
     *
     * @param date
     *            - the date, in the format MM-DD-YYYY
     * @return the string representation of the log entries for the specified date
     */
    public String getLogEntriesForDate(String date) {
        return null;
    }

    /**
     * Return the level order traversal of the Log Tree
     * as a string in the format (where each "node" is indented 3 spaces):
     *
     * LevelOrder[
     * LogEntry[timestamp=2015/09/13 02:58:49, user=user2, description=save patient
     * list]
     * LogEntry[timestamp=2012/12/18 16:25:58, user=user18, description=view
     * diagnoses]
     * LogEntry[timestamp=2016/12/12 06:28:13, user=user6, description=edit patient
     * representative list]
     * ]
     *
     * @return the level order traversal (as a string) of the log tree
     */
    public String getLogTreeLevelOrder() {
        return null;
    }

    /**
     * Returns the values (as a string) propagated to the root node
     * using the formulas from the project writeup.
     * For example:
     * GOAL Step[Use DDoS Attack to Disrupt All Users, C=21557.12, P=0.878, I=8.00]
     *
     * @return the metric values (as a string) that are propagated to the root node
     */
    public String propagateValues() {
        this.tree.propogate();
        return "";

    }

    public AttackNodeList StringReader(String path) {
        BufferedReader buf;
        StringBuilder strBuf;
        AttackNodeList list = new AttackNodeList();
        try {
            buf = new BufferedReader(new FileReader(path));
            String line; // new string line
            String[] lines; // line array that will be delimited
            String type; // brand field
            double prob;
            double impact;
            double cost;
            String description; // brand description

            while ((line = buf.readLine()) != null) { // checks if line is null
                strBuf = new StringBuilder(); // creates string builder
                lines = line.split(" "); // split lines by space

                type = lines[0]; // assigns first entry to brand

                if (lines[1].matches("-?\\d+(\\.\\d+)?")) {
                    prob = Double.parseDouble(lines[1]);
                    cost = Double.parseDouble(lines[2]);
                    impact = Double.parseDouble(lines[3]);
                    for (int i = 4; i < lines.length; i++) { // appends all other words into description
                        strBuf.append(lines[i]);
                        strBuf.append(" ");
                    }
                    description = strBuf.toString().trim(); // returns description
                    list.add(new AttackNode(type, prob, cost, impact, description));
                } else {
                    for (int i = 1; i < lines.length; i++) { // appends all other words into description
                        strBuf.append(lines[i]);
                        strBuf.append(" ");
                    }
                    description = strBuf.toString().trim(); // returns description
                    list.add(new AttackNode(type, description));
                }

            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) { // catches IO exception
            e.printStackTrace();
        }

        return list;
    }
}
