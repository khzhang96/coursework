proc import out=houses datafile='houses.xls' dbms=xls replace;
getnames=yes;
run;

proc print data=houses (obs=5);
run;

proc univariate data=houses plot;
var sqft;
histogram sqft;
title 'Histogram of Square Footage';
run;

proc univariate data=houses;
var sqrt;
probplot sqft/normal (Mu=Est SIgma=Est);
qqplot sqft/normal (Mu=Est SIgma=Est);
title;
run;

proc univariate data=houses freq;
var baths;
run;


proc univariate data=houses freq;
var baths;
run;

proc means data=houses;
run;

proc sort data=houses; *sort data so can use BY statement;
by location;
run;

proc univariate data-houses;
var price;
histogram price;
by location;
run;

proc means data=houses median mean;
var price;
by location; *data must be sorted;
run;

*alternatively, use class statement so data doesn't have to be sorted;
proc means data=houses median mean;
class location;
var price;
run;

quit;


***ADDITIONAL PROBLEM;
proc import out=dose datafile= 'C:\Users\stat\Documents\ST307\datasets\dosedata.xls' dbms=xls replace; 
getnames=yes;
run;

proc print data=dose (obs=5); 
run;

proc sort data=dose;
by dose;
run;

proc univariate data=dose plot;
var liver spleen;
histogram liver spleen;
run;

proc means data=dose;
var liver spleen;
run;

proc univariate data=dose plot;
class dose;
var liver spleen;
histogram liver spleen;
run;

proc means data=dose;
class dose;
var liver spleen;
run;

quit;
        
