####################################
##R Class - ST 308 - Activity05
##Name: Kevin Zhang
####################################
setwd("D:\\work\\ST308")
#Create an R script that will do the following
#1.  Read in the California Health Information Survey (CHIS.csv) (available on web)
chris = read.csv(file = "CHIS.csv")
#2.  Determine which values of Height are greater than 75
attach(chris)
greater = subset(chris, Height > 75)
#3.  Find the proportion of values that are greater than 75
prop = nrow(greater)/nrow(chris)
#4.  Determine which values are either greater than 75 or less than or equal to 60
greater2 = subset(chris, Height > 75 || Height <= 60)
#5.  Determine which values are both greater than 65 and less then 68
greater2 = subset(chris, Height > 65 && Height < 68)
#6.  Create an indexing vector corresponding to the Asian variable being 1
index1 = which(Asian == 1)
#7.  Create 2 histograms (in 1 window) of the height variable.  One histogram of the 
#Asian (Asian variable equal to 1) heights and one of the non-Asian heights.
#you sould have proper titles and labels
index2 = which(Asian != 1)
par(mfrow = c(1,2))

hist(index1)
hist(index2)
#8. For the 2nd person in this study, if their Height value is less than 60
#print "Small."  If their Height is less than 70 print "Medium", otherwise, print "Tall"

inc = 1
while (inc <= length(Height)) {
  if (Height[inc] < 60) {
    print("Small")
  } else if (Height[inc] < 70) {
    print("Medium")
  } else {
    print("Tall")
  }
}


#8.  Create a new vector (called Condition) that is "At-Risk" if Height is less than 60 and Weight is greater than 170
# set the value to "OK" otherwise

condition = Height
inc = 1
while (inc <= length(condition)) {
  inc = inc + 1
  if (Height[inc] < 60 && Weight[inc] > 170) {
    condition[inc] = "At-Risk"
  } else {
    condition[inc] = "ok"
  }
}


#####Save this script and upload it to the wolfware page