proc import out=wot datafile='responses.xls'
dbms=xls replace;
getnames=yes;
run;

/*money hours stats percentage enjoyment*/

proc gplot data=wot;
plot percentage*stats;
run;

proc corr data=wot;
var percentage stats;
run;

proc reg data=wot;
model percentage=stats;
plot residual.*stats; /*linearity*/
plot residual.*predicted.; /*constant variance*/
plot nqq.*residual.; /*normality*/
run;

proc gplot data=wot;
plot hours*enjoyment;
run;

proc corr data=wot;
var hours enjoyment;
run;

proc reg data=wot;
model hours=enjoyment;
plot residual.*enjoyment; /*linearity*/
plot residual.*predicted.; /*constant variance*/
plot nqq.*residual.; /*normality*/
run;
