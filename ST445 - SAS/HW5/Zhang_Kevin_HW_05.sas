libname HW4 '\\tsclient\D\ST445\HW4\';

libname HW5 '\\tsclient\D\ST445\HW5\';

data HW5.Air_Emps_Raises;

	set HW4.Air_Emps_Full;
	
	New_Salary = CurrentSalary;
	difference = today() - year(1991);

	if difference > 35 * 365.25 then
		if substr(empID, 5, 1) = 3 or substr(empID, 5, 1) = 2 then 
			New_Salary = New_Salary * 1.025;
		else if substr(empID, 5, 1) = 1 then 
			New_Salary = New_Salary * 1.015;
		else 
			New_Salary = New_Salary * 1.02;

		New_Salary = New_Salary + 3500;

	if 35 * 365.25 >= difference > 25 * 365.25 then
		if substr(empID, 5, 1) = 3 or substr(empID, 5, 1) = 2 then 
			New_Salary = New_Salary * 1.02;
		else if substr(empID, 5, 1) = 1 then 
			New_Salary = New_Salary * 1.01;
		else 
			New_Salary = New_Salary * 1.0175;
		
		New_Salary = New_Salary + 2000;

	if difference <= 25 * 365.25 then
		if substr(empID, 5, 1) = 3 or substr(empID, 5, 1) = 2 then 
			New_Salary = New_Salary * 1.025;
		else if substr(empID, 5, 1) = 1 then 
			New_Salary = New_Salary * 1.0125;
		else 
			New_Salary = New_Salary * 1.0075; 

		New_Salary = New_Salary + 2000;

	rename CurrentSalary = Old_Salary;

	if New_Salary > 35000 then delete;
run;

proc sort data = HW5.Air_Emps_Raises;
	by empID;
run;




