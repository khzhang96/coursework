#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>

// Maximum number of songs on the playlist.
#define LIST_MAX 10

// Maximum length of the name of a song.
#define SONG_MAX 30

// Representation for a list of songs.
struct Playlist {
  // List of all the titles on the playlist.
  char title[ LIST_MAX ][ SONG_MAX ];
  
  int size;
  // Add more fieds as needed.
  // ...
};
 
struct Playlist *pList;

// Print out an error message and exit.
static void fail( char const *message ) {
  fprintf( stderr, "%s\n", message );
  exit( 1 );
}



void addFront(char *str) {

  //char *str2 = NULL;
  if (pList->size == 0) {
    strcpy(pList->title[0], str);
    pList->size++;
  } else if (pList->size == 1) {
    strcpy(pList->title[1], str);
    pList->size++;
  } else if (pList->size == 2) {
    strcpy(pList->title[2], pList->title[1]);
    strcpy(pList->title[1], str);
    pList->size++;
  } else if (pList->size < 30) {
    for (int i = pList->size /*max index*/; i > 1; i--) {
      //pList->title[i] = pList->title[i - 1];
      strcpy(pList->title[i], pList->title[i - 1]);
    }
    pList->size++;
    strcpy(pList->title[1], str);
    
  } else {
    printf("error: list full\n");
  }
  

}

void addBack(char *str) {
  if (pList->size != 0) {
    if ( pList->size < 30 ) {
      strcpy(pList->title[pList->size],str);
      pList->size++;
    } else {
      printf("error: list full\n");
    }
  } else {
    addFront(str);
  }
  
  
}

void removeBack() {
  if (pList->size != 0) {
    pList->size--;
  } else {
    printf("error: list empty\n");
  }
  

}

void removeFront() {
  if (pList->size != 0) {
    for(int i = 0; i < pList->size; i++) {
      strcpy(pList->title[i], pList->title[i + 1]);
    }
    pList->size--;
  } else {
    printf("error: list empty\n");
  }
  
}

void returnList() {
  if (pList->size == 0) {
    printf("error: list empty\n");
  } else {
    for (int i =0; i < pList->size; i++) {
      printf("%s\n", pList->title[i]);
    }
  }
  

  printf("size: %d\n", pList->size);
}

void returnPlaying() {
  printf("%s\n", pList->title[0]);
}

void addNext(char *str) {
  if (pList->size == 0) {
    strcpy(pList->title[0], str);
    pList->size++;

  } else if (pList->size < 30) {
    
    for (int i = pList->size - 1; i > 1; i--) {
      strcpy(pList->title[i + 1],pList->title[i]);
    }

    pList->size++;

    strcpy(pList->title[1],str);


    printf("%d\n", pList->size);
    
  } else {
    printf("too big\n");
  }
}

void test() {
  addNext("Hello");
  addNext("Hello2");
  addNext("Hello3");
  addNext("Hello4");
  addNext("Hello5");

  
}

int main( int argc, char *argv[] ) {

  key_t key = 1;

  if (argc < 1) {
    fail("error");
  }
  int shm_id = shmget(key, sizeof(struct Playlist), IPC_CREAT | 0666 );
  pList = (struct Playlist *) shmat(shm_id, NULL, IPC_CREAT | 0666);
  //fd = shm_open(key, O_CREAT|O_RDWR, 0666);
  //ftruncate (fd, sizeof(struct Playlist));

  //pList = mmap(NULL, sizeof(struct Playlist), PROT_READ | PROT_WRITE, MAP_SHARED,fd, 0);
  char *fullstring = NULL;
  if (argc > 2) {
    fullstring = malloc(1000);
    strcpy(fullstring, argv[2]);
    if (argc > 3) {
      for (int i = 3; i< argc; i++) {
        strcat(fullstring, ' ');
        strcat(fullstring, argv[i]);
      }
    }
  }
  
  
  
  if (strcmp(argv[1],"next") == 0) {
    addFront(fullstring);
  } else if (strcmp(argv[1],"last") == 0) {
    addBack(fullstring);
  } else if (strcmp(argv[1],"forward") == 0) {
    removeFront();
  } else if (strcmp(argv[1],"playing") == 0) {
    returnPlaying();
  } else if (strcmp(argv[1],"list") == 0) {
    returnList();
  } else {
    fail("error");
  }

  //test();


  return 0;
}
