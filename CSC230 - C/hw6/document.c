#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "document.h"

/** max text per line*/
#define MAX_TEXT_PER_LINE 35
/** max lines per document*/
#define MAX_LINE_PER_DOC 20

/** Create a dynamically allocated line containing the next line read
    from the given file, containing the text of the line, but not the newline
    at the end of the line.
    @param fp File to read the line from.
    @return The new line read fromt he file. */
Line *loadLine(FILE *fp) {
    Line *newLine = (Line*) malloc( sizeof(Line) );

    newLine->len = 0;
    newLine->cap = MAX_TEXT_PER_LINE;

    newLine->text = malloc( newLine->cap );
    char c;

    while ( ( c = fgetc(fp) ) != EOF  ) {
        if (c != '\n') {
            if ( newLine->len >= newLine->cap ) {
                newLine->text = realloc(newLine->text, 2 * newLine->cap + newLine->len + 1);
                newLine->cap = 2 * newLine->cap + newLine->len + 1;
            }
            newLine->text[newLine->len++] = c;

        } else  {
            if ( newLine->len >= newLine->cap ) {
                newLine->text = realloc(newLine->text, 2 * newLine->cap + newLine->len + 1);
                newLine->cap = 2 * newLine->cap + newLine->len + 1;
            }
            newLine->text[newLine->len] = '\0';
            return newLine;
        }
    }

    free(newLine);
    return NULL;
}

/** Free the memory used to store the given line.
    @param line Line to free. */
void freeLine(Line *line) {
    //printf("freeLine\n");

    if ( line != NULL )
    {
        if ( line->text != NULL ) {
            free( line->text );
        }
        free(line);
    }
}

/** Dynamically allocate a document and initialize it with the lines of the
    file with the given name.
    @param filename Name of the file to read into the document.
    @return dynamically allocated document containing a copy of the
    text in the given file. */
Document *loadDocument( const char *filename ) {
    //printf("loadDocument\n");

    FILE *fp = fopen(filename, "r");

    if (fp == NULL)
    {
        perror("file.txt: No such file or directory");
        exit(EXIT_FAILURE);
    }

    Document *doc = (Document *)malloc( sizeof(Document) );
    doc->len = 0;
    doc->cap = MAX_LINE_PER_DOC;
    doc->cRow = 0;
    doc->cCol = 0;

    doc->lines = (Line **)malloc( sizeof(Line *) * MAX_LINE_PER_DOC );

    Line *line;
    while ((line = loadLine(fp)) != NULL) {
        if ( doc->len + 1 >= doc->cap ) {

        doc->cap = 2 * doc->len + 1;

        Line** new_lines = (Line **) malloc( sizeof(Line *) * doc->cap );

        for (int i=0; i < doc->len; i++) {
            Line *perLine = (Line*) malloc( sizeof(Line) );
            perLine->len = doc->lines[i]->len;
            perLine->cap = doc->lines[i]->cap;
            perLine->text = (char*) malloc(strlen(doc->lines[i]->text)+1);
            strcpy(perLine->text, doc->lines[i]->text);

            new_lines[i] = perLine;

            freeLine(doc->lines[i]);
        }

        doc->lines = new_lines;
    }
        doc->lines[doc->len++] = line;
        //doc->len = doc->len + 1;
    }
    fclose(fp);

    return doc;
}

/**
Inserts a line
@param row row num
@param text string
*/
void insertLine(Document *doc, int row, char *text) {
    Line *new_line = (Line *)malloc( sizeof(Line) );

    new_line->len = strlen(text);
    new_line->cap = MAX_TEXT_PER_LINE;
    new_line->text = text;

    if ( doc->len + 1 >= doc->cap ) {

        doc->cap = 2 * doc->len + 1;

        Line** new_lines = (Line **) malloc(sizeof(Line *) * doc->cap );

        for (int i=0; i < doc->len; i++) {
            Line *perLine = (Line*) malloc( sizeof(Line) );
            perLine->len = doc->lines[i]->len;
            perLine->cap = doc->lines[i]->cap;
            perLine->text = (char*) malloc( strlen(doc->lines[i]->text)+1 );
            strcpy( perLine->text, doc->lines[i]->text );

            new_lines[i] = perLine;

            freeLine(doc->lines[i]);
        }

        doc->lines = new_lines;
    }

    for ( int i= doc->len - 1; i>=row; i-- ) {
        doc->lines[i+1] =doc->lines[i];
    }

    doc->lines[row] = new_line;

    doc->len++;
}

/**
removes a line
@param doc Document
@param row row number
*/
void removeLine( Document* doc, int row ) {
    freeLine( doc->lines[row] );
    
    for ( int i=row; i< doc->len-1; i++ ) {
        doc->lines[i] = doc->lines[i+1];
    }

    doc->len--;
}

/** Save the given document to a file with the given name.
    @param doc Document to save.
    @param dir Filename to save it to.
    @return true if successful. */
bool saveDocument( Document *doc, const char *filename ) {
    //printf("saveDocument: %s\n", filename);

    FILE* fp = fopen ( filename, "w" );
    if (fp == NULL) {
        perror("cannot write to file");
        return false;
    }

    for ( int i=0; i< doc->len; i++ ) {
        Line* curLine = doc->lines[i];
        fputs(curLine->text, fp);
        fputs("\n", fp);
    }

    fclose(fp);
    return true;
}

/** If possible, move the cursor in doc in the indicated direction.
    @param doc Document in which to move the cursor.
    @param dir Direction to move the currsor.
    @return true if the cursor can be moved in the requested direction. */
bool moveCursor (Document *doc, CursorDir dir ) {
    if ( dir == CursorUp ) {
        if (doc->cRow > 0) {
            doc->cRow--;
            if (doc->cCol > doc->lines[doc->cRow]->len) {
                doc->cCol = doc->lines[doc->cRow]->len;
            }
            return true;
        }
        else {
            return false;
        }
    }
    else if ( dir == CursorDown ) {
        if (doc->cRow < doc->len - 1) {
            doc->cRow++;
            if (doc->cCol > doc->lines[doc->cRow]->len) {
                doc->cCol = doc->lines[doc->cRow]->len;
            }
            return true;
        }
        else {
            return false;
        }
    }
    else if ( dir == CursorRight ) {
        if (doc->cCol < doc->lines[doc->cRow]->len) {
            doc->cCol++;
            return true;
        }
        else {
            return false;
        }
    }
    else if ( dir == CursorLeft ) {
        if ( doc->cCol > 0 ) {
            doc->cCol--;
            return true;
        }
        else {
            return false;
        }
    }
    // Should not happen
    return false;
}

/** Free all the memory for the given document, including freeing all the
    Line structures it contains.
    @param doc Document to free. */
void freeDocument(Document *doc) {
    //printf("freeLine\n");

    for ( int i = 0; i < doc->len; i++ ) {
        Line *curLine = doc->lines[i];
        freeLine(curLine);
    }

    free(doc->lines);
}
