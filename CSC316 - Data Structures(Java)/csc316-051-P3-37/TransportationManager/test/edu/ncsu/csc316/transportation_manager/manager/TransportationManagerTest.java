package edu.ncsu.csc316.transportation_manager.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class
 *
 * @author khzhang
 *
 */
public class TransportationManagerTest {

    TransportationManager mgr;

    /**
     * pretest items
     */
    @Before
    public void beforeTest() {
        this.mgr = new TransportationManager("highways.txt");

    }

    /**
     * Tests get adjancy list
     */
    @Test
    public void testGetAdjacencyList() {
        
        System.out.println(this.mgr.getAdjacencyList());
        Assert.assertTrue(true);
    }
    
    /**
     * Tests get min heaps
     */
    @Test
    public void testGetMinHeaps() {
        
        System.out.println("Cost heap:");
        System.out.println(this.mgr.getMinHeaps("COST"));
        System.out.println("Asphalt heap:");
        System.out.println(this.mgr.getMinHeaps("ASPHALT"));

        Assert.assertTrue(true);
    }
    
    /**
     * Tests get min highways
     */
    @Test
    public void testGetMinimumHighways() {
        System.out.println("Cost min");
        System.out.println(this.mgr.getMinimumHighways("COST"));
        System.out.println("Asphalt min");
        System.out.println(this.mgr.getMinimumHighways("ASPHALT"));
        
        Assert.assertTrue(true);
    }

}
