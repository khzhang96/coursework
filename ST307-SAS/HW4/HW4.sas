proc import out=htwtdata datafile='htwtdata.xls' dbms=xls replace;
getnames=yes;
run;

data htwtdata2;
set htwtdata;
BMI = weight / ((height * .01) * (height * .01));
if BMI < 18.5 then BMIstat = 'Underweight' ;
else if BMI <= 25 then BMIstat = 'Normal' ;
else if BMI <= 30 then BMIstat = 'Overweight' ;
else if BMI > 30 then BMIstat = 'Obese' ;
run;

proc print data=htwtdata2;
run;

proc freq data=htwtdata2;
tables gender*BMIstat;
run;

proc gplot data=htwtdata2;
plot height*weight;
run;


proc gplot
plot height*weight/haxis=axis1 vaxis=axis2;
run;


