/**
 *
 */
package edu.ncsu.csc316.grocerystore2.customer;

/**
 * Customer Class
 *
 * @author khzhang
 *
 */
public class Customer {
    /**
     * Customer Company
     */
    private String company;
    /**
     * Customer ID number
     */
    private String id;
    /**
     * Customer State
     */
    private String state;
    /**
     * Customer zipcode
     */
    private String zipcode;

    /**
     * Constructor for Customer Class
     *
     * @param id
     *            Customer ID Number
     * @param company
     *            Customer Company
     * @param state
     *            Customer State
     * @param zipcode
     *            Customer zipcde
     */
    public Customer(String id, String company, String state, String zipcode) {
        this.id = id.trim(); // trims the customer id
        this.company = company.trim(); // trims the company
        this.state = state.trim(); // trims the state
        this.zipcode = zipcode.trim(); // trims the zipcode
    }

    /**
     * Gets the company
     *
     * @return the company
     */
    public String getCompany() {
        return this.company; // gets the company
    }

    /**
     * Gets the id
     *
     * @return the id
     */
    public String getId() {
        return this.id; // gets the id
    }

    /**
     * Gets the state
     *
     * @return the state
     */
    public String getState() {
        return this.state; // gets the state
    }

    /**
     * Gets the zip
     *
     * @return the zipcode
     */
    public String getZipcode() {
        return this.zipcode; // gets the zipcode
    }

    /**
     * Sets the company
     *
     * @param company
     *            the company to set
     */
    public void setCompany(String company) {
        this.company = company; // sets the company
    }

    /**
     * set the ID
     *
     * @param id
     *            the id to set
     */
    public void setId(String id) {
        this.id = id; // sets the id
    }

    /**
     * set the state
     *
     * @param state
     *            the state to set
     */
    public void setState(String state) {
        this.state = state; // sets the state
    }

    /**
     * set the zip
     *
     * @param zipcode
     *            the zipcode to set
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode; // sets the zipcode
    }
    
    @Override
    /**
     * Translates Customer to a String to be printed
     */
    public String toString() {

        StringBuffer newBuf = new StringBuffer(); // creates a string buffer as appending a string buffer is less
                                                  // resource heavy

        newBuf.append("Customer [id=");
        newBuf.append(this.getId());
        newBuf.append(", company=");
        newBuf.append(this.getCompany());
        newBuf.append(", state=");
        newBuf.append(this.getState());
        newBuf.append(", zipcode=");
        newBuf.append(this.getZipcode());
        newBuf.append("]\n");

        return newBuf.toString();
    }
}
