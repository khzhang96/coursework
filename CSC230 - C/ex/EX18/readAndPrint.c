// Exercise to read a sequence of doubles stored in binary format from
// a file, and then write the cosine of each to a file (in text)

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main( int argc, char *argv[] )
{
    // Open the the input file in binary mode
    FILE *src = fopen( argv[ 1 ], "rb");

    // Open the the output file in text mode
    FILE *dest = fopen( argv[ 2 ], "w");

    if ( argc < 2 ) {
        exit(1);
    }

    // Read double values from the inupt file using fread().  Use an array
    // that's large enough to hold 100 values, and use the return value
    // of fread to tell how many you successfully read.
    int counter = 0;

    //double *values2 = malloc( 100 * sizeof(double) );
    double values[100];
    //double *ptr = &values[0];
    

    while ( !feof(src) && ( counter < 100 ) ) {
        counter += fread(&values[counter], sizeof(double), 1, src);
        //values++;
        //ptr++;

        //printf("%lf\n", ptr[0]);
    }
  

  


    // Loop over the list of values and print the cosine of each
    // to the output file, one value per line with 4 fractional digits
    // of precision.
    // ...
    
    double temp = 0;
    for ( int countOut = 0; countOut < counter; countOut++ ) {
        fprintf(dest, "%.4lf\n", cos(values[countOut]));        
        temp = cos(values[countOut]);
        values[countOut] = temp;       
    }
    //fwrite(values, sizeof(double), counter, dest);
    // Close both of the files.
    fclose(src);
    fclose(dest);

    return 0;
}
