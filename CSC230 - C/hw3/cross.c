#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

char *words[100001];
int wordCount = 0;

void readWords( char const *filename ) {
    FILE *fp = fopen( filename, "r" );
    
    if (!fp) {
        printf("Can't open word file");
    }
    char *strW;
    
    strW = (char *) malloc(22 * sizeof ( char ) );
    *words = malloc(22 * sizeof( char ) );
    
    
    //words = (char *) malloc(100000);
    //wordCount = 0;
    //rewind(fp);
    //fgetc(fp);
    fflush(stdout);
    //fscanf( fp, "%s", strW );
    while ( ( fscanf( fp, "%s", strW ) != -1 ) && wordCount < 100000 ) {
        //printf("here");
        strW = (char *) malloc(22 * sizeof ( char ) );
        words[wordCount] = strW;

        wordCount++;
        if ( wordCount > 100000 ) {
            printf("Invalid word file");
            exit(1);
        }
    }
    
    /*printf("%d\n", wordCount);
    for (int j = 0; j < wordCount; j++ ) {
        printf("%s\n", words[j]);
    }*/

    //free(strW);
    //fclose(fp);
}

bool getPattern( char *pat ) {
    
    if ( (strlen(pat)) > 20 ) {
        printf("Invalid Pattern");
        return false;
    } 

    for (int i = 0; i < strlen(pat); i++ ) { //i to length of pattern
        if  ((pat[i] != 'a') &&
            (pat[i] != 'b') &&
            (pat[i] != 'c') &&
            (pat[i] != 'd') &&
            (pat[i] != 'e') &&
            (pat[i] != 'f') &&
            (pat[i] != 'g') &&
            (pat[i] != 'h') &&
            (pat[i] != 'i') &&
            (pat[i] != 'j') &&
            (pat[i] != 'k') &&
            (pat[i] != 'l') &&
            (pat[i] != 'm') &&
            (pat[i] != 'n') &&
            (pat[i] != 'o') &&
            (pat[i] != 'p') &&
            (pat[i] != 'q') &&
            (pat[i] != 'r') &&
            (pat[i] != 's') &&
            (pat[i] != 't') &&
            (pat[i] != 'u') &&
            (pat[i] != 'v') &&
            (pat[i] != 'w') &&
            (pat[i] != 'x') &&
            (pat[i] != 'y') &&
            (pat[i] != 'z') &&
            (pat[i] != '?')) {
                printf("Invalid Pattern\n"); //checks if element isn't an approved char
                return false;
        } 
        
        
    }
    return true;
}

bool matches( char const *word, char const *pat ) {
    //printf("%s %d %s %d\n", word, strlen(word), pat, strlen(pat));
    if (strlen(pat) == strlen(word)) { //make sure sizes/lengths are equal
        for (int i = 0; i < strlen(word); i++) {
            if ( word[i] != pat[i] ) {
                if ( pat[i] != '?') {
                    return false;
                } 
            } 
        }
        return true;
    }
    return false;
}

int main(int argc, char *argv[]) {
    if ( argc != 2) {
        printf("Usage: cross <word-file>\n");
        exit(1);
    }
    readWords(&*argv[1]);

    

    
    //printf("pattern>");
    int status;
    char *pat;
    while ( true ) {
        pat = (char *) malloc (22 * sizeof ( char ) );
        printf("pattern>");
        while( ( status = scanf("%s", pat) ) != 1 || getPattern(pat) == 0 ) {
            //pat = (char *) malloc (22 * sizeof ( char ) );
            //*words = malloc(22 * sizeof( char ) );
            //if ( *pat == EOF ) {
                //exit(1);
            //}
            //printf("Invalid Pattern\n");
            printf("pattern>");
            
        }
        

        /*printf("WordCount: %d\n", wordCount);
        for (int j = 0; j < wordCount; j++ ) {
            printf("%s\n", words[j]);
        }*/
        //printf("done\n");
        //*words = malloc(22 * sizeof( char ) );

        //printf("%d\n", wordCount);
        
        for ( int i = 0; i < wordCount - 1; i++ ) {
            if (matches(words[i], &*pat)) {
                //printf("%s %s\n", words[i], pat);
                printf("%s\n", words[i]);
            }
        }        
    }
    free(pat);

} 


