package edu.ncsu.csc316.transportation_manager.heap;

import edu.ncsu.csc316.transportation_manager.highway.Highway;

/**
 * Min Highway heap
 *
 * @author khzhang
 *
 */
public class MinHighwayHeap {
    /**
     * default cap
     */
    public static final int DEF_CAP = 1;
    
    /** size of list */
    int size;
    /** capacity of list */
    int capacity = MinHighwayHeap.DEF_CAP;

    int isCost;

    /** List to store highway */
    Highway[] highwayList;

    /**
     * Constructs a new Highway heap
     *
     * @param type
     *            the type of weight to consider ("COST" or "ASPHALT") when
     *            operating on the heap
     */
    public MinHighwayHeap(String type) {
        if (type.equals("COST")) {
            System.out.println("Cost");
            this.isCost = 1;
        } else if (type.equals("ASPHALT")) {
            System.out.println("Asphalt");
            this.isCost = 0;
        } else {
            this.isCost = 1;
        }

        this.highwayList = new Highway[MinHighwayHeap.DEF_CAP];
        this.size = 0;

    }
    
    /**
     * Returns the Highway with minimum weight in the minheap
     *
     * @return the Highway with minimum weight in the minheap
     */
    public Highway deleteMin() {
        if (this.size <= 0) {
            return new Highway(-1, -1, Integer.MAX_VALUE, Integer.MAX_VALUE);
        }
        switch (this.size) {
            case 1:
                this.size--;
                return this.highwayList[0];
            default:
                break;
        }

        return this.deleteMinHelper();
        
    }
    
    /**
     * Helper method for deletemin
     *
     * @return highway to be deleted
     */
    public Highway deleteMinHelper() {
        Highway root = this.highwayList[0];

        this.highwayList[0] = this.highwayList[this.size - 1];
        this.size--;
        if (this.isCost == 1) {
            this.heapifyCost(0);
            System.out.println("Cost remove " + root.toString());
        } else if (this.isCost == 0) {
            this.heapifyAsphalt(0);
            System.out.println("Asphalt remove " + root.toString());
        }
        return root;
    }

    /**
     * ensures list has enough capacity
     */
    public void ensureCapacity() { // sets the capacity of the current list so there is no index out of bounds or
        // seg fault in the array
        if (this.size + 1 >= this.capacity) { // tests size of current array
            Highway[] old = this.highwayList; // creates pointer for current list

            this.capacity = this.capacity + this.capacity * 2; // sets new capacity
            Highway[] newArray = new Highway[this.capacity]; // creates new array with larger capacity

            for (int i = 0; i < this.size; i++) {
                newArray[i] = old[i];
            }

            this.highwayList = newArray;
        }
    }
    
    /**
     * bubble down method
     *
     * @param i
     *            start
     */
    public void heapifyAsphalt(int i) {
        int smallest = i;
        
        switch ((2 * i + 1 < this.size && this.highwayList[2 * i + 1].getAsphalt() < this.highwayList[i].getAsphalt())
                ? 1
                : 0) {
            case 1:
                smallest = 2 * i + 1;
                break;
            default:
                break;
        }

        switch ((2 * i + 2 < this.size
                && this.highwayList[2 * i + 2].getAsphalt() < this.highwayList[smallest].getAsphalt()) ? 1 : 0) {
            case 1:
                smallest = 2 * i + 2;
                break;
            default:
                break;

        }
        
        this.heapifyAsphaltHelper(smallest, i);
        
    }

    /**
     * Healper method for heapify asphalt
     *
     * @param smallest
     *            index
     * @param i
     *            index
     */
    public void heapifyAsphaltHelper(int smallest, int i) {
        switch ((smallest != i) ? 1 : 0) {
            case 1:
                Highway temp = this.highwayList[i];
                this.highwayList[i] = this.highwayList[smallest];
                this.highwayList[smallest] = temp;

                this.heapifyAsphalt(smallest);
                break;
            default:
                break;
        }
    }

    /**
     * bubble down method
     *
     * @param i
     *            start
     */
    public void heapifyCost(int i) {
        int smallest = i;

        switch ((2 * i + 1 < this.size && this.highwayList[2 * i + 1].getCost() < this.highwayList[i].getCost()) ? 1
                : 0) {
            case 1:
                smallest = 2 * i + 1;
                break;
            default:
                break;
        }

        switch ((2 * i + 2 < this.size && this.highwayList[2 * i + 2].getCost() < this.highwayList[smallest].getCost())
                ? 1
                : 0) {
            case 1:
                smallest = 2 * i + 2;
                break;
            default:
                break;

        }
        this.heapifyCostHelper(smallest, i);
    }
    
    /**
     * Helper method for heapify
     *
     * @param smallest
     *            smallest index
     * @param i
     *            index
     */
    public void heapifyCostHelper(int smallest, int i) {
        switch ((smallest != i) ? 1 : 0) {
            case 1:
                Highway temp = this.highwayList[i];
                this.highwayList[i] = this.highwayList[smallest];
                this.highwayList[smallest] = temp;

                this.heapifyCost(smallest);
                break;
            default:
                break;
        }
    }

    /**
     * Inserts the given Highway into the minheap
     *
     * @param hwy
     *            the Highway to insert into the minheap
     */
    public void insert(Highway hwy) {

        this.ensureCapacity();
        this.size++;
        int i = this.size - 1;
        this.highwayList[i] = hwy;
        switch (this.isCost) {
            case 0:
                System.out.println("asphalt add " + hwy.toString());
                this.insertHelperAsphalt();
                break;
            case 1:
                System.out.println("cost add " + hwy.toString());
                this.insertHelperCost();
                break;
            default:
                break;
        }

    }
    
    /**
     * Helper method for insert
     */
    public void insertHelperAsphalt() {
        int i = this.size - 1;
        while (i != 0 && this.highwayList[(i - 1) / 2].getAsphalt() > this.highwayList[i].getAsphalt()) {
            Highway temp = this.highwayList[i];
            this.highwayList[i] = this.highwayList[(i - 1) / 2];
            this.highwayList[(i - 1) / 2] = temp;
            i = (i - 1) / 2;

        }
    }

    /**
     * Helper method for insert
     */
    public void insertHelperCost() {
        int i = this.size - 1;
        while (i != 0 && this.highwayList[(i - 1) / 2].getCost() > this.highwayList[i].getCost()) {
            Highway temp = this.highwayList[i];
            this.highwayList[i] = this.highwayList[(i - 1) / 2];
            this.highwayList[(i - 1) / 2] = temp;
            i = (i - 1) / 2;
            
        }
    }

    /**
     * checks if its empty
     *
     * @return 1 if its empty
     */
    public int isEmpty() {
        if (this.size == 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    /**
     * Returns a string representation of the level-order traversal
     * of the heap in the following format:
     *
     * Heap[
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X],
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X],
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X]
     * ]
     *
     * @return the string representation of the minheap
     */
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        if (this.size != 0) {
            buf.append("Heap[\n   ");
            buf.append(this.highwayList[0].toString());
            for (int i = 1; i < this.size; i++) {
                buf.append(",\n   ");
                buf.append(this.highwayList[i].toString());
            }
            buf.append("\n]");
        } else {
            buf.append("Heap[\n]");
        }

        return buf.toString();

    }
}
