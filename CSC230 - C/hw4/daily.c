#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "activity.h"
#include "schedule.h"

/** Size of line*/
#define LINESIZE 100
/**Default length of name*/
#define NAMESIZE 22
/**max length for leader*/
#define MAXNAMESIZE 20
/**Super long name*/
#define SUPERLONGNAME 256
/**minutes in hour*/
#define MINUTES 60
/*hours in a day*/
#define HOURS 24
/*FIVE*/
#define FACTOR 5
/*default hour*/
#define DEFAULTHOUR 7
/*hourlimit*/
#define HOURLIMIT 19
/*halfhour*/
#define HALFHOUR 30

/*schedule struct to be used*/
Schedule *sched;
/*counter for remove*/
int removeCounter = 0;


/**
@param startTime, startTime
@param endTime, endTime
@param selectedTime, selTime
@return bool, whether time is within start and end timeSel

This function returns whether the selected time is within start or end times
*/
bool withinETimes( Time *startTime, Time *endTime, Time *timeSel ) {

    int startTimeI = startTime->hour * MINUTES + startTime->minute;
    int endTimeI = endTime->hour * MINUTES + endTime->minute;
    int timeSelI = timeSel->hour * MINUTES + timeSel->minute;

    if ( timeSelI >= startTimeI && timeSelI < endTimeI ) {
        return true;
    } else {
        return false;
    }
    return false;
}

/**
@param act, activity
@param arg, void argument
@return bool, returns true for all case
*/
bool printAll( Activity *act, void *arg ) {
    return true;
}

/**
@param act, activity
@param arg, leader to match with leader in activity
@return bool, true if leader matches
*/
bool printLeader( Activity *act, void *arg ) {
    if (strcmp(act->leader, ( char * )arg ) == 0) {
        return true;
    } else  {
        return false;
    }
}

/**
@param act, activity
@param arg, time to match with time in activity
@return bool, true if time matches
*/
bool printTime( Activity *act, void *arg ) {
    if ( withinETimes( act->startTime, act->endTime, ( Time * ) arg ) ) {
        return true;
    } else {
        return false;
    }
}

/**
@param str1, activity
@param const str2, second string to compare
@return bool, true if one string is a subset of another
*/
char* stristr( char* str1, const char* str2 )
{
    /* assigns pointer of str1*/
    char* p1 = str1;
    /* assigns pointer of str2 */
    const char* p2 = str2;
    /* char r */
    char* r = *p2 == 0 ? str1 : 0;

    /* compares pointers*/
    while ( *p1 != 0 && *p2 != 0 )
    {
        if ( tolower( *p1 ) == tolower( *p2 ) )
        {
            if ( r == 0 )
            {
                r = p1;
            }

            p2++;
        }
        else
        {
            p2 = str2;
            if ( tolower( *p1 ) == tolower( *p2 ) )
            {
                r = p1;
                p2++;
            }
            else
            {
                r = 0;
            }
        }

        p1++;
    }

    return *p2 == 0 ? r : 0;
}

/**
@param act, activity
@param arg, activity to match with activity descrip in activity
@return bool, true if descrip matches
*/
bool printActivity( Activity *act, void *arg ) {
    if (stristr(act->title, (char *) arg ) ) {
        return true;
    } else {
        return false;
    }
}

/**
Cmd corresponding to add
*/
void addCmd() {
    /*error status*/
    int error = 0;

    /*startTime time struct*/
    Time *startTm = malloc( sizeof(Time) );
    /*endTime time struct*/
    Time *endTm = malloc( sizeof(Time) );

    /*leadername pointer*/
    char *leaderName = ( char * ) malloc( NAMESIZE );
    /*pointer for description name*/
    char *descrip; //= ( char * ) malloc( LINESIZE ); //[-/001-/377]

    /*scans in starttime*/
    if ( scanf("%d : %02d  ", &startTm->hour, &startTm->minute) != 2 ) {
        //error = 1;
    }

    /*scans in endtime*/
    if ( scanf("%d : %02d ", &endTm->hour, &endTm->minute) != 2 ) {
        //error = 1;
        //use atoi to convert str to int
    }

    /*checks if startTime is valid*/
    if ( startTm->hour * MINUTES +  startTm->minute > HOURS * MINUTES ||
         startTm->hour * MINUTES +  startTm->minute < 0 ||
         startTm->hour < 0  || startTm->minute < 0 ||
         startTm->minute >= MINUTES ) {
        error = 1;
    }

    /*checks if startTime is valid*/
    if ( endTm->hour * MINUTES +  endTm->minute > HOURS * MINUTES ||
         endTm->hour * MINUTES +  endTm->minute < 0 ||
         endTm->hour < 0  || endTm->minute < 0 ||
         endTm->minute >= MINUTES) {
        error = 1;
    }

    /*scans in leadername*/
    if ( scanf("%21s", leaderName) ) {
        //error = 1;

    }

    /*allocates room for description*/
    descrip = ( char * ) malloc( LINESIZE );

    /*assigns pointer to descrip*/
    char *line = descrip;

    /*current length*/
    int len = 0;
    /*current max size of array*/
    int maxlen = LINESIZE;

    /*char for index*/
    char c;

    /*checks for starting spaces*/
    int spaceStatus = 0;

    /*loop to go all the way until a newline char is detected*/
    while (1) {

        /*assigns c to std input char*/
        c = fgetc(stdin);

        /*checks to see if array needs to be largened*/
        if ( len > maxlen-1 ) {
            int newMaxSize = 2 * ( len + 1 );
            descrip = ( char * ) realloc( descrip, newMaxSize );

            line = descrip + len;
            maxlen = newMaxSize;
        }

        /*checks if newline or eof is detected*/
        if ( c == '\n' || 'c' == EOF ) {
            *line++ = '\0';
            len++;
            break;
            /*does nothing for prefix spaces*/
        } else if ( c == ' ' && spaceStatus == 0 ) {

            /*adds to string if no problems found*/
        } else {
            *line++ = c;
            len++;
            spaceStatus = 1;
        }
    }

    //printf("%d %d\n", len,strlen(descrip));


    /*converts time to minutes*/
    int startTimeI = startTm->hour * MINUTES + startTm->minute;

    /*converts time to minutes*/
    int endTimeI = endTm->hour * MINUTES + endTm->minute;

    /*checks if startime is after endtime*/
    if ( startTimeI >= endTimeI ) {
        error = 1; //start time is later than end time
    }

    //printf("\n");
    //printf("keyword: %s\n", "add");
    //printf("Start Time: %02d:%02d\n", startTm->hour, startTm->minute);
    //printf("End Time: %02d:%02d\n", endTm->hour, endTm->minute);

    //printf("Leader: %s\n", leaderName);
    //printf("Activity: %s\n", descrip);

    /*checks if a schedule exists*/
    if ( sched == NULL ) {
        sched = createSchedule();
    }

    //printf("compare overlap");
    //printf("start:%d %d\n", startTm->hour, startTm->minute);
    //printf("end:%d %d\n", endTm->hour, endTm->minute);

    /*converts more time*/
    int startTimeCur = startTm->hour * MINUTES + startTm->minute;
    /*converts end time to minute*/
    int endTimeCur = endTm->hour * MINUTES + endTm->minute;
    //printf("%d %d\n", startTimeCur, endTimeCur);

    /*traverses through the array and sees if a time is conflicting with another time*/
    for (int i = 0; i < sched->currSize; i++ ) {
        //printf("%s\n", sched->activities[i].leader);

        if (strcmp(sched->activities[i]->leader, leaderName)==0 ) {
            //printf("same leader\n");

            /*converts each time of existing person to minutes*/
            int startTimeI = sched->activities[i]->startTime->hour * MINUTES +
                             sched->activities[i]->startTime->minute;
            /*converts each time of existing person to minutes*/
            int endTimeI = sched->activities[i]->endTime->hour * MINUTES +
                           sched->activities[i]->endTime->minute;

            /*checks if times overlap*/
            if ( ( (startTimeCur > startTimeI) && (startTimeCur < endTimeI) ) ||
                 ( (endTimeCur   > startTimeI) && (startTimeCur < endTimeI) )
               ) {
                   //printf("overlap\n");
                   printf("Schedule conflict\n");
                   return;
            } else {
                //printf("OK not overlap\n");
            }
        }
    }

    /*if an error has occured, do not create the activity*/
    if ( error == 1 ) {
        printf("Invalid command\n");
        return;
    }
    sched->IDCounter++;
    /*creates a new activity pointer and allocates space*/
    Activity *newAct = malloc( sizeof( Activity ) );

    newAct->title = descrip;
    newAct->leader = leaderName;
    newAct->startTime = startTm;
    newAct->endTime = endTm;
    newAct->ID = sched->IDCounter;
    /*adds the activity to the list*/
    addActivity(sched, newAct);
    return;
}

/**
Cmd of remove
Removes the intended activity
*/
void removeCmd() {

    removeCounter++;
    /*checks if a schedule exists*/
    if ( sched==NULL ) {
        return;
    }

    /*default ID*/
    int ID = -1;
    /*ID length*/
    char removeID[ NAMESIZE ];

    /*fgets(removeID, 4, stdin);
    if ( !( atoi(removeID) > 0 ) ) {
        printf("Invalid Command\n");
        return;
    }*/

    /*reads in ID as a string*/
    fgets(removeID, NAMESIZE - 1, stdin);

    /*changes string to int*/
    ID = atoi(removeID);

    /*checks if conversion is successful*/
    if ( ID == 0 ) {
        //fgets(removeID, 4, stdin);
        printf("Invalid command\n");
        return;
    }

    /*default index*/
    int index = 0;

    /*searches for activity to remove*/
    while ( index < sched->currSize ) {
        /*if id matches, pass to remove*/
        if ( ( sched->activities[index]->ID ) == ID ) {
            removeActivity( sched, sched->activities[index], index );
            return;
        }
        index++;
    }
    /*No activities with that ID found*/
    printf("Invalid command\n");
    //printf("not found\n");
}

/**
Cmd for schedule
Shows all scheduled events
*/
void scheduleCmd() {
    /*sets fp to address of printAll*/
    TESTFunc fp = &printAll;
    printSchedule(sched, fp, NULL);

}

/**
Cmd for leader
shows events based on leader
*/
void leaderCmd() {

    /*allocates room for a leader*/
    char *leader = (char *) malloc ( NAMESIZE );

    /*reads in a leader*/
    if ( scanf("%21s", leader) != 1 ) {
        return;
    }

    if ( strlen(leader) > MAXNAMESIZE ) {

        printf("Invalid command\n");
        return;
    }

    /*sets fp to address of printLeader*/
    TESTFunc fp = &printLeader;
    /*prints*/
    printSchedule(sched, fp, leader);
    free(leader);
}

/**
Cmd for at.
Shows events happening during a certain time
*/
void atCmd() {

    /*default hour and minute*/
    int hour = -1;
    /*default hour and minute*/
    int minute = -1;

    /*reads in hour and minute*/
    scanf( "%d : %d", &hour, &minute );

    /*checks if hours and minutes are valid*/
    if ( hour * MINUTES +  minute > ( HOURS * MINUTES ) ||
         hour * MINUTES +  minute < 0 || hour < 0 ||
         minute < 0 || minute >= MINUTES ) {
             printf("Invalid command\n");
             return;
         }
    Time *atTime = (Time * ) malloc( sizeof( Time ) );;
    atTime->hour = hour;
    atTime->minute = minute;
    TESTFunc fp = &printTime;
    printSchedule(sched, fp, atTime);
    free(atTime);
}

/*
Cmd for match.
Shows matches for certain descriptions
*/
void matchCmd() {

    /*allocates room for description*/
    char *descrip = ( char * ) malloc( LINESIZE );

    /*assigns line pointer address of description*/
    char *line = descrip;

    /*default length = 0*/
    int len = 0;

    /*default max length = 24*/
    int maxlen = LINESIZE;

    /*char to read in individual char from stdin*/
    char c;
    /*checks if there are any prefix spaces*/
    int spaceStatus = 0;

    /*traverses until a newline or eof is encountered*/
    while (1) {

        /*assigns c*/
        c = fgetc(stdin);

        /*checks if c is EOF*/
        if (c == EOF) {
            *line++ = '\0';
            break;
        }

        /*checks if reallocation is needed*/
        if ( len > maxlen ) {
            int newMaxSize = 2 * ( len + 1 );
            line = ( char * ) realloc( line, newMaxSize );
            maxlen = newMaxSize;
        }

        /*checks if c is newline or eof*/
        if ( c == '\n' || 'c' == EOF ) {
            *line++ = '\0';
            break;

            /*checks if it is a space*/
        } else if ( c == ' ' && spaceStatus == 0 ) {

            /*assigns char to c*/
        } else {
            *line++ = c;
            spaceStatus = 1;
        }
    }

    //scanf( "%[-/001-/377]", descrip );

    /*assigns fp to address of printactivity*/
    TESTFunc fp = &printActivity;

    /*prints schedule*/
    printSchedule(sched, fp, descrip);
    free(descrip);

}

/*prints out a summary of all the people and activities*/
void summaryCmd() {
    printf("\n");
    Time *start = malloc( sizeof(Time));;
    start->hour = DEFAULTHOUR;
    start->minute = 0;

    int index = 0;
    int index2 = 0;

    while ( ( start->hour ) < HOURLIMIT ) {
        //printf("%d:%02d", start->hour, start->minute);
        while ( index2 < sched->currSize ) {
            if ( withinETimes(sched->activities[index2]->startTime,
                          sched->activities[index2]->endTime, start ) ) {
                printf("*  ");
            }
            index2++;
        }
        printf("\n");

        /*traverse hours*/
        if ( (index % 2 ) == 0 ) {
            start->minute =+ HALFHOUR;
        } else {
            start->hour++;
            start->minute = HALFHOUR;
        }
        index++;
    }

    free(start);

}

/* main function */
int main() {
    sched = createSchedule();
    /*creates a keyword char*/
    char keywd[ NAMESIZE ];

    /*infinite loop until quit*/
    while ( printf("> ") ) {
        /*scans in keyword*/
        int n=scanf("%21s", keywd);

        if (n ==-1) {
            //printf("\n >");
            //break;
            freeSchedule(sched);
            return 0;

        }

        if (!strcmp(keywd, "quit")) {
            return 0;
        } else if (!strcmp(keywd, "add")) {
            addCmd();
        } else if (!strcmp(keywd, "remove")) {
            removeCmd();
        } else if (!strcmp(keywd, "schedule")) {
            scheduleCmd();
        } else if (!strcmp(keywd, "at")) {
            atCmd();
        } else if (!strcmp(keywd, "match")) {
            matchCmd();
        } else if (!strcmp(keywd, "leader")) {
            leaderCmd();
        } else if (!strcmp(keywd, "summary")) {
            summaryCmd();
        } else {
            printf("Invalid command\n");
        }
    }
    freeSchedule(sched);
    return 1;
}
