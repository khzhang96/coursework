//
// A program for playing with the debugger.
//
// The program is a little silly, but it gives us something we can
// trace with the debugger.  We make an array of pointers to
// functions, then we call a randomly selected function on the list.
// It prints out a message.  Your job is to trace the program's
// execution and figure out which funciton printed each message.
//

#include <stdio.h>
#include <stdlib.h>

void edith( int x )
{
  // Edith likes numbers that are even.
  if ( x % 2 == 0 )
    printf( "I like %d\n", x );
  else
    printf( "I don't like %d\n", x );
}

void tyler( int x )
{
  // Tyler likes numbers that are less than 50.
  if ( x < 50 )
    printf( "I like %d\n", x );
  else
    printf( "I don't like %d\n", x );
}


void melonie( int x )
{
  // Melonie likes number that have a 3 or a 7 in them.

  // Write out the number as a string.
  char buffer[ 20 ];
  sprintf( buffer, "%d", x );

  // Look for an occurrence of a 3 or a 7.
  for ( int i = 0; buffer[ i ]; i++ )
    if ( buffer[ i ] == '3' || buffer[ i ] == '7' ) {
      printf( "I like %d\n", x );
      return;
    }

  // If we didn't find one of those digits, we don't like the number.
  printf( "I don't like %d\n", x );
}

void aaron( int x )
{
  // Aaron likes number that are larger than
  // the last one he saw.

  static int previous = -1;

  if ( x > previous )
    printf( "I like %d\n", x );
  else
    printf( "I don't like %d\n", x );
  
  previous = x;
}


int main( void )
{
  // Make an array of function pointers.
  void (*flist[])( int ) = {
    edith, tyler, melonie, aaron
  };

  // Generate a random sequence of numbers.
  int seq[ 5 ];
  for ( int i = 0; i < sizeof( seq ) / sizeof( seq[ 0 ] ); i++ )
    seq[ i ] = rand() % 100;

  // Here's where we call the randomly selected functions.  Using the array
  // of function pointers, choose a random function, and call it with a value
  // from seq[] as the parameter.
  for ( int i = 0; i < sizeof( seq ) / sizeof( seq[ 0 ] ); i++ ) {
    int choice = rand() % ( sizeof( flist ) / sizeof( flist[ 0 ] ) );
    flist[ choice ]( seq[ i ] );
  }
}
