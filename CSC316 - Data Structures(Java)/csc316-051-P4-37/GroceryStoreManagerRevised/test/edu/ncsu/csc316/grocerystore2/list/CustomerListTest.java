package edu.ncsu.csc316.grocerystore2.list;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import edu.ncsu.csc316.grocerystore2.customer.Customer;
import edu.ncsu.csc316.grocerystore2.list.CustomerList;

/**
 * Customer List test
 *
 * @author khzhang
 *
 */
public class CustomerListTest {
    /**
     * Creates a random string of a certain length
     *
     * @param candidateChars
     *            possible characters
     * @param length
     *            length of intended string
     * @return a string
     */
    public static String generateRandomChars(String candidateChars, int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(candidateChars.charAt(random.nextInt(candidateChars.length())));
        }
        
        return sb.toString();
    }
    
    /**
     * Company field
     */
    public String company;
    /**
     * id field
     */
    public String id;
    /**
     * state field
     */
    public String state;
    /**
     * zipcode field
     */
    public String zipcode;
    
    /**
     * Test 1
     */
    @Test
    public void test() {
        CustomerList testList = new CustomerList();
        
        this.id = "C01";
        this.company = "EEKO";
        this.state = "HK";
        this.zipcode = "00000";
        
        Customer testCust1 = new Customer(this.id, this.company, this.state, this.zipcode);
        
        testList.add(testCust1);
        
        Assert.assertEquals(testList.getSize(), 1);
        
        this.id = "C02";
        this.company = "EEK0";
        this.state = "HKK";
        this.zipcode = "00001";
        
        Customer testCust2 = new Customer(this.id, this.company, this.state, this.zipcode);
        
        testList.add(testCust2);
        Assert.assertEquals(testList.getSize(), 2);
        
        String test = "Customer [id=C01, company=EEKO, state=HK, zipcode=00000]\nCustomer [id=C02, company=EEK0, state=HKK, zipcode=00001]\n";
        Assert.assertEquals(test, testList.toString());
        
        this.id = "C03";
        this.company = "EEK1";
        this.state = "HHK";
        this.zipcode = "10000";
        
        Customer testCust3 = new Customer(this.id, this.company, this.state, this.zipcode);
        testList.add(testCust3);
        Assert.assertEquals(testList.getSize(), 3);
        
        Assert.assertEquals(testCust2, testList.get(1));
        
        // System.out.println(testList.toString());
        testList.remove(0);
        
        // System.out.println(testList.toString());
        Assert.assertEquals(2, testList.getSize());
        
        this.id = "C04";
        this.company = "EEK4";
        this.state = "HHK";
        this.zipcode = "10000";
        
        Customer testCust4 = new Customer(this.id, this.company, this.state, this.zipcode);
        testList.set(1, testCust4);
        Assert.assertEquals(testCust4, testList.get(1));
        
        this.id = "C05";
        this.company = "EEK4";
        this.state = "HHK";
        this.zipcode = "10000";
        
        Customer testCust5 = new Customer(this.id, this.company, this.state, this.zipcode);
        testList.add(testCust5);
        
        // System.out.println(testList.toString());
        Assert.assertEquals(testCust5, testList.get(2));
        
        String test2 = testList.toString();
        Assert.assertEquals(test2, testList.toString());
        
        this.id = "C01";
        this.company = "EEK4";
        this.state = "HHK";
        this.zipcode = "10000";
        
        Customer testCust6 = new Customer(this.id, this.company, this.state, this.zipcode);
        testList.add(testCust6);
        
        for (int i = 0; i < 500000; i++) {
            testList.add(new Customer(CustomerListTest.generateRandomChars("0123456789ABCDEF", 4),
                    CustomerListTest.generateRandomChars("0123456789ABCDEF", 4),
                    CustomerListTest.generateRandomChars("0123456789ABCDEF", 4),
                    CustomerListTest.generateRandomChars("0123456789ABCDEF", 4)));
        }
        
        // testList.sort();
        
        testList.msort();
        // testList.selectionSort(); // only for coverage
        // System.out.println(testList.toString(0));
        // System.out.println(testList.toString());
        
    }
    
}
