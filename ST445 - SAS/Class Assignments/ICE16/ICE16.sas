libname ICE16 '\\tsclient\D\ST445\Class Assignments\ICE16';

filename weight '\\tsclient\D\ST445\Class Assignments\ICE16\weight.txt';
data ICE16.weight;
	infile weight dlm = ',:';

	input name $ w1 w2 w3 w4 w5 w6;
run;

proc print data = ICE16.weight;
run;

data ICE16.newdata;
	array BaseWeightDiffs[5] BaseWeightDiff1-BaseWeightDiff5;
	array Weights[5] Weight1-Weight5;
	do counter1 = 1 to 7;
		do counter2 = 1 to 3;
			BaseWeightDiff[counter2] = w2 - w1
			output;
		end;
	end;
run;
