/**
 *
 */
package edu.ncsu.csc316.grocerystore2.list;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import edu.ncsu.csc316.grocerystore2.list.ProductList;
import edu.ncsu.csc316.grocerystore2.order.Product;

/**
 * Product List Test
 *
 * @author khzhang
 *
 */
public class ProductListTest {
    
    /**
     * Genererates a random character string
     *
     * @param candidateChars
     *            possible characters
     * @param length
     *            length of characters
     * @return String of characters
     */
    public static String generateRandomChars(String candidateChars, int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(candidateChars.charAt(random.nextInt(candidateChars.length())));
        }
        
        return sb.toString();
    }

    private String brand;
    
    private String description;
    
    /**
     * Test 1
     */
    @Test
    public void test() {

        ProductList testList = new ProductList();

        this.brand = "HTC";
        this.description = "Dream";

        Product dream = new Product(this.brand, this.description);

        testList.add(dream);

        this.brand = "HTC";
        this.description = "Dream";

        Product dream2 = new Product(this.brand, this.description);

        testList.add(dream2);

        Assert.assertEquals(1, testList.getSize());
        Assert.assertEquals(2, testList.get(0).getFreq());

        this.brand = "HTC";
        this.description = "Desire";

        Product desire = new Product(this.brand, this.description);
        testList.add(desire);

        this.brand = "Samsung";
        this.description = "Captivate";

        Product capt = new Product(this.brand, this.description);
        testList.add(capt);

        System.out.println(testList.toString());

        testList.remove(desire);
        testList.remove(dream);
        // assertEquals(1, testList.get(0).getFreq());
        testList.add(capt);
        testList.add(capt);
        testList.add(capt);
        testList.add(capt);
        testList.add(capt);
        testList.add(capt);
        testList.add(capt);

        testList.add(dream2);
        testList.add(dream2);
        testList.add(dream2);
        testList.add(dream2);
        testList.add(dream2);
        testList.add(dream2);
        testList.add(dream2);
        testList.add(dream2);

        testList.add(desire);
        testList.add(desire);
        testList.add(desire);
        testList.add(desire);
        testList.add(desire);
        testList.add(desire);

        testList.remove(capt);

        Assert.assertEquals(7, testList.get(2).getFreq());
        
        for (int i = 0; i < 5000; i++) {
            testList.add(new Product(ProductListTest.generateRandomChars("0123456789ABCDEF", 4),
                    ProductListTest.generateRandomChars("0123456789ABCDEF", 4)));
            testList.add(new Product(ProductListTest.generateRandomChars("0123456789ABCDEF", 4),
                    ProductListTest.generateRandomChars("0123456789ABCDEF", 1)));
            testList.add(new Product(ProductListTest.generateRandomChars("0x" + "0123456789ABCDEF", 4),
                    ProductListTest.generateRandomChars("0123456789ABCDEF", 4)));
            testList.add(new Product(ProductListTest.generateRandomChars("0123456789ABCDEF", 1),
                    ProductListTest.generateRandomChars("0123456789ABCDEF", 4)));
            testList.add(new Product(ProductListTest.generateRandomChars("0x" + "0123456789ABCDEF", 4),
                    ProductListTest.generateRandomChars("0123456789ABCDEF", 4)));
        }

        System.out.println(testList.toString());

    }
    
}
