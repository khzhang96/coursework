/* @author: Kevin Zhang
@date: 2016-9-26

Reads in Cost and LIC_702-P.txt
*/


libname HW3 '\\tsclient\D\ST445\HW3\';
libname HW3fmt '\\tsclient\D\ST445\HW3\';

filename Costs '\\tsclient\D\ST445\HW3\Costs.txt';
filename LIC '\\tsclient\D\ST445\HW3\LIC_702-P.txt';
proc format library=HW3fmt;
	value YESNO 1 = 'Yes'
		        0 = 'No';
	value $YESNO 'Y' = 'Yes'
				 'N' = 'No';
	value costInt low-20 = '1'
				   21-40 = '2'
				   41-60 = '3'
				   61-80 = '4'
				 81-high = '5';
	value $cost '1'-<'20' = '1'
			   '21'-<'40' = '2'
			   '41'-<'60' = '3'
			   '61'-<'80' = '4'
			   '81'-<'high' = '5';
	/*difference between cost and cost int are that cost is a numeric variable while cost is a categorical variable*/
run;

data HW3.Costs;

	infile Costs;

	input     Q1  : 1.
		      Q2  : $1.
		      C1  : dollar8.
		  @13 C2  : $6.;
	
run;

proc print data=HW3.Costs;
	options fmtsearch = (HW3fmt);
	format Q1 YESNO. Q2 $YESNO. C1 costInt. C2 $cost.;
run;

data HW3.LIC;
infile LIC dlm='|';
input $pro_cde $Profession-Name lic_id Expire-Date Original-Date Rank-Code License-Number Status-Effective-Date Board-Action-Indicator License-Status-Description $Last-Name $First-Name $Middle-Name $Name-Suffix $Business-Name $License-Active-Status-Description $County $County-Description $Mailing-Address-Line1 $Mailing-Address-line2 $Mailing-Address-City $Mailing-Address-State $Mailing-Address-ZIPcode $Mailing-Address-Area-Code $Mailing-Address-Phone-Number $Mailing-Address-Phone-Extension $Practice-Location-Address-Line1 $Practice-Location-Address-line2 $Practice-Location-Address-City $Practice-Location-Address-State $Practice-Location-Address-ZIPcode $Email Mod-Cdes $Prescribe-Ind $Dispensing-Ind
run;

proc print data=HW3.LIC;
run; 

