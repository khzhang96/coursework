data cigarettes;
infile 'cigarettes.txt' firstobs=2;
input brand$ tar nicotine weight co;
run;

symbol value=dot cv=blue;
proc gplot data=cigarettes;
plot tar*co;
plot nicotine*co;
plot weight*co;
run;

proc corr data=cigarettes;
var co tar nicotine weight;
run;

proc reg data=cigarettes;
model co = tar nicotine weight;
run;

proc reg data=cigarettes;
model co = tar nicotine weight;
plot residual.*tar;
plot residual.*nicotine;
plot residual.*weight;
plot residual.*predicted.;
plot nqq.*residual.;
run;

proc reg data=cigarettes alpha=.1;
model co = tar nicotine weight/clb;
run;

data missing;
input brand$ tar nicotine weight co;
datalines;
. 10 .85 .95 .
;

data cigarettes;
set cigarettes missing;
run;

proc reg data=cigarettes alpha=.05;
model co = tar nicotine weight;
output out=out1 r=resid p=pred ucl=pihigh lcl=pilow uclm=cihigh lclm=cilow stdp=stdmean;
run;

data intervals;
set out1;
if co=.;
run;

proc print data=intervals;
run;
