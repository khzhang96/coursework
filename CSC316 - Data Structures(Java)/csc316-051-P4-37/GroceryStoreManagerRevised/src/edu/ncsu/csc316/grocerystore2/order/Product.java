package edu.ncsu.csc316.grocerystore2.order;

/**
 * Product Class
 *
 * @author khzhang
 *
 */
public class Product {

    /**
     * Product Brand
     */
    private String brand;
    /**
     * Product Description
     */
    private String description;
    /**
     * Product Frequency
     */
    private int freq;

    /**
     * Constructor for product
     *
     * @param brand
     *            - sets the brand
     * @param description
     *            - sets the description
     */
    public Product(String brand, String description) {
        this.brand = brand.trim(); // trims the brand
        this.description = description.trim(); // trims the description
        this.freq = 1; // sets default frequency to one
    }

    /**
     * Decrements the frequency
     *
     * @return returns the frequency
     */
    public int dec() {
        return --this.freq; // returns frequency after its been decremented
    }

    /**
     * Custom equals function to test if 2 Products are equal without taking into
     * account frequency
     *
     * @param p
     *            Product
     * @return Boolean - true if equal, fast if not equal
     */

    public Boolean equalsMod(Product p) { // checks if only brand and description fields match
        if (p.getBrand().equals(this.getBrand()) && p.getDescription().equals(this.getDescription())) {
            return true;
        }
        
        return false;
    }

    /**
     * Gets the brand
     *
     * @return product brand
     */
    public String getBrand() {
        return this.brand; // gets the brand
    }

    /**
     * Gets the description
     *
     * @return returns the description
     */
    public String getDescription() {
        return this.description; // gets the description
    }

    /**
     * gets the frequency
     *
     * @return returns Frequency
     */
    public int getFreq() {
        return this.freq; // gets the frequency
    }

    /**
     * Increments the frequency
     *
     * @return returns the freqency
     */
    public int inc() { // increments the frequency
        return ++this.freq;
    }

    /**
     * Set Brand
     *
     * @param brand
     *            - sets the brand
     */
    public void setBrand(String brand) {
        this.brand = brand.trim(); // sets the brand
    }

    /**
     * Set description
     *
     * @param description
     *            - sets the description
     */
    public void setDescription(String description) {
        this.description = description.trim(); // sets the description
    }

    @Override
    public String toString() { // creates a string representation of the product

        StringBuffer newBuf = new StringBuffer(); // creates a new string buffer to append to

        newBuf.append("Product [brand=");
        newBuf.append(this.brand);
        newBuf.append(", description=");
        newBuf.append(this.description);
        newBuf.append(", frequency=");
        newBuf.append(this.freq);
        newBuf.append("]");

        return newBuf.toString(); // returns string buffer now a string
    }

}
