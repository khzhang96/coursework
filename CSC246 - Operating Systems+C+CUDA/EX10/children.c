#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

static void fail( char *msg ) {
  printf( "Error: %s\n", msg );
  exit( EXIT_FAILURE );
}


// parent gets the pid of its child 
//child gets pid of 0

int main( int argc, char *argv[] ) {
  // Try to make a child process.
  pid_t pid1 = fork();
  
  //pid_t pid2 = fork();
  //pid_t pid3 = fork();

  
  
  if ( pid1 < 0 )
    fail( "Can't create child process" );
  else if ( pid1 == 0 ) {
    sleep( 1 );
    
    //printf( "child: current pid: %d, parent pid: %d\n", getpid(), getppid() );
    printf( "I am %d, child of %d\n", getpid(), getppid() );
    exit( EXIT_SUCCESS );
  } else if ( pid1 > 0 ) { //pid > 0 means that this is parent
    //printf( "parent: current pid: %d, parent pid: %d\n", getpid(), getppid() );
    pid1 = wait(NULL);
  }

  pid_t pid2 = fork();
  
  //pid_t pid2 = fork();
  //pid_t pid3 = fork();

  
  
  if ( pid2 < 0 )
    fail( "Can't create child process" );
  else if ( pid2 == 0 ) {
    sleep( 1 );
    
    //printf( "child: current pid: %d, parent pid: %d\n", getpid(), getppid() );
    printf( "I am %d, child of %d\n", getpid(), getppid() );
    exit( EXIT_SUCCESS );
  } else if ( pid2 > 0) { //pid > 0 means that this is parent
    //printf( "parent: current pid: %d, parent pid: %d\n", getpid(), getppid() );
    pid2 = wait(NULL);
  }

  pid_t pid3 = fork();
  
  //pid_t pid2 = fork();
  //pid_t pid3 = fork();

  
  
  if ( pid3 < 0 )
    fail( "Can't create child process" );
  else if ( pid3 == 0 ) {
    sleep( 1 );
    
    //printf( "child: current pid: %d, parent pid: %d\n", getpid(), getppid() );
    printf( "I am %d, child of %d\n", getpid(), getppid() );
    exit( EXIT_SUCCESS );
  } else { //pid > 0 means that this is parent
    //printf( "parent: current pid: %d, parent pid: %d\n", getpid(), getppid() );
    pid3 = wait(NULL);
  }

  printf( "Done\n" );
  

  return EXIT_SUCCESS;
}
