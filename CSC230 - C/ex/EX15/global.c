#include <stdio.h>
#include <math.h>

//#define M_PI 3.14159

struct City {
  char *name;
  double latitude;
  double longitude;
};

struct City readCity() {
  char *name;
  int lat;
  int longi;


  scanf("%s \n", name);
  scanf("%d \n", &lat);
  scanf("%d \n", &longi);

  struct City c = {name, lat, longi};

  printf("City Name: %s\n", name);
  printf("Latutude: %d\n", lat);
  printf("Longitude: %d\n", longi);

  return c;
}

void printDistance( struct City cty1, struct City cty2 ) {
  // OK, pretend the center of the earth is at the origin, turn these
  // two city locations into vectors pointing from the origin.
  // This could be simplified.
  double v1[ 3 ] = { cos( cty1.longitude * M_PI / 180 ) * cos( cty1.latitude * M_PI / 180 ),
                     sin( cty1.longitude * M_PI / 180 ) * cos( cty1.latitude * M_PI / 180 ),
                     sin( cty1.latitude * M_PI / 180 ) };

  double v2[ 3 ] = { cos( cty2.longitude * M_PI / 180 ) * cos( cty2.latitude * M_PI / 180 ),
                     sin( cty2.longitude * M_PI / 180 ) * cos( cty2.latitude * M_PI / 180 ),
                     sin( cty2.latitude * M_PI / 180 ) };

  // Compute the angle between these two vectors.
  double angle = acos( v1[ 0 ] * v2[ 0 ] + v1[ 1 ] * v2[ 1 ] + v1[ 2 ] * v2[ 2 ] );

  // Distance based on the radius of the earth.
  double distance = 3959.0 * angle;
  printf( "Distance between %s and %s: %.2f miles\n", cty1.name, cty2.name,
          distance );
}

int main()
{
  // Read names and locations for two cities.
  struct City cty1 = readCity();
  struct City cty2 = readCity();

  printDistance( cty1, cty2 );
}
