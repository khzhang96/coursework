data film;

input time fType $ p1 p2 p3;
pType = 'P1'; press = p1; output;
pType = 'P2'; press = p2; output;
pType = 'P3'; press = p3; output;

keep time fType pType press;
/*fType is Film Type*/
/*press is pressure*/
datalines;
1 old 15.39 15.49 15.39
1 new 15.40 15.62 15.14
2 old 15.97 15.79 14.99
2 new 15.25 15.37 15.55
3 old 15.88 15.91 15.48
3 new 15.92 15.26 15.43
4 old 15.36 15.30 15.47
4 new 15.30 15.53 15.66
5 old 15.86 15.19 14.93
5 new 15.42 15.03 15.26
6 old 15.53 15.61 15.49
6 new 15.32 15.55 15.50
7 old 15.91 16.06 15.53
7 new 15.75 15.54 15.68
8 old 16.06 15.55 15.49
8 new 15.75 15.31 15.62
;
run;

proc glm plots = diagnostics (unpack );
class time fType pType;
model press = fType time ( fType ) pType fType * pType;
random ( fType ) / test;
means fType / snk e = time (fType);
run;

proc print data = film;
run;
