/*
Author: Jonathan Stallings
Date: UNK
Purpose: Demonstrate libraries

Updated by: Jonathan Duggins
Date: 2017-08-19
Purpose: Add header, update file locations

*/

/*
You will need to replace the directories below with 
wherever you have stored the data
*/


/*
Import veggies.txt into temporary data set 
without WORK. reference
*/

data veg; *Same as before but not WORK.Veg;
   infile "\\tsclient\D\ST445\Class Assignments\822";
   input Food $ Code $ Days Number Price;
   CostPerSeed=Price/Number;
RUN;

/*
- Check out the WORK library to verify it is there
- Close SAS and reopen (but don't run code again!)
- Look at the WORK library now
*/


/*********************************************************/

*What's wrong with this LIBNAME statement;
libname Demo2_1 "S:/Documents/CURRENT COURSES/ST 445/Lectures/Lecture 2/Data/veggies.txt"; 

/*
Note: you should be connecting to a folder, not a specific file
*/


/*********************************************************/

/*Write code below to create a permanent copy of Veg in the library Demo2_1*/

data Demo2_1.Veg;
   set Veg;
run;


/*********************************************************/

/*Write code below to create a temporary copy of Blood*/
/*NOTE: It is often a good idea to create temporary copies of your data
when writing new code.  This way if you mess up you haven't ruined the original*/


data Blood;
   set Demo2_1.Blood;
run;
 


