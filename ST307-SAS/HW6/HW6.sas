
data weightloss;
infile 'weightloss.txt' dlm=',';
input before after;
run;

proc ttest data=weightloss;
paired before*after;
run;

/*data headaches;
infile 'headaches.txt';
group='Asp'; input DRP @@; output;
group='Tyl'; input DRP @@; output;
run;

proc ttest data=headaches;
class group;
run;*/
