#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>
#include <semaphore.h>

// Maximum number of songs on the playlist.
#define LIST_MAX 10

// Maximum length of the name of a song.
#define SONG_MAX 30

sem_t calc;

// Representation for a list of songs.
struct Playlist {
  // List of all the titles on the playlist.
  char title[ LIST_MAX ][ SONG_MAX ];
  
  int size;
  // Add more fieds as needed.
  // ...
};
 
struct Playlist *pList;

// Print out an error message and exit.
static void fail( char const *message ) {
  fprintf( stderr, "%s\n", message );
  exit( 1 );
}



void addNext(struct Playlist *pList, char *str) {
  #ifndef UNSAFE 
  sem_wait(&calc); 
  #endif

  //char *str2 = NULL;
  if (pList->size == 0) {
    strcpy(pList->title[0], str);
    pList->size++;
  } else if (pList->size == 1) {
    strcpy(pList->title[1], str);
    pList->size++;
  } else if (pList->size == 2) {
    strcpy(pList->title[2], pList->title[1]);
    strcpy(pList->title[1], str);
    pList->size++;
  } else if (pList->size < 30) {
    for (int i = pList->size /*max index*/; i > 1; i--) {
      //pList->title[i] = pList->title[i - 1];
      strcpy(pList->title[i], pList->title[i - 1]);
    }
    pList->size++;
    strcpy(pList->title[1], str);
    
  } else {
    printf("error: list full\n");
  }
  
  sem_post(&calc);
}

void addLast(struct Playlist *pList, char *str) {
  #ifndef UNSAFE 
  sem_wait(&calc); 
  #endif
  if (pList->size != 0) {
    if ( pList->size < 30 ) {
      strcpy(pList->title[pList->size],str);
      pList->size++;
    } else {
      printf("error: list full\n");
    }
  } else {
    addNext(pList, str);
  }
  sem_post(&calc);
  
}

void removeBack(struct Playlist *pList) {
  #ifndef UNSAFE 
  sem_wait(&calc); 
  #endif
  if (pList->size != 0) {
    pList->size--;
  } else {
    printf("error: list empty\n");
  }
  sem_post(&calc);
  

}

void forward(struct Playlist *pList) {
  #ifndef UNSAFE 
  sem_wait(&calc); 
  #endif
  if (pList->size != 0) {
    for(int i = 0; i < pList->size; i++) {
      strcpy(pList->title[i], pList->title[i + 1]);
    }
    pList->size--;
  } else {
    printf("error: list empty\n");
  }
  sem_post(&calc);
}

void showList(struct Playlist *pList) {
  if (pList->size == 0) {
    printf("error: list empty\n");
  } else {
    for (int i =0; i < pList->size; i++) {
      printf("%s\n", pList->title[i]);
    }
  }
  

  //printf("size: %d\n", pList->size);
}

void playing(struct Playlist *pList) {
  printf("%s\n", pList->title[0]);
}


int main( int argc, char *argv[] ) {
  sem_init(&calc, 0, 1); 

  key_t key = 1;

  if (argc < 1) {
    fail("error");
  }
  int shm_id = shmget(key, sizeof(struct Playlist), IPC_CREAT | 0666 );
  pList = (struct Playlist *) shmat(shm_id, NULL, IPC_CREAT | 0666);
  //fd = shm_open(key, O_CREAT|O_RDWR, 0666);
  //ftruncate (fd, sizeof(struct Playlist));

  //pList = mmap(NULL, sizeof(struct Playlist), PROT_READ | PROT_WRITE, MAP_SHARED,fd, 0);
  char *fullstring = NULL;
  /*if (argc > 2) {
    fullstring = malloc(1000);
    strcpy(fullstring, argv[2]);
    if (argc > 3) {
      for (int i = 3; i< argc; i++) {
        strcat(fullstring, ' ');
        strcat(fullstring, argv[i]);
      }
    }
  }*/
  
  if (argc == 2) {
    if (strcmp(argv[1],"forward") == 0) {
      forward(pList);
    } else if (strcmp(argv[1],"playing") == 0) {
      playing(pList);
    } else if (strcmp(argv[1],"list") == 0) {
      showList(pList);
    }
  } else if (argc > 2) {
    if (strcmp(argv[1],"next") == 0) {
      fullstring = malloc(10000);
      strcpy(fullstring, argv[2]);
      if (argc > 3) {
        for (int i = 3; i< argc; i++) {
          strcat(fullstring, " ");
          strcat(fullstring, argv[i]);
        }
      }
      addNext(pList, fullstring);
    } else if (strcmp(argv[1],"last") == 0) {
      fullstring = malloc(10000);
      strcpy(fullstring, argv[2]);
      if (argc > 3) {
        for (int i = 3; i< argc; i++) {
          strcat(fullstring, " ");
          strcat(fullstring, argv[i]);
        }
      }
      addLast(pList, fullstring);
    } else if (strcmp(argv[1],"test") == 0) {
      int n = atoi(argv[2]);
      fullstring = malloc(10000);
      strcpy(fullstring, argv[3]);
      if (argc > 4) {
        for (int i = 4; i< argc; i++) {
          strcat(fullstring, " ");
          strcat(fullstring, argv[i]);
        }
      }
      
      for (int i = 0; i < n; i++) {
        //printf("n = %d\n", n);
        addLast(pList, fullstring);
        forward(pList);
      }

    }
  }
  
  /*if (strcmp(argv[1],"next") == 0) {
    addNext(fullstring);
  } else if (strcmp(argv[1],"last") == 0) {
    addLast(fullstring);
  } else if (strcmp(argv[1],"forward") == 0) {
    forward();
  } else if (strcmp(argv[1],"playing") == 0) {
    playing();
  } else if (strcmp(argv[1],"list") == 0) {
    showList();
  } else {
    fail("error");
  }*/

  //test();


  return 0;
}
