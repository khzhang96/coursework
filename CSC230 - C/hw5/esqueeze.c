/**
@author Kevin zhang
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>

#include "bits.h"
#include "codes.h"

#define ARGNUM 3
#define EXITFLAG 31
#define NEG1 -1

/**
*@param argc num of args
*@param *argv[] args
*/
int main( int argc, char *argv[] ) {
    if ( argc != ARGNUM ) {
        fprintf(stderr, "usage: squeeze <infile> <outfile>\n");
        exit(EXIT_FAILURE);
    }
    FILE *fpRead = fopen(argv[1], "r");
    FILE *fpWrite = fopen(argv[2], "wb");

    if ( fpRead == NULL ) {
        fprintf( stderr, "%s: No such file or directory\n", argv[1] ) ;
        exit(EXIT_FAILURE);
    }

    BitBuffer *buffer = malloc(sizeof(BitBuffer));

    buffer->bits = 0;
    buffer->bcount = 0;

    write5Bits(0x1, buffer, fpWrite); //writes starting code

    int c = NEG1;
    //fscanf( fp, "%c", &c )
    while ( ( c = fgetc(fpRead) ) != EOF ) {
        int code;
        if ( ( code = symToCode(c) ) != EXITFLAG ) { //part of key
            write5Bits( code, buffer, fpWrite );
        } else if ( ( code = symToCode(c) ) == 31 ) { //not part of key
            write5Bits( code, buffer, fpWrite ); //writes 31 (escape code 0x1f)
            write8Bits( (unsigned char) c, buffer, fpWrite ); //writes the 8 bit code
        }
    }

    flushBits(buffer, fpWrite);


    fseek(fpRead, 0L, SEEK_END);
    int readSize = ftell(fpRead);

    fseek(fpWrite, 0L, SEEK_END);
    int writeSize = ftell(fpWrite);
    fclose(fpRead);
    fclose(fpWrite);

    if ( readSize < writeSize ) {
        fpRead = fopen(argv[1], "r");
        fpWrite = fopen(argv[2], "wb");
        //printf("here\n");
        buffer->bits = 0;
        buffer->bcount = 0;

        write5Bits(0x0, buffer, fpWrite); //writes starting code

        int c = NEG1;
        //fscanf( fp, "%c", &c )
        while ( ( c = fgetc(fpRead) ) != EOF ) {
            write8Bits( (unsigned char) c, buffer, fpWrite ); //writes the 8 bit code
        }
        flushBits(buffer, fpWrite);
        fclose(fpRead);
        fclose(fpWrite);
    }

    return 0;
}
