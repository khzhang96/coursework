#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>

#define E 0
#define T 1
#define A 2
#define I 3
#define QUOTE 4
#define N 5
#define SPACE 6
#define S 7
#define O 8
#define L 9
#define R 10
#define D 11
#define C 12
#define GREATER 13
#define LESS 14
#define BSLASH 15
#define P 16
#define M 17
#define DASH 18
#define U 19
#define PERIOD 20
#define H 21
#define F 22
#define UNDERSCORE 23
#define EQUAL 24
#define G 25
#define COLON 26
#define B 27
#define ZERO 28
#define Y 29
#define NEWLINE 30
#define OTHER 31

int symToCode(unsigned char ch) {
    switch (ch) {

        case 'e' :
            return E;
        case 't' :
            return T;
        case 'a' :
            return A;
        case 'i' :
            return I;
        case '"' :
            return QUOTE;
        case 'n' :
            return N;
        case ' ' :
            return SPACE;
        case 's' :
            return S;
        case 'o' :
            return O;
        case 'l' :
            return L;
        case 'r' :
            return R;
        case 'd' :
            return D;
        case 'c' :
            return C;
        case '>' :
            return GREATER;
        case '<' :
            return LESS;
        case '/' :
            return BSLASH;
        case 'p' :
            return P;
        case 'm' :
            return M;
        case '-' :
            return DASH;
        case 'u' :
            return U;
        case '.' :
            return PERIOD;
        case 'h' :
            return H;
        case 'f' :
            return F;
        case '_' :
            return UNDERSCORE;
        case '=' :
            return EQUAL;
        case 'g' :
            return G;
        case ':' :
            return COLON;
        case 'b' :
            return B;
        case '0' :
            return ZERO;
        case 'y' :
            return Y;
        case '\n' :
            return NEWLINE;
        default:
            return OTHER;
    }
}

int codeToSym(int code) {
    switch (code) {

        case E :
            return (int)'e';
        case T :
            return (int)'t';
        case A :
            return (int)'a';
        case I :
            return (int)'i';
        case QUOTE :
            return (int)'"';
        case N :
            return (int)'n';
        case SPACE :
            return (int)' ';
        case S :
            return (int)'s';
        case O :
            return (int)'o';
        case L :
            return (int)'l';
        case R :
            return (int)'r';
        case D :
            return (int)'d';
        case C :
            return (int)'c';
        case GREATER :
            return (int)'>';
        case LESS :
            return (int)'<';
        case BSLASH :
            return (int)'/';
        case P :
            return (int)'p';
        case M :
            return (int)'m';
        case DASH :
            return (int)'-';
        case U :
            return (int)'u';
        case PERIOD :
            return (int)'.';
        case H :
            return (int)'h';
        case F :
            return (int)'f';
        case UNDERSCORE :
            return (int)'_';
        case EQUAL :
            return (int)'=';
        case G :
            return (int)'g';
        case COLON :
            return (int)':';
        case B :
            return (int)'b';
        case ZERO :
            return (int)'0';
        case Y :
            return (int)'y';
        case NEWLINE :
            return (int)'\n';
    }
    return (int) code;
}
