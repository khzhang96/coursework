data mods;
length type$ 9;
infile 'McDonalds_comma.txt' firstobs=2 DLM=',' DSD;
input type$ cal cal_fat tot_fat sat_fat chol sodium carbs fiber sugars protein;
run;

proc print data=mcds (obs=10);
run;

proc gchart data=mcds;
vbar tot_fat/space=0;
pie type/percent=inside;
run;

proc boxplot data=mcds;
plot sodium*type;
