data reading;
infile 'C:\Users\stat\Documents\ST307\datasets\reading.txt' firstobs=2 DLM='09'X DSD; *note data is separated by a tab;
group='control'; input DRP @@; output;
group='trt'; input DRP @@; output;
run;

proc print data=reading (obs=5); 
run;

proc ttest data=reading H0=54;
var DRP;
run;

proc ttest data=reading H0=54 alpha=0.1;
var DRP;
run;

proc sort data=reading;
by group;
run;

proc ttest data=reading H0=54;
by group;
run;

quit;


***ADDITIONAL PROBLEM;
data apple;
infile 'C:\Users\stat\Documents\ST307\datasets\applejuice.txt'; 
input ounces;
run;

proc print data=apple (obs=5); 
run;

proc ttest data=apple H0=64.05;
var ounces;
run;

quit;
