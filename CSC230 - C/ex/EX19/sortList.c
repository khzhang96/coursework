#include <stdio.h>
#include <stdlib.h>

// Put your macro definitions here.  That should be all
// you need to do to complete this exercise.
//#define list


#define DECL_LIST( type, list ) \
\
    type list##_cap = 5; \
    int list##_len = 0; \
    type* list =  ( type * ) malloc( list##_cap * sizeof( type ));

#define CHECK_CAP( type, list ) \
{\
    if ( list##_len >= list##_cap ) { \
        list##_cap *= 2; \
        list = (double *) realloc( list, list##_cap * sizeof( type ) ); \
    }\
}


#define SIZE( list ) list##_len

#define FOR(index, length) for( int index = 0; index < length; index++ ) \


#define SWAP( type, item1, item2)  \
{ \
    type tmp = item1; \
    item1 = item2; \
    item2 = tmp; \
}


int main()
{
  // Make a resizable list.
  DECL_LIST( double, list );

  double val;
  while ( scanf( "%lf", &val ) == 1 ) {
    // Grow the list when needed.
    CHECK_CAP( double, list );

    // Add this item to the list
    list[ SIZE( list ) ] = val;
    SIZE( list ) += 1;
  }


  // Bubble-sort the list.
  FOR( i, SIZE( list ) )
    FOR( j, SIZE( list ) - i - 1 )
      if ( list[ j ] > list[ j + 1 ] )
        SWAP( double, list[ j ], list[ j + 1 ] );



  // Print out the resulting, sorted list, one value per line.
  FOR( i, SIZE( list ) )
    printf( "%.2f\n", list[ i ] );

  return EXIT_SUCCESS;
}