#ifndef _CODES_H_
#define _CODES_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>


#define E 0
#define T 1
#define A 2
#define I 3
#define QUOTE 4
#define N 5
#define SPACE 6
#define S 7
#define O 8
#define L 9
#define R 10
#define D 11
#define C 12
#define GREATER 13
#define LESS 14
#define BSLASH 15
#define P 16
#define M 17
#define DASH 18
#define U 19
#define PERIOD 20
#define H 21
#define F 22
#define UNDERSCORE 23
#define EQUAL 24
#define G 25
#define COLON 26
#define B 27
#define ZERO 28
#define Y 29
#define NEWLINE 30
#define OTHER 31

/**
*@param ch char
*converts symbols to code
*/
int symToCode(unsigned char ch);

/**
*@param ch cpde
*converts code to symbols
*/
int codeToSym(int code);

#endif
