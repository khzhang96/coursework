package edu.ncsu.csc316.transportation_manager.list;

import edu.ncsu.csc316.transportation_manager.highway.AdjListItem;
import edu.ncsu.csc316.transportation_manager.highway.Highway;

/**
 * Adj list
 *
 * @author khzhang
 *
 */
public class AdjList {
    /**
     * Default capacity for list
     */
    public static final int DEFAULT_CAP = 1;
    /**
     * Current max capacity of list
     */
    private int capacity = AdjList.DEFAULT_CAP;
    /**
     * List of customers
     */
    private AdjListItem[] adjList;
    /**
     * Current list size
     */
    private int size = 0;
    
    /**
     * Constructor
     */
    public AdjList() {
        this.adjList = new AdjListItem[this.capacity];
    }
    
    /**
     * Adds a customer
     *
     * @param element
     *            Customer element
     */
    public void add(Highway element) { // adds a product at the end of the list if product is not found, otherwise it
        // will match current product

        int index1 = this.lookUpCity1(element); // finds the index of product
        if (index1 == -1) { // -1 means item not found
            this.ensureCapacity(); // makes sure list has the capacity
            this.adjList[this.size] = new AdjListItem(element, 1); // assigns last index to new element
            this.size++; // increments size
        } else {
            this.adjList[index1].getHighwayList().add(element); // if item is found, increases product frequency
        }

        int index2 = this.lookUpCity2(element); // finds the index of product
        if (index2 == -1) { // -1 means item not found
            this.ensureCapacity(); // makes sure list has the capacity
            this.adjList[this.size] = new AdjListItem(element, 2); // assigns last index to new element
            this.size++; // increments size
        } else {
            this.adjList[index2].getHighwayList().add(element); // if item is found, increases product frequency
        }

    }
    
    /**
     * Ensures that there is enough capacity in the array before adding Does not do
     * anything to save processing power if there is enough capacity
     */
    public void ensureCapacity() { // sets the capacity of the current list so there is no index out of bounds or
                                   // seg fault in the array
        if (this.size + 1 >= this.capacity) { // tests size of current array
            AdjListItem[] old = this.adjList; // creates pointer for current list
            
            this.capacity = this.capacity * 2 + 1; // sets new capacity
            AdjListItem[] newArray = new AdjListItem[this.capacity]; // creates new array with larger capacity
            
            for (int i = 0; i < this.size; i++) {
                newArray[i] = old[i];
            }
            
            this.adjList = newArray;
        }
    }
    
    /**
     * Gets size of adjency list
     *
     * @return size
     */
    public int getSize() {
        return this.size;
    }
    
    /**
     * Lookup city 1 in list
     *
     * @param element
     *            Highway to lookup
     * @return index found
     */
    public int lookUpCity1(Highway element) {

        int city = element.getCity1();
        for (int i = 0; i < this.size; i++) { // sees if current product is idential to product in the list
            if (city == this.adjList[i].getCity1()) {
                return i; // return index if identical
            }
        }
        
        return -1; // return -1 if product isn't found
        
    }
    
    /**
     * Look up city 2 in list
     *
     * @param element
     *            Highway to lookup
     * @return index found
     */
    public int lookUpCity2(Highway element) {

        int city = element.getCity2();
        for (int i = 0; i < this.size; i++) { // sees if current product is idential to product in the list
            if (city == this.adjList[i].getCity1()) {
                return i; // return index if identical
            }
        }
        
        return -1; // return -1 if product isn't found
        
    }

    /**
     * (taken from my grocerystoremanager project)
     * Merge sort recursive method Orders individual elements to rebuild the list
     *
     * @param array
     *            array for sorting
     * @param first
     *            first index
     * @param middle
     *            middle index
     * @param last
     *            last index
     */
    private void merge(AdjListItem firstArray[], AdjListItem secondArray[], int first, int middle, int last) { // merges
                                                                                                               // 2
        // elements
        // into sorted
        // order
        
        // Initial indexes of first and second subarrays
        int index1; // indexes for copying
        int index2; // indexes for copying
        
        // Initial index of merged subarry array
        int completeIndex = first; // index for copying to into the finished array to return
        
        for (completeIndex = first, index1 = 0, index2 = 0; index1 < firstArray.length
                && index2 < secondArray.length; completeIndex++) {
            if (firstArray[index1].getCity1() > secondArray[index2].getCity1()) { // compares the id, and makes
                this.adjList[completeIndex] = secondArray[index2++]; // copies second array element into complete
                                                                     // array
            } else {
                this.adjList[completeIndex] = firstArray[index1++]; // copies first array element into complete
                                                                    // array
            }

        }
        
        for (int i = index1; i < firstArray.length; i++, completeIndex++) { // copies any remaining elements into the
                                                                            // complete array
            this.adjList[completeIndex] = firstArray[i];
        }
        
        for (int i = index2; i < secondArray.length; i++, completeIndex++) { // copies any remaining elements into the
                                                                             // complete array
            this.adjList[completeIndex] = secondArray[i];
        }
        
    }

    /**
     * Splits the array until each array has one element left, (first = last)
     *
     * @param first
     *            first element
     * @param last
     *            last element
     */
    private void mergeSort(int first, int last) { // helper function for merge sort. starts the splitting processes
                                                  // until array is of size 1
        if (first < last) { // checks if array is of size of 1 or greater
            // Sort first and second halves
            this.mergeSort(first, (first + last) / 2); // splits first array from first to middle
            this.mergeSort((first + last) / 2 + 1, last); // splits second array from middle + 1 to last

            // Merge the sorted halves
            
            AdjListItem firstArray[] = new AdjListItem[(first + last) / 2 - first + 1]; // creates the first array of
                                                                                        // specific
            // size
            AdjListItem secondArray[] = new AdjListItem[last - (first + last) / 2]; // creates the second array of
                                                                                    // specific
            // size
            int i = 0, j = 0;
            while (i < firstArray.length) { // creates an array of the first half of the list
                firstArray[i] = this.adjList[first + i++];
            }
            while (j < secondArray.length) { // creates an array of the second half of the list
                secondArray[j] = this.adjList[(first + last) / 2 + 1 + j++];
            }
            
            this.merge(firstArray, secondArray, first, (first + last) / 2, last); // merges new list from sorted arrays
        }
    }
    
    /**
     * Merge sort helper method to start the recursive sort
     */
    public void msort() { // starts the sort. can be used for whatever sort I choose, used to be a
                          // selection sort stub
        this.mergeSort(0, this.size - 1);
        for (int i = 0; i < this.size; i++) {
            this.adjList[i].getHighwayList().msort();
        }
    }

    /**
     * Returns a string representation of the AdjacencyList
     * in the following format, where (for each city) the Highways are
     * in sorted order by city1, then city2, then cost, then asphalt:
     *
     * AdjacencyList[
     * City 0: -> Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] ->
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] -> Highway[city1=X, city2=X,
     * cost=X.X, asphalt=X.X]
     * City 1: -> Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] ->
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X]
     * City 2: -> Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] ->
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] -> Highway[city1=X, city2=X,
     * cost=X.X, asphalt=X.X]
     * City 3: -> Highway[city1=X, city2=X, cost=X.X, asphalt=X.X]
     * ]
     *
     * @return the string representation of the AdjacencyLists
     */
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();

        buf.append("AdjacencyList[\n");

        for (int i = 0; i < this.size; i++) {
            buf.append("   ");
            buf.append(this.adjList[i].toString());
            buf.append("\n");
        }
        buf.append("]");
        return buf.toString();
    }

}
