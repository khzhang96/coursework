// Elapsed Real Time for input-4.txt:
// Elapsed Real Time for input-5.txt:

#include <stdio.h>
#include <stdbool.h>
#include <cuda_runtime.h>

// Representation for a 2D point with integer coordinates.
typedef struct {
  // Coordinates of the point.
  int x, y, num;
} Point;

// List of all points from the input
Point *ptList;

__device__ int counter;

// Number of points in the input.
int ptCount = 0;

//this is da cuda kernel
__global__ 
void countTriangles( int n, Point *ptL) {

  // Unique index for this worker, it's the index of the first point
  // in any triangles we're supposed to find.

  int a = blockDim.x * blockIdx.x + threadIdx.x;
  int numTriangles = 0;
  if (a < n) {
    int pt = a;
    // Make sure I actually have something to work on.
    if ( a < n ) {
      for (int i = 0; i < pt; i++) {
        for (int j = i + 1; j < pt; j++) {

          float dx = ptL[ pt ].x - ptL[ i ].x;
          float dy = ptL[ pt ].y - ptL[ i ].y;
          double s = sqrt(dx * dx + dy * dy);
          
          
          //double len2 = distSquared(pt1, pt3);
          dx = ptL[ pt ].x - ptL[ j ].x;
          dy = ptL[ pt ].y - ptL[ j ].y;
          double m = sqrt(dx * dx + dy * dy);


          //double len3 = distSquared(pt2, pt3);
          dx = ptL[ i ].x - ptL[ j ].x;
          dy = ptL[ i ].y - ptL[ j ].y;
          double l = sqrt(dx * dx + dy * dy);

          double temp;
          if (s > m) {
            temp = s;
            s = m;
            m = temp;
          }
          if (m > l) {
            temp = m;
            m = l;
            l = temp;
          }
          if (s > m) {
            temp = s;
            s = m;
            m = temp;
          }
        
          /*if (s == l) {
            return 1;
          } else {
            return 0;
          }*/
          int result = -1;
          if (s * 1.10 <= l) {
            result = 0;
          } else {
            result = 1;
          }
        
          if (result == 1) {
        
            atomicAdd(&counter, 1);
            //printf("%d\n", counter);
            numTriangles++;
            
          } else {
            
            
          }
        }
      } 
      
      ptL[pt].num = numTriangles;
    }
  }

  
}

// Read the list of all points
void readPoints() {
  // Use a resizable array, increasing capacity as we read
  int capacity = 10;
  ptList = (Point *) malloc( capacity * sizeof( Point ) );

  // Read points until we can't read any more.
  int x, y;
  while ( scanf( "%d%d", &x, &y ) == 2 ) {
    // Grow the point list if needed.
    if ( ptCount >= capacity ) {
      capacity *= 2;
      ptList = (Point *) realloc( ptList, capacity * sizeof( Point ) );
    }
    
    // Add this new point to the end of the list.
    ptList[ ptCount ].x = x;
    ptList[ ptCount ].y = y;
    ptCount += 1;
  }
}

// General function to report a failure and exit.
static void fail( char const *message ) {
  fprintf( stderr, "%s\n", message );
  exit( 1 );
}

int main( ) {
  readPoints();

  // ...

  // Block and grid dimensions.
  int threadsPerBlock = 250;
  // Round up.
  int blocksPerGrid = ( ptCount + threadsPerBlock - 1 ) / threadsPerBlock;

  Point *ptL;
  int *triCount = (int *)malloc(ptCount * sizeof(int));


  int *count2 = (int *)malloc(sizeof(int));

  cudaMalloc( (void **)&ptL, ptCount * sizeof( Point));

  
  //cudaMalloc( (void **)&counter, sizeof(int));

  cudaMemcpy(ptL, ptList, ptCount * sizeof(Point), cudaMemcpyHostToDevice);

  // Run our kernel on these block/grid dimensions
  countTriangles<<<blocksPerGrid, threadsPerBlock>>>( ptCount, ptL);
  if ( cudaGetLastError() != cudaSuccess )
    fail( "Failure in CUDA kernel execution." );

  // ...
  cudaMemcpy(ptList, ptL, ptCount * sizeof(Point), cudaMemcpyDeviceToHost);

  //cudaMemcpy(triCount, triC, ptCount * sizeof(int), cudaMemcpyDeviceToHost);
  //cudaMemcpy(count2, &counter, sizeof(int), cudaMemcpyDeviceToHost);
  int total = 0;
  for (int i = 0; i < ptCount; i++) {
    total += ptList[i].num;
  }

  free( ptList );

  cudaDeviceReset();

  printf("Triangles: %d\n", total);
}
