/**
 * @file style
 * @author Kevin Zhang khzhang
 * This is a file for style
 */

#include <stdio.h>

/**
 * This is a function called sleepy
 */
void sleepy() {
    //this is a loop
    for ( char c = 'a'; c <= 'z'; c++ )
    {
        printf( "%c", c );
    }
    printf( "\n" );
}

/**
 * This is a function called grumpy
 * @return returns an int
 */
int grumpy( int x )
{
    //This is a while loop
    while ( x % 7 != 0 ) {
        x++;
    } 
    return x;
}

/**
 * This is a comment called dopey
 */
void dopey() 
{
    //this is a for loop
    for ( int i = 1; i < 100; i += 10 )
    {
        int x = grumpy( i ); printf( "%d\n", x );
    }
}

/**
 * This is the main
 */
int main()
{ 
    sleepy(); 
    dopey(); 
    return 0;
}
