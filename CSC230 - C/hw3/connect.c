#include "board.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define MAXSIZE 20

int main() {
    char player;
    int rows, cols;
    printf("Rows and cols>");
    if ( scanf( "%d %d", &rows, &cols) == 0 || rows > MAXSIZE || rows < RUNLEN || cols > MAXSIZE || cols < RUNLEN ) {
        printf("Invalid board size\n");
        return 1;
    }

    char board[ rows ][ cols ];
    clearBoard( rows, cols, &board );
    printBoard( rows, cols, &board );

    player = 'X';
    int status = gameStatus( rows, cols, &board );
    while ( status == 0 ) {
        makeMove( player, rows, cols, &board);
        player = 'O';

        status = gameStatus( rows, cols, &board );
    }

    if ( status == 1 ) {
        printf( "Player %c wins", player);
        return 1;
    } else if ( status == 2 ) {
        printf( "Stalemate" );
        return 0;
    } else {
        printf( "status: %d", status); //program should never get here;
        return 1;
    }
    
}