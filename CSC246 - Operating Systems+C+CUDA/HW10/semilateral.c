#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <math.h>

#define READ 0
#define WRITE 1
// Representation for a 2D point with integer coordinates.
typedef struct {
  // Coordinates of the point.
  int x, y;
} Point;

// Print out an error message and exit.
/*static void fail( char const *message ) {
  fprintf( stderr, "%s\n", message );
  exit( 1 );
}*/

// List of all points from the input
Point *ptList;

// Number of points in the input.
int ptCount = 0;

// Read the list of all points
void readPoints() {
  // Use a resizable array, increasing capacity as we read
  int capacity = 10;
  ptList = (Point *) malloc( capacity * sizeof( Point ) );

  // Read points until we can't read any more.
  int x, y;
  while ( scanf( "%d%d", &x, &y ) == 2 ) {
    // Grow the point list if needed.
    if ( ptCount >= capacity ) {
      capacity *= 2;
      ptList = (Point *) realloc( ptList, capacity * sizeof( Point ) );
    }
    
    // Add this new point to the end of the list.
    ptList[ ptCount ].x = x;
    ptList[ ptCount ].y = y;
    ptCount += 1;
  }
}

// Return the squared distance between point i and point j in the ptList.
double distSquared( int i, int j ) {
  int dx = ptList[ i ].x - ptList[ j ].x;
  int dy = ptList[ i ].y - ptList[ j ].y;
  return sqrt(dx * dx + dy * dy);
}

int checkEquilateral(int pt1, int pt2, int pt3) {
  double len1 = distSquared(pt1, pt2);
  double len2 = distSquared(pt1, pt3);
  double len3 = distSquared(pt2, pt3);

  double s = len1;
  double m = len2;
  double l = len3;
  double temp;
  if (s > m) {
    temp = s;
    s = m;
    m = temp;
  }
  if (m > l) {
    temp = m;
    m = l;
    l = temp;
  }
  if (s > m) {
    temp = s;
    s = m;
    m = temp;
  }

  /*if (s == l) {
    return 1;
  } else {
    return 0;
  }*/
  
  if (s * 1.10 <= l) {
    return 0;
  } else {
    return 1;
  }

  
}

int main( int argc, char *argv[] ) {
  // Read in all the points
  int coreCount;
  if (argc == 1) { 
    coreCount = 1;
  } else {
    coreCount = atoi(argv[1]);
  }
  readPoints();
 
  /*308*/

  // Make a pipe for getting counts back from our workers.
  // ...
  int fd[coreCount][2]; //from children
  for (int i = 0; i < coreCount; i++) {
    if (pipe(fd[i]) == -1) {
      printf("Pipe Failed" );
      exit(1);
    }
  }
  
  //int fd2[coreCount][2]; //from children

  

  // Make all the workers, and let them solve their parts of the problem.
  // ...
  int pid[coreCount];
  int totalTriangles = 0;
  for (int f = 0; f < coreCount; f++) {

      pid[f] = fork();
      if (pid[f] < 0) {
        perror("Fork Error:");
        printf("error\n");
        exit(1);
      } else if (pid[f] > 0) { //parent
        //wait(NULL);
        int *getCount = malloc(sizeof(int));
        read(fd[f][READ], getCount, sizeof(int));
        close(fd[f][READ]);
        //printf("reading: %d %d\n", totalTriangles, *getCount);
        totalTriangles += *getCount;
        
      } else if (pid[f] == 0) { //child
        int *newCount = malloc(sizeof(int));
        for (int i = f * ptCount/coreCount; i < (f + 1) * ptCount/coreCount; i++) { //first point
          for (int j = i + 1; j < ptCount; j++) { //second point
            for (int k = j + 1; k < ptCount; k++) { //third point
              if (checkEquilateral(i,j,k) == 1) {
                *newCount = *newCount + 1;
              }
            }
          }
        }
        //printf("writing: %d\n", *newCount);
        lockf( fd[f], F_LOCK, 0 );
        write(fd[f][WRITE], newCount, sizeof(int));
        lockf( fd[f], F_ULOCK, 0 );
        close(fd[f][WRITE]);
        
        exit(1);
      }
  }

  // Get a count back from each worker
  // ... 
  
  /*for (int i = 0; i < coreCount; i++) {
    totalTriangles += triangleCounter[i];
  }*/

  // Wait for all the workers to finish.
  // ...

  // Report the total number of triangles we found.
  // ..

  
  printf("Triangles: %d\n",totalTriangles);
  
  

  

  

  /*int triangleCounter2 = 0;
  for (int i = ptCount/4; i < ptCount/2; i++) { //first point
    for (int j = i + 1; j < ptCount; j++) { //second point
      for (int k = j + 1; k < ptCount; k++) { //third point
        if (checkEquilateral(i,j,k) == 1) {
          triangleCounter2++;
        }
      }
    }
  }

  int triangleCounter3 = 0;
  for (int i = ptCount/2; i < 3 * ptCount/4; i++) { //first point
    for (int j = i + 1; j < ptCount; j++) { //second point
      for (int k = j + 1; k < ptCount; k++) { //third point
        if (checkEquilateral(i,j,k) == 1) {
          triangleCounter3++;
        }
      }
    }
  }

  int triangleCounter4 = 0;
  for (int i = 3 * ptCount/4; i < ptCount; i++) { //first point
    for (int j = i + 1; j < ptCount; j++) { //second point
      for (int k = j + 1; k < ptCount; k++) { //third point
        if (checkEquilateral(i,j,k) == 1) {
          triangleCounter4++;
        }
      }
    }
  }*/
  

  
}

