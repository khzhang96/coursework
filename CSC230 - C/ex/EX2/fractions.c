/*
 * Print the decimal approximation of a set of fractions.
 */
#include <stdio.h>
#include <stdlib.h>

/** Number of rows and columns of fractions to print. */
#define LIMIT 4

void printFraction( int num, int denom )
{
  printf("  ");

  printf("%d / %d = %1.2f", num, denom, (float)num/denom);
}

int main( void )
{
  for (int rows = 1; rows <= LIMIT; rows ++) {
    for (int columns = 1; columns <=  LIMIT; columns ++) {
      printFraction(rows, columns);
    }
    printf("\n");
  }
  return EXIT_SUCCESS;
}

