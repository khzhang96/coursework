#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main( int argc, char *argv[] )
{
    // Check command-line arguments and open the input file.
    FILE *fp;
    if ( argc != 2 ||
         ( fp = fopen( argv[ 1 ], "r" ) ) == NULL ) {
    printf( "usage: readFile <input-file>\n" );
    exit( EXIT_FAILURE );
}

    // Allocate a string with a small, initial capacity.
    int capacity = 5;
    char *buffer = malloc( capacity ); 
    
    // Number of characters we're currently using.
    int len = 0;
  
    // Read from standard input (probably redirected from a file) until we
    // reach end-of-file, storing all characters in buffer.
    // Keep enlarging the buffer as needed until it contails the
    // whole file from standard input.  This took me 9 lines of code.
    // ...
    char *word = malloc(1);;
    capacity = 5;

    while ( fscanf(fp, "%c", word ) == 1 ) {

        if ( strlen(buffer) + 1 + 1 > capacity ) {
            capacity = capacity * 2 + strlen(buffer) + 1 + 1;
            buffer = realloc( buffer, capacity );
        }
        //strcat( buffer, " ");
        strcat( buffer, word );
        
        len++;
    }

    strcat( buffer, "\0" );

    // Print out the total size of the file, in characters.
    printf( "%d\n", len );

    // Print out the whole file, it's one big string.
    printf( "%s", buffer );

    // Don't leak memory or open files.
    free( buffer );
    fclose( fp) ;

    return 0;
}