#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <mqueue.h>

#include "common.h"


static void fail( char *msg ) {
  // This program does redirection of standard output.  Good thing
  // we still have standard error around, so we can print error
  // messages even 
  fprintf( stderr, "Error: %s\n", msg );
  exit( EXIT_FAILURE );
}

char *strdup2(const char *s) {
    size_t size = strlen(s) + 1;
    char *p = malloc(size);
    if (p) {
        memcpy(p, s, size);
    }
    return p;
}


int main( int argc, char *argv[] ) {

  

  // Make a child process to run cat.
  pid_t pid = fork();
  if ( pid == -1 )
    fail( "Can't create child process" );

  if ( pid == 0 ) {
    // I'm the child.  Before replacing myself with the cat program,
    // change its environment so it reads from "input.txt" instead of
    // standard input.

    // ...

    close(STDOUT_FILENO);
    open("./output.txt", O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU);


    char *myargs[3];

    myargs[0] = strdup2("cat");
    myargs[1] = strdup2("input.txt");
    myargs[2] = NULL;

    execvp(myargs[0], myargs);

  


    // ... and writes to "output.txt" instead of standard output.

    // ...

    // Now, run cat.  Even though it thinks it's reading from standard
    // input and writing to standard output, it will really be copying these
    // files.

    // ...
  } 

  // Wait for the cat program to finish.
  wait( NULL );

  return EXIT_SUCCESS;
}