/**
 *
 */
package edu.ncsu.csc316.grocerystore2.manager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import edu.ncsu.csc316.grocerystore2.customer.Customer;
import edu.ncsu.csc316.grocerystore2.list.CustomerList;
import edu.ncsu.csc316.grocerystore2.list.ProductHashList;
import edu.ncsu.csc316.grocerystore2.order.Product;

/**
 * Grocery Store Manager class
 *
 * @author khzhang
 *
 */
public class GroceryStoreManager {
    /**
     * Customer List object
     */
    CustomerList customerList;
    /**
     * Product List object
     */
    ProductHashList productLists;

    /**
     * Constructs a new GroceryStoreManager object using the two input files of
     * customers and products
     *
     * @param pathToProductFile
     *            - the path to the product file
     * @param pathToCustomerFile
     *            - the path to the customer file
     */
    public GroceryStoreManager(String pathToProductFile, String pathToCustomerFile) {
        this.customerList = new CustomerList(); // creates new customer list
        this.productLists = new ProductHashList(); // creates new product list
        this.customerStringReader(pathToCustomerFile); // reads customer file
        this.productStringReader(pathToProductFile); // reads product file
    }

    /**
     * Reads in Customer String
     *
     * @param path
     *            file path to text file containing customer string
     */
    public void customerStringReader(String path) {

        try {

            BufferedReader buf; // creates a buffered reader
            String line; // creates a line
            String[] lines; // creates a line array for delimited line
            String id, company, state, zipcode; // creates fields to enter into customer
            buf = new BufferedReader(new FileReader(path)); // creates new buffered reader of file path

            while ((line = buf.readLine()) != null) { // tests if current line is null
                lines = line.split(","); // splits by comma

                id = lines[0]; // assigns id from first entry
                company = lines[1]; // assigns company from second entry
                state = lines[2]; // assigns state from third entry
                zipcode = lines[3]; // assigns zipcode from fourth line entry

                this.customerList.add(new Customer(id, company, state, zipcode));
            }

            buf.close();
        } catch (FileNotFoundException e) { // catches file not found exception
            e.printStackTrace();
        } catch (IOException e) { // catches IO exception
            e.printStackTrace();
        }
    }
    
    /**
     * Get all products
     *
     * @return a string with all products
     */
    public String getAllProducts() { // returns all products in string form
        return this.productLists.toString();
    }

    /**
     * Returns the list of customers sorted in ascending order by customer ID as a
     * String in the following format:
     *
     * Customer [id=C0000473, company=Wigmann's, state=DE, zipcode=45272] Customer
     * [id=C0000646, company=Super Food, state=CA, zipcode=22962] Customer
     * [id=C0000679, company=Martino's, state=SD, zipcode=05989] ... and so on
     *
     * @return the sorted list of customers
     */
    public String getCustomers() { // returns customers in string form
        this.customerList.msort();
        return this.customerList.toString();
    }

    /**
     * Returns the product as a String in the following format: Product [brand=Wolf,
     * description=soda, frequency=698]
     *
     * @param brand
     *            - the brand of the product to lookup
     * @param description
     *            - the description of the product to lookup
     * @return the full product information
     */
    public String getProduct(String brand, String description) { // gets a product by the brand a description
        return this.productLists.lookUp(brand, description).toString();
    }

    /**
     * Reads in Product String
     *
     * @param path
     *            filepath containing product string
     */
    public void productStringReader(String path) { // reads product string

        try {
            BufferedReader buf; // buffered reader
            String line; // new string line
            String[] lines; // line array that will be delimited
            String brand; // brand field
            String description; // brand description
            StringBuilder strBuf = new StringBuilder(); // creates a stringbuilder for the description

            buf = new BufferedReader(new FileReader(path)); // creates a new buffered reader with file path

            while ((line = buf.readLine()) != null) { // checks if line is null
                strBuf = new StringBuilder(); // creates string builder
                lines = line.split(" "); // split lines by space

                brand = lines[0]; // assigns first entry to brand

                for (int i = 1; i < lines.length; i++) { // appends all other words into description
                    strBuf.append(lines[i]);
                    strBuf.append(" ");
                }
                description = strBuf.toString(); // returns description

                this.productLists.add(new Product(brand, description)); // creates product ot add
            }

            buf.close();

        } catch (FileNotFoundException e) { // catches file not found exception
            e.printStackTrace();
        } catch (IOException e) { // catches io exception
            e.printStackTrace();
        }
    }

}
