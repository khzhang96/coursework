#include "board.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "board.h"

#define RUNLEN 4
#define MAXSIZE 20

void printBoard( int rows, int cols, char board[ rows ][ cols ] ) {
    for ( int rowCount = 0; rowCount < rows; rowCount++ ) {
        for ( int colCount = 0; colCount < cols; colCount++) {
            if ( board[ rowCount ][ colCount ] == '\0') {
                printf( "| " );
            } else {
                printf( "|%s", board[ rowCount ][ colCount ] );
            }           
        }
        printf( "|\n" );
    }
    for ( int colCount = 0; colCount < cols; colCount++ ) {
        printf("+-");
    }
    printf("+");
    for ( int colCount = 0; colCount < cols; colCount++ ) {
        printf(" %d", colCount - ( colCount / 10 ) * 10);
    }
    
}

void clearBoard( int rows, int cols, char board[ rows ][ cols ] ) {
    for ( int rowCount = 0; rowCount < rows; rowCount++ ) {
        for ( int colCount = 0; colCount < cols; colCount++) {
            board[ rowCount ][ colCount ] = '\0';
        }
    }
    
}

int gameStatus( int rows, int cols, char board[ rows ][ cols ] ) {
    for ( int rowCount = 0; rowCount < rows; rowCount++ ) {
        for (int colCount = 0; colCount < cols; colCount++ ) {
            if (winner( rows, cols, board[ rows ][ cols ], rowCount, colCount, 0, 1 ) == 1 || //checks for right
                winner( rows, cols, board[ rows ][ cols ], rowCount, colCount, 1, 0 ) == 1 || //checks for down
                winner( rows, cols, board[ rows ][ cols ], rowCount, colCount, -1, 0 ) == 1 || //checks for up;
                winner( rows, cols, board[ rows ][ cols ], rowCount, colCount, 0, -1 ) == 1 || //checks for left
                winner( rows, cols, board[ rows ][ cols ], rowCount, colCount, -1, 1 ) == 1 || //checks for up right
                winner( rows, cols, board[ rows ][ cols ], rowCount, colCount, -1, -1 ) == 1 || //checks for up left
                winner( rows, cols, board[ rows ][ cols ], rowCount, colCount, 1, 1 ) == 1 || //checks for down right
                winner( rows, cols, board[ rows ][ cols ], rowCount, colCount, 1, -1 ) == 1 )  //checks for down
            {
                return 1;
            }
        }
    }

    for ( int rowCount = 0; rowCount < rows; rowCount++ ) {
        for ( int colCount = 0; colCount < cols; colCount++ ) {
            if ( board[rowCount][colCount] == '\0' ) {
                return 0; //empty space means game isn't over'
            }
        }
    }
    return 2; //board is full since it didn't detect null
}

/**
    Return true if there's a winning sequence of markers starting at
    the given board location, startRow, startCol location, either a
    sequence of X characters or O characters.  The dRow and dCol
    parameters indicate what direction to look, with dRow giving
    change-in-row for each step and dCol giving the change-in-column.
    For example, a dRow of -1 and a dCol of 1 would loo for a sequence
    of markers starting from the given start location and diagonally up
    to the right.
    @param rows Number of rows the board has.
    @param cols Number of columns the board has.
    @param board The game board.
    @param startRow Row start position to look for a win.
    @param startCol Column start position to look for a win.
    @param dRow Direction to move vertically looking for a win.
    @param dCol Direction to move horizontally looking for a win.
    @return true if there's a win in the given board location.
 **/

bool winner( int rows, int cols, char board[ rows ][ cols ],
             int startRow, int startCol, int dRow, int dCol ) {
    // Number of X and O symbols in this sequence of locations
    int xcount = 0, ocount = 0;

    // Walk down the sequence of board spaces.
    for ( int k = 0; k < RUNLEN; k++ ) {

        // Figure out its row and column index and make sure it's on the board.
        int r = startRow + k * dRow;
        int c = startCol + k * dCol;

        if ( r < 0 || r >= rows || c < 0 || c >= cols ) {
            return false;
        }

        // Count an X or an O if it's one of those.

        if ( board[ r ][ c ] == 'X' ) {
            xcount++;
        } else if ( board[ r ][ c ] == 'O' ) {
            ocount++;
        }

    }

    // We have a winner if it's all Xs or Os.
    return xcount == RUNLEN || ocount == RUNLEN;

}

void makeMove( char player, int rows, int cols, char board[ rows ][ cols ] ) {
    int colNum = -1;
    while ( true ) {
        if ( scanf( "Please input a column number >%d", &colNum ) != 1 ) {
            printf( "Invalid Move\n" ); //not an int
        } else if ( colNum <= 0 || colNum > cols ) {
            printf( "Invalid Move\n" ); //out of bounds
        } else if ( board[ 0 ][ colNum - 1] != '\0') {
            printf( "Invalid Move\n"); //col full
        } else { //theres space
            break;
        }
        //keeps on going until theres valid input
    }
    bool status = 0;
    for ( int rowNum = rows - 1; rowNum >= 0; rowNum -- ) {
        if ( board[ rowNum ][ colNum] != '\0' ) {
            board[ rowNum ][ colNum ] = player;
            status = 1;
            break; 
        }
    }
    
    if ( status == 0 ) { //move hasn't been made, yet program made it out of while loop
        printf( "Program should have never have gotten here" );
    }
    //move done.

}