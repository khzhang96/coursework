#include <stdio.h>

void incr(int *p) { /* pass in a pointer (address) to an interger which is */
  /* p is a pointer*/
  printf( "what is p:%x\n",p);

  // The content of this address is represented by *p , *p=100 

  *p= *p+1;
}

int* ret(int *p) {  // passing a pointer, return the pointer
  return p;
} 

int ret1(int *p) { // passing a pointer , ret contents of the pointer
  /* at this moment, p is pointer. *p is the content */
  return *p;
}

void main() {
  int a=100;
  printf("valude of a:%d\n",a);
  printf("the address of a %x\n",&a);
  incr(&a);  /* &a is the address of a */
  /* after I come out, the address content gets changed */
  int *p = ret(&a);
  printf("%x\n",p);

  int b = ret1(&a);
  printf("%d\n",b);
}

