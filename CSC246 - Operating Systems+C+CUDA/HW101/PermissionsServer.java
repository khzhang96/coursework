import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Scanner;
import java.net.InetAddress;

public class PermissionsServer {
    /** stores each table entry*/
    public static class Table {
        public int p;
        public String name;
        public int q;
        public String obj;
        public String cmd;

        public Table(int argP, String argName, int argQ, String argObj, String argCmd) {
            p = argP;
            q = argQ;

            name = argName;
            obj = argObj;

            cmd = argCmd;
        }
    }
    /** Server port*/
    public static final int SERVER_PORT = 26027;
    // Your port numbers are: 26026 and 26027

    public static void main(String[] args) throws Exception {
        Scanner usrSc;
        Scanner objSc;
        Scanner tblSc;
        File usrFile = new File("users.txt");
        File objFile = new File("objects.txt");
        File tblFile = new File("table.txt");
        
        usrSc = new Scanner(usrFile);
        objSc = new Scanner(objFile);
        tblSc = new Scanner(tblFile);

        //reads in users
        ArrayList<String> users = new ArrayList<String>();
        while ((usrSc.hasNextLine())) {
            users.add(usrSc.nextLine());
        }
        //reads in objects
        ArrayList<String> objs = new ArrayList<String>();
        while (objSc.hasNextLine()) {
            objs.add(objSc.nextLine());
        }
        //reads in table
        ArrayList<Table> tbls = new ArrayList<Table>();
        int p, q;
        String name;
        tblSc.useDelimiter("\\s+");
        int m = 0;
        while (tblSc.hasNext()) {
            p = tblSc.nextInt();
            q = tblSc.nextInt();
            name = tblSc.next();
            System.out.println(m + ": " + p + " " + q + " " + name);
            m++;
            tbls.add(new Table(p, users.get(p), q, objs.get(q), name));
        }

        usrSc.close();
        objSc.close();
        tblSc.close();

        DatagramSocket serverSocket = new DatagramSocket(SERVER_PORT);
        byte[] receiveData = new byte[2048];
        byte[] sendData = new byte[2048];

        while (true) {
            try {
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                serverSocket.receive(receivePacket);
                String sentence = (new String(receivePacket.getData())).trim();
                String[] splitSentence = sentence.split("\\s");

                boolean found = false;
                for (int i = 0; i < tbls.size(); i++) {
                    if (splitSentence[0].equals(tbls.get(i).name) && splitSentence[1].equals( tbls.get(i).obj)
                            && splitSentence[2].equals(tbls.get(i).cmd)) {
                        found = true;
                        String sendString = "Access granted";
                        sendData = sendString.getBytes();
                        break;
                    } else {
                        found = false;
                    }
                }

                if (found == false) {
                    String sendString = "Access denied";
                    sendData = sendString.getBytes();
                }

                InetAddress IPAddress = receivePacket.getAddress();
                int port = receivePacket.getPort();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                serverSocket.send(sendPacket);
            } catch (SocketTimeoutException e) {
                System.out.println("No response");
            } catch (IOException e) {
                System.err.println("Error in communicating with the server" + e);
            }

        }
    }
}
