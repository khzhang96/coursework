#include <stdio.h>

// Using a character array to hold a sequence of characters entered
// by the user.
int main( void )
{
  printf( "What's your name: " );

  // Storage for a name, as an array of characters without a null
  // marking the end.
  char name[ 10 ];
  int len = 0;
  
  char ch;
  
  while ( (ch = getchar()) != EOF || ch != '/n' || len < 9 )
    {
       name[len] = ch;      
    }
    

  // ...

  // Exit successfully when finished.
  // ...
}
