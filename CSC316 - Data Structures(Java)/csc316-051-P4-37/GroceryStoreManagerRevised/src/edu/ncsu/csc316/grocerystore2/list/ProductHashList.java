/**
 *
 */
package edu.ncsu.csc316.grocerystore2.list;

import edu.ncsu.csc316.grocerystore2.order.Product;

/**
 * Customer List class
 *
 * @author khzhang
 *
 */
public class ProductHashList {
    /**
     * Default capacity for list
     */
    public static final int DEFAULT_CAP = 7919;
    /**
     * Current max capacity of list
     */
    private int capacity = ProductHashList.DEFAULT_CAP;
    /**
     * List of customers
     */
    private ProductList[] productLists;

    /**
     * Constructor
     */
    public ProductHashList() {
        this.productLists = new ProductList[this.capacity];
    }

    /**
     * Adds an element to the hashtable
     *
     * @param element
     *            element to be added
     */
    public void add(Product element) {
        int key = this.bitShift(element.getBrand());

        if (this.productLists[key] == null) {
            this.productLists[key] = new ProductList();
            this.productLists[key].add(element);
        } else if (this.productLists[key].getSize() == 0) {
            this.productLists[key].add(element);
        } else {
            this.productLists[key].add(element);
        }
        
    }
    
    /**
     * Cyclic shift
     * From class notes about hashing
     *
     * @param s
     *            String to hash
     * @return integer
     */
    public int bitShift(String s) {
        int h = 0;
        for (int i = 0; i < s.length(); i++) {
            h = h << 5;
            
            h += s.charAt(i);
            
            if (h < 0) {
                h = -h;
            }
        }
        
        h = h % ProductHashList.DEFAULT_CAP;
        // System.out.println(h);
        return h;
    }
    
    /**
     * Lookup a product based on brand and description
     *
     * @param brand
     *            brand to be lookedup
     * @param description
     *            description to be looked up
     * @return Product
     */
    public Product lookUp(String brand, String description) {
        int key = this.bitShift(brand);
        return this.productLists[key].lookUpProduct(brand, description);
    }

    /**
     * Returns a string for the customer
     *
     * @return returns a string representation of the entire list
     */
    @Override
    public String toString() {
        StringBuffer newBuf = new StringBuffer(); // creates a string buffer for easy concatenation
        for (int i = 0; i < ProductHashList.DEFAULT_CAP; i++) {
            if (this.productLists[i] != null) {
                newBuf.append(this.productLists[i].toString()); // adds individual to string outputs into a much larger
            }
        }

        return newBuf.toString(); // returns the outcome
    }
    
}
