package edu.ncsu.csc316.transportation_manager.manager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import edu.ncsu.csc316.transportation_manager.heap.MinHighwayHeap;
import edu.ncsu.csc316.transportation_manager.highway.Highway;
import edu.ncsu.csc316.transportation_manager.kruskal.Kruskal;
import edu.ncsu.csc316.transportation_manager.list.AdjList;
import edu.ncsu.csc316.transportation_manager.list.HighwayList;

/**
 * Transportation Manager
 *
 * @author khzhang
 *
 */
public class TransportationManager {
    AdjList hwyList;
    HighwayList basicList;
    MinHighwayHeap costHeap;
    MinHighwayHeap asphaltHeap;
    
    /**
     * Constructs a new TransportationManager
     *
     * @param pathToFile
     *            the path to the file that contains the set of highways in the
     *            graph
     */
    public TransportationManager(String pathToFile) {
        this.readFile(pathToFile);
    }

    /**
     * Returns a string representation of the AdjacencyList
     * in the following format, where (for each city) the Highways are
     * in sorted order by city1, then city2, then cost, then asphalt:
     *
     * AdjacencyList[
     * City 0: -> Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] ->
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] -> Highway[city1=X, city2=X,
     * cost=X.X, asphalt=X.X]
     * City 1: -> Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] ->
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X]
     * City 2: -> Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] ->
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X] -> Highway[city1=X, city2=X,
     * cost=X.X, asphalt=X.X]
     * City 3: -> Highway[city1=X, city2=X, cost=X.X, asphalt=X.X]
     * ]
     *
     * @return the string representation of the AdjacencyLists
     */
    public String getAdjacencyList() {

        return this.hwyList.toString();
        
    }

    /**
     * Gets a list of all items in heap
     *
     * @param type
     *            type of heap
     * @return string of heap
     */
    public String getMinHeaps(String type) {
        if (type.equals("COST")) {
            /*
             * while (this.costHeap.isEmpty() == 0) {
             * System.out.println(this.costHeap.deleteMin().toString());
             * }
             */
            return this.costHeap.toString();
        } else if (type.equals("ASPHALT")) {

            return this.asphaltHeap.toString();
        } else {
            return "";
        }
    }
    
    /**
     * Returns a string representation of the list of Highways contained in the
     * minimum spanning set of Highways. The returned string is in the following
     * format,
     * where the Highways are in sorted order by city1, city2, then cost, then
     * asphalt:
     *
     * List[
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X],
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X],
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X]
     * ]
     *
     * @param type
     *            the type ("COST" or "ASPHALT") of field to minimize
     * @return a string representation of the minimum spanning set of Highways
     */
    public String getMinimumHighways(String type) {
        // this.costHeap.deleteMin();
        Kruskal k;
        if (type.equals("COST")) {
            // System.out.println(this.costHeap.toString());
            k = new Kruskal(this.costHeap, this.hwyList);
            return k.toString();
        } else if (type.equals("ASPHALT")) {
            // System.out.println(this.asphaltHeap.toString());
            k = new Kruskal(this.asphaltHeap, this.hwyList);

            return k.toString();
        } else {
            return "";
        }

    }

    /**
     * City1[whitespace]City2[whitespace]Cost[whitespace]Distance
     * 2 0 7.0 77.0
     * 3 2 12.0 122.0
     * 0 3 14.0 144.0
     * 1 0 5.0 101.0
     * 3 1 10.0 66.0
     * 1 2 6.0 55.0
     *
     * 0 3 14.0 144.0
     * 1 0 5.0 101.0
     * 1 2 6.0 55.0
     * 2 0 7.0 77.0
     * 3 1 10.0 66.0
     * 3 2 12.0 122.0
     *
     * @param pathToFile
     *            filepath
     */
    public void readFile(String pathToFile) {
        try {

            BufferedReader buf; // creates a buffered reader
            int city1;
            int city2;
            double cost;
            double distance;
            buf = new BufferedReader(new FileReader(pathToFile)); // creates new buffered reader of file path
            String line = new String();
            String[] lines = new String[4];
            
            this.costHeap = new MinHighwayHeap("COST");
            this.asphaltHeap = new MinHighwayHeap("ASPHALT");
            this.basicList = new HighwayList();

            this.hwyList = new AdjList();
            System.out.println("New");
            while ((line = buf.readLine()) != null) { // tests if current line is null
                System.out.println(line);
                lines = line.split(" "); // splits by space

                city1 = Integer.parseInt(lines[0]); // assigns id from first entry
                city2 = Integer.parseInt(lines[1]); // assigns company from second entry
                cost = Double.parseDouble(lines[2]); // assigns state from third entry
                distance = Double.parseDouble(lines[3]); // assigns zipcode from fourth line entry
                // System.out.print(city1);
                // System.out.print(" ");
                // System.out.print(city2);
                // System.out.print(" ");
                // System.out.print(cost);
                // System.out.print(" ");
                // System.out.println(distance);

                Highway newHighway = new Highway(city1, city2, cost, distance);
                // System.out.println(newHighway.toString());
                // System.out.println();
                this.hwyList.add(newHighway);
                this.costHeap.insert(newHighway);
                this.basicList.add(newHighway);
                this.asphaltHeap.insert(newHighway);
                
            }
            this.hwyList.msort();

            buf.close();
        } catch (FileNotFoundException e) { // catches file not found exception
            e.printStackTrace();
        } catch (IOException e) { // catches IO exception
            e.printStackTrace();
        }
    }
}
