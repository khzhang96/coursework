data sat_iq;
infile 'SAT_IQ.txt' firstobs=2;
input sat iq;
run;

proc gplot data=sat_iq;
plot sat*iq;
run;

proc reg data=sat_iq alpha=.05;
model sat=iq/clb;
plot residual.*sat; /*checking for linearity*/
plot residual.*predicted.; /*cheking for constant variance*/
plot nqq.*residual.; /*checking for normality*/
run;

data missing;
input sat iq;
datalines;
. 100
run;

data sat_iq;
set sat_iq missing;
run;

proc reg data=sat_iq;
model sat=iq;
output out=out1 r=resid p=pred ucl=pihigh lcl=pilow uclm=cihigh lclm=cilow stdp=stdmean;
run;

data intervals;
set out1;
if sat=.;
run;

proc print data=intervals;
run;


/*
proc reg data=sat_iq;
model IQ=SAT;
output out=reg p=pred r=resid;
run;

proc univariate data=sat_iq plots;
var resid;
run;

proc gplot data=reg;
plot resid*SAT;
plot resid*pred;
run;*/


