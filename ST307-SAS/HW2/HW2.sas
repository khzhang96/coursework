data diet;
length type$ 10;
infile 'diet_comma.txt' firstobs=10 DLM = ',' DSD;
input alter$ excer bodytype$ bmi;
run;

proc print data=diet;
run;

proc univariate data=diet;
histogram excer;
run;

proc boxplot data=diet;
plot excer*bodytype;
run;

proc sort data=diet out=diet2;
	by bmi;
run;

proc boxplot data=diet2;
plot excer*bmi;
run;
