package edu.ncsu.csc316.transportation_manager.highway;

/**
 * Highway class
 *
 * @author khzhang
 *
 */
public class Highway {
    int city1;
    int city2;

    double cost;
    double asphalt;
    
    /**
     * Constructs a Highway with the given information
     *
     * @param city1
     *            city1 of the highway
     * @param city2
     *            city2 of the highway
     * @param cost
     *            cost of building the highway
     * @param asphalt
     *            amount (in miles) of asphalt needed to build the highway
     */
    public Highway(int city1, int city2, double cost, double asphalt) {
        this.city1 = city1;
        this.city2 = city2;
        this.cost = cost;
        this.asphalt = asphalt;
    }
    
    /**
     * Gets asphalt
     *
     * @return the asphalt
     */
    public double getAsphalt() {
        return this.asphalt;
    }
    
    /**
     * gets city1
     *
     * @return the city1
     */
    public int getCity1() {
        return this.city1;
    }
    
    /**
     * Gets city2
     *
     * @return the city2
     */
    public int getCity2() {
        return this.city2;
    }
    
    /**
     * Gets the cost
     *
     * @return the cost
     */
    public double getCost() {
        return this.cost;
    }

    /**
     * Returns a string representation of the Highway
     * in the format:
     *
     * Highway[city1=X, city2=X, cost=X.X, asphalt=X.X]
     *
     * @return the string representation of the highway
     */
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        
        buf.append("Highway[city1=");
        buf.append(this.city1);
        buf.append(", city2=");
        buf.append(this.city2);
        buf.append(", cost=");
        buf.append(this.cost);
        buf.append(", asphalt=");
        buf.append(this.asphalt);
        buf.append("]");
        
        return buf.toString();
    }
}
