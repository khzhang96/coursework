#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <math.h>

// Print out an error message and exit.
static void fail( char const *message ) {
  fprintf( stderr, "%s\n", message );
  exit( 1 );
}

// Representation for a 2D point with integer coordinates.
typedef struct {
  // Coordinates of the point.
  int x, y, taken;
} Point;

// Maximum number of points we can store on ptList.  For this program,
// the list is allocated at a fixed size at the start of execution.
// This letsd so workers start working on Points as soon as we read
// them in, without having to worry about the list moving in memory
// (if, say, it had to get resized).
#define INIT_NUM 100

// List of all points from the input
Point *ptList;

// Number of points in the input.
int ptCount = 0;

// Total number of approximately equilateral triangles we've found
int total = 0;

int coreCount;
int f;

int triangleCount;

sem_t calc;
sem_t read;
sem_t work;
sem_t reaoc;


// Read the lsit of points.
void readPoints() {
  // Read points until we can't read any more.
  int x,y;
  int capacity = INIT_NUM;
  while ( scanf( "%d%d", &x, &y ) == 2 ) {
    if ( ptCount >= capacity - 1 ) {
      sem_wait(&work);
      capacity *= 100;
      ptList = (Point *) realloc( ptList, capacity * sizeof( Point ) );
      sem_post(&work);

    }
    // Append the point we just read to the list.
    sem_post(&work);
    ptList[ ptCount ].x = x;
    ptList[ ptCount ].y = y;
    ptList[ ptCount ].taken = 0;
    //sem_wait(&read2);
    ptCount++;
    sem_post(&work);
    //sem_post(&read2);  
    //printf("read\n");

  }
}

void readThreePoints() {

  ptList = (Point *) malloc( INIT_NUM * sizeof( Point ) );
  sem_wait(&work);
  int x, y;
  scanf( "%d%d", &x, &y );
  ptList[ ptCount ].x = x;
  ptList[ ptCount ].y = y;
  ptList[ ptCount ].taken = 1;
  //sem_wait(&read2);
  ptCount++;
  //sem_post(&read2);
  //printf("read 1\n");
  
  scanf( "%d%d", &x, &y );
  ptList[ ptCount ].x = x;
  ptList[ ptCount ].y = y;
  ptList[ ptCount ].taken = 1;
  //sem_wait(&read2);
  ptCount++;
  //sem_post(&read2);  
  //printf("read 1\n");  

  scanf( "%d%d", &x, &y );
  ptList[ ptCount ].x = x;
  ptList[ ptCount ].y = y;
  ptList[ ptCount ].taken = 0;
  //sem_wait(&read2);
  ptCount++;
  //sem_post(&read2);  
  //printf("read 1\n");  
  sem_post(&work);
}

// Return the squared distance between point i and point j in the ptList.
float distSquared( int i, int j ) {
  sem_wait(&work);
  float dx = ptList[ i ].x - ptList[ j ].x;
  float dy = ptList[ i ].y - ptList[ j ].y;
  sem_post(&work);
  return sqrt(dx * dx + dy * dy);
}

int checkEquilateral(int pt1, int pt2, int pt3) {
  //printf("hello");
  float s = distSquared(pt1, pt2);
  float m = distSquared(pt1, pt3);
  float l = distSquared(pt2, pt3);
  float temp;
  if (s > m) {
    temp = s;
    s = m;
    m = temp;
  }
  if (m > l) {
    temp = m;
    m = l;
    l = temp;
  }
  if (s > m) {
    temp = s;
    s = m;
    m = temp;
  }

  /*if (s == l) {
    return 1;
  } else {
    return 0;
  }*/
  
  
  

  if (s * 1.10 <= l) {
    
    return 0;
  } else {
    //printf("points: %d %d %d\n", pt1, pt2, pt3);
    //printf("hello: %f %f %f\n", s, m, l);
    return 1;
  }


  
}

int getWork() {
  
  
  sem_wait(&read); 
  int ptCount2 = ptCount;
  sem_post(&read);

  sem_wait(&work);
  for (int pt = 2; pt < ptCount2; pt++) {
    //sem_wait(&read);
    if (ptList[pt].taken == 0) {
      //printf("work pt:%d\n", pt);
      ptList[pt].taken = 1;
      sem_post(&work);
      return pt;
      
    }
    //sem_post(&read);
  }

  sem_post(&work);
  
  

  //end of list
  return -1;
}


/** Start routine for each worker. */
void *workerRoutine( void *arg ) {
  int pt;

  //int localCount = 0;
  while (1) {
    pt = getWork();

    //printf("routine pt: %d\n", pt);
    if (pt == -1) {
      return NULL;
    }
    //printf("In while loop \n");

    for (int i = 0; i < pt; i++) {
      for (int j = i + 1; j < pt; j++) {
        //printf("hello");
        if (checkEquilateral(pt,i,j) == 1) {
          sem_wait(&calc);
          triangleCount++;
          sem_post(&calc);
          
        }
      }
    }
    
  }
  

  return NULL;
}
  
int main( int argc, char *argv[] ) {
  int workers = 4;

  triangleCount = 0;
  if ( argc != 2 ||
       sscanf( argv[ 1 ], "%d", &workers ) != 1 ||
       workers < 1 )
    fail( "usage: semilateral <workers>" );

  // Allocate our statically-sized list for points.
  

  // Make each of the workers.
  sem_init(&read, 0, 1);
  sem_init(&work, 0, 1); 
  sem_init(&calc, 0, 1); 
  sem_init(&reaoc, 0 ,1);

  readThreePoints(); //read three points first;
  pthread_t worker[ workers ];
  for ( int i = 0; i < workers; i++ ) {
    pthread_create(&worker[ i ], NULL, workerRoutine, NULL);
  }

  // Then, start getting work for them to do.
  readPoints();
  

  // Wait until all the workers finish.
  for ( int i = 0; i < workers; i++ ) {
    pthread_join(worker[ i ], NULL);
  }
    // ...

  // Report the total and release the semaphores.
  printf( "Triangles: %d\n", triangleCount );
  sem_destroy(&read); 
  sem_destroy(&work);  
  sem_destroy(&calc); 
  sem_destroy(&reaoc); 
  
  return EXIT_SUCCESS;
}