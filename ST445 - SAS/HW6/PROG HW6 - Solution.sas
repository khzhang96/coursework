/*
Author: Jonathan Duggins
Date: 2017-10-13
Purpose: PROG HW #6 Solution
*/

libname hw6 "S:\Documents\CURRENT COURSES\ST 445\Assignments\Duggins\PROG HW6";

*Section 1;

data hw6.design;
   do block = 1 to 5;
      do treatment = 'A', 'B', 'C', 'D';
         do replicate = 1 to 3;
            output;
         end;
      end;
   end;
run;

data hw6.design_rand_0;
   do block = 1 to 5;
      do treatment = 'A', 'B', 'C', 'D';
         do replicate = 1 to 3;
            rannum = rand('uniform');
            output;
         end;
      end;
   end;
run;

proc sort data = hw6.design_rand_0 out = hw6.rcbd_0(drop = rannum);
   by block rannum;
run;

/*
 The values of BLOCK, TREATMENT, and REPLICATE stay the same but because
 the values of RANNUM change we get a different sort order each time
 we submit the code.

 This would correspond to generating multiple RCBD rather than letting
 us regenerate the same RCBD. Thus our current approach lacks reproducability.
*/

data hw6.design_rand_seed;
   call streaminit(12345);
   do block = 1 to 5;
      do treatment = 'A', 'B', 'C', 'D';
         do replicate = 1 to 3;
            rannum = rand('uniform');
            output;
         end;
      end;
   end;
run;

proc sort data = design_rand_seed out = rcbd_seed(drop = rannum);
   by block rannum;
run;

/*
  Changing the value of seed from 12345 to a different positive number
  will change the sequence of random numbers we get, but it will still
  be repeateable. Thus, we have a randomized and repeatable design.
*/

************************************************************************;

*Section 2;


*Sort data by LT and DAM for creating summary statistics;
*Only keep relevant variables;
proc sort data = st555.fish(drop = lat: long:)  out = fish;
   by lt dam;
run;

*Compute summary statistics using BY-group processing - NOT a CLASS statement!;
*Save summary statistics for merging back with original data;
proc means data = fish mean median;
   by lt dam;
   ods output summary = statz(drop = vname: label_:);
run;

*Create final data set;
data fish2(drop = i);
   *Use formats to control decimal places AND to set variable order in the PDV;
   format name $25. lt 1. dam 1. 
           hg 5.3  hg_mean  hg_avg_dev_raw  hg_avg_dev_pct  hg_median  hg_med_dev_raw  hg_med_dev_pct 7.2
            n 5.3   n_mean   n_avg_dev_raw   n_avg_dev_pct   n_median   n_med_dev_raw   n_med_dev_pct 7.2
          elv 5.3 elv_mean elv_avg_dev_raw elv_avg_dev_pct elv_median elv_med_dev_raw elv_med_dev_pct 7.2
           sa 5.3  sa_mean  sa_avg_dev_raw  sa_avg_dev_pct  sa_median  sa_med_dev_raw  sa_med_dev_pct 7.2
            z 5.3   z_mean   z_avg_dev_raw   z_avg_dev_pct   z_median   z_med_dev_raw   z_med_dev_pct 7.2
           st 5.3  st_mean  st_avg_dev_raw  st_avg_dev_pct  st_median  st_med_dev_raw  st_med_dev_pct 7.2
           da 5.3  da_mean  da_avg_dev_raw  da_avg_dev_pct  da_median  da_med_dev_raw  da_med_dev_pct 7.2
           rf 5.3  rf_mean  rf_avg_dev_raw  rf_avg_dev_pct  rf_median  rf_med_dev_raw  rf_med_dev_pct 7.2
           fr 5.3  fr_mean  fr_avg_dev_raw  fr_avg_dev_pct  fr_median  fr_med_dev_raw  fr_med_dev_pct 7.2
   ;
   *Set appropriate labels;
   label name = 'Lake Name'
         lt = 'Lake Type'
         dam = 'Dam Present (1 = Yes)'
         hg = 'Mercury Level (ppm)'
         hg_mean = 'Average Mercury Level (ppm) Within Lake Type and Dam Present'
         hg_avg_dev_raw = 'Absolute Difference from Average Mercury Level (ppm)'
         hg_avg_dev_pct = 'Percentage Difference from Average Mercury Level (ppm)'
         hg_median = 'Median Mercury Level (ppm) Within Lake Type and Dam Present'
         hg_med_dev_raw = 'Absolute Difference from Median Mercury Level (ppm)'
         hg_med_dev_pct = 'Percentage Difference from Median Mercury Level (ppm)'
         n = '# of Fish'
         n_mean = 'Average # of Fish Within Lake Type and Dam Present'
         n_avg_dev_raw = 'Absolute Difference from Average # of Fish'
         n_avg_dev_pct = 'Percentage Difference from Average # of Fish'
         n_median = 'Median # of Fish Within Lake Type and Dam Present'
         n_med_dev_raw = 'Absolute Difference from Median # of Fish'
         n_med_dev_pct = 'Percentage Difference from Median # of Fish'
         elv = 'Elevation (ft.)'
         elv_mean = 'Average Elevation (ft.) Within Lake Type and Dam Present'
         elv_avg_dev_raw = 'Absolute Difference from Average Elevation (ft.)'
         elv_avg_dev_pct = 'Percentage Difference from Average Elevation (ft.)'
         elv_median = 'Median Elevation (ft.) Within Lake Type and Dam Present'
         elv_med_dev_raw = 'Absolute Difference from Median Elevation (ft.)'
         elv_med_dev_pct = 'Percentage Difference from Median Elevation (ft.)'
         sa = 'Surface Area (acres)'
         sa_mean = 'Average Surface Area (acres) Within Lake Type and Dam Present'
         sa_avg_dev_raw = 'Absolute Difference from Average Surface Area (acres)'
         sa_avg_dev_pct = 'Percentage Difference from Average Surface Area (acres)'
         sa_median = 'Median Surface Area (acres) Within Lake Type and Dam Present'
         sa_med_dev_raw = 'Absolute Difference from Median Surface Area (acres)'
         sa_med_dev_pct = 'Percentage Difference from Median Surface Area (acres)'
         z = 'Max Depth (ft.)'
         z_mean = 'Average Max Depth (ft.) Within Lake Type and Dam Present'
         z_avg_dev_raw = 'Absolute Difference from Average Max Depth (ft.)'
         z_avg_dev_pct = 'Percentage Difference from Average Max Depth (ft.)'
         z_median = 'Median Max Depth (ft.) Within Lake Type and Dam Present'
         z_med_dev_raw = 'Absolute Difference from Median Max Depth (ft.)'
         z_med_dev_pct = 'Percentage Difference from Median Max Depth (ft.)'
         st = 'Lake Stratification Indicator'
         st_mean = 'Average Lake Stratification Indicator Within Lake Type and Dam Present'
         st_avg_dev_raw = 'Absolute Difference from Average Lake Stratification Indicator'
         st_avg_dev_pct = 'Percentage Difference from Average Lake Stratification Indicator'
         st_median = 'Median Lake Stratification Indicator Within Lake Type and Dam Present'
         st_med_dev_raw = 'Absolute Difference from Median Lake Stratification Indicator'
         st_med_dev_pct = 'Percentage Difference from Median Lake Stratification Indicator'
         da = 'Drainage Area'
         da_mean = 'Average Drainage Area Within Lake Type and Dam Present'
         da_avg_dev_raw = 'Absolute Difference from Average Drainage Area'
         da_avg_dev_pct = 'Percentage Difference from Average Drainage Area'
         da_median = 'Median Drainage Area Within Lake Type and Dam Present'
         da_med_dev_raw = 'Absolute Difference from Median Drainage Area'
         da_med_dev_pct = 'Percentage Difference from Median Drainage Area'
         rf_mean = 'Average Runoff Factor Within Lake Type and Dam Present'
         rf_avg_dev_raw = 'Absolute Difference from Average Runoff Factor'
         rf_avg_dev_pct = 'Percentage Difference from Average Runoff Factor'
         rf_median = 'Median Runoff Factor Within Lake Type and Dam Present'
         rf_med_dev_raw = 'Absolute Difference from Median Runoff Factor'
         rf_med_dev_pct = 'Percentage Difference from Median Runoff Factor'
         fr_mean = 'Average Flushing Rate (#/year) Within Lake Type and Dam Present'
         fr_avg_dev_raw = 'Absolute Difference from Average Flushing Rate (#/year)'
         fr_avg_dev_pct = 'Percentage Difference from Average Flushing Rate (#/year)'
         fr_median = 'Median Flushing Rate (#/year) Within Lake Type and Dam Present'
         fr_med_dev_raw = 'Absolute Difference from Median Flushing Rate (#/year)'
         fr_med_dev_pct = 'Percentage Difference from Median Flushing Rate (#/year)'
   ;

   *Merge summary data onto original data by LT and DAM;
   merge fish statz;
   by lt dam;

   *Set up arrays for computing deviations;
   *Start with three arrays to hold: original data, means, and medians;
   *Use asterisks to increase program flexibility - as new summary variables
    are added, the array dimensions do not have to be manually updated;
   *Note, these variables __ALREADY__ exist in the PDV;

   array obs[*] hg n elv sa z st da rf fr;
   array avg[*] hg_mean n_mean elv_mean sa_mean z_mean st_mean da_mean rf_mean fr_mean;
   array med[*] hg_median n_median elv_median sa_median z_median st_median da_median rf_median fr_median;

   *Create arrays to hold the differences from the mean and median;
   *Note, these variables do __NOT__ already exist in the PDV - you are 
    adding them to the PDV via the ARRAY statement;

   array avg_raw[*] hg_avg_dev_raw n_avg_dev_raw elv_avg_dev_raw sa_avg_dev_raw
                    z_avg_dev_raw st_avg_dev_raw da_avg_dev_raw rf_avg_dev_raw fr_avg_dev_raw;
   array avg_pct[*] hg_avg_dev_pct n_avg_dev_pct elv_avg_dev_pct sa_avg_dev_pct
                    z_avg_dev_pct st_avg_dev_pct da_avg_dev_pct rf_avg_dev_pct fr_avg_dev_pct;

   array med_raw[*] hg_med_dev_raw n_med_dev_raw elv_med_dev_raw sa_med_dev_raw 
                    z_med_dev_raw st_med_dev_raw da_med_dev_raw rf_med_dev_raw fr_med_dev_raw;
   array med_pct[*] hg_med_dev_pct n_med_dev_pct elv_med_dev_pct sa_med_dev_pct 
                    z_med_dev_pct st_med_dev_pct da_med_dev_pct rf_med_dev_pct fr_med_dev_pct;

   *Iterative DO loop to compute deviations;
   do i = 1 to dim(obs);
      avg_raw[i] = obs[i] - avg[i];
      if avg[i] ne 0 then avg_pct[i] = 100*avg_raw[i]/avg[i];
         else call missing(avg_pct[i]);
      med_raw[i] = obs[i] - med[i];
      if med[i] ne 0 then med_pct[i] = 100*med_raw[i]/med[i];
         else call missing(med_pct[i]);
   end;
run;

*Sort data by lake NAME to match the final output;
proc sort data = fish2 out = all;
   by name;
run;

quit;
