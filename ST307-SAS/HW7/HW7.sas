data rebound;
infile 'rebound.txt' firstobs=2;
input drop_height rebound;
run;

symbol value=dot i=r ci=red cv=blue;
proc gplot data=rebound;
plot rebound*drop_height;
run;

proc corr data=rebound;
var rebound drop_height;
run;

proc reg data=rebound;
model rebound=drop_height;
run;
