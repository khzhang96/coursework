LIBNAME Assign16 "INSERT DIRECTORY HERE";

DATA Assign16.ClinicalTrials;
	INFILE "INSERT ClinicalTrials.txt FILE" ENCODING=ANY;
	INPUT @"Title:" Title & $200.
		  @"Recruitment:" Recruit & $50.  
		  @"Study Results:" Results & $50.
		  @"Conditions:" Conditions & $200.
		  @"Interventions:" Intervention & $200.
		  @"URL:" URL & $100.;
	/*Include observations that have diabetes in the conditions or title and have studies with a drug intervention*/
	/*Grab the numbers after NCT from URL for NCT varibale*/
	/*Grab the first drug name using SUBSTR and then do again with SCAN*/
	propcase(recuit);
RUN;

data newdata;
	set Assign16.ClinicalTrials
run;


