/**
 *@file warmup
 *@author Kevin Zhang khzhang
 *this program is a warmup program and homework 1
 */

#include <stdio.h>
#include <stdlib.h>

/**
    Print n copies of the given character c.
    @param c character to print.
    @param n number of times to print c, must be non-negative.
*/
void nTimes( char c, int n )
{
    int counter;
    for (counter = 0; counter < n; counter = counter + 1){
        printf("%c", c);
    }
}

/**
    Print out a rectangle of a given size and made from the given inner
    and outer character symbols.
    @param width the width of the rectangle to be printed, should be 2 or greater.
    @param height the number of rows high the rectangle should be, must be 2 or greater.
    @param outer the character symbol to use for the outer edge of the rectangle.
    @param inner the character symbol fill the inside of the rectangle with.
 */
void filledBox( int width, int height, char outer, char inner )
{
    nTimes(outer, width);
    printf("\n");
    /*counter for loop*/
    int counter;
    /*can't use magic numbers so offset represents front and back*/
    int offset = 2;
    //loops through the second to second last line
    for (counter = 0; counter < height - offset; counter = counter + 1) {
        nTimes(outer, 1);
        nTimes(inner, width - offset);
        nTimes(outer, 1);
        printf("\n");
    }
    nTimes(outer, width);
    printf("\n");    
}

/**
    Return the X intercept for a line defined as y = m x + b.
    @param m the slope of the line, must be non-zero.
    @param b the y intercept of the line.
    @return the x intercept
 */
double intercept( double m, double b )
{
    return  (-1 * b) / m;
}

/**
   Print a report about the line, y = m x + b, including its x intercept.
   @param m the slope of the line, must be non-zero.
   @param b the y intercept of the line.
 */
void describeLine( double m, double b )
{
    /*Line y = m x + b has an x intercept of x*/
    
    printf("Line y = %.3f", m);
    printf(" x + %.3f", b);
    printf(" has an x intercept of: %.3f", intercept(m,b));
    printf("\n");
}

/**
   Starting point for the program, prints a couple of rectangles and
   reports about a couple of lines.
   @return Exit status for the program.
 */
int main()
{
  // Print a short, wide box with # on the outside and . on the inside.
  filledBox( 20, 6, '#', '.' );
  printf( "\n" );

  // Print tall, thin box with * on the outside and spaces inside.
  filledBox( 7, 11, '*', ' ' );
  printf( "\n" );

  // Report about one line.
  describeLine( 0.75, 18.3 );

  // Then, another line.
  describeLine( -3.4281, 102.0 );

  return EXIT_SUCCESS;
}
