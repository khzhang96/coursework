// Code to read numbers from standard input inserting them into a list
// in sorted order as we read them.

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Node for building our linked list.
struct NodeTag {
  // Value in this node.
  int value;

  // Pointer to the next node.
  struct NodeTag *next;
};

// A short type name to use for a node.
typedef struct NodeTag Node;

/*
  Create a node for the given value and insert it into the given list
  in the proper location to keep the list sorted.  Return an updated
  head pointer.  The head should only need to change if this value gets
  inserted at the front of the list.  Otherwise, you can just return
  the old head pointer.
*/
Node *insert( Node *head, int val )
{
   // Make a new node.
  Node *n = (Node *)malloc( sizeof( Node ) );

  // Save val in the value field.
  n->value = val;

  // Link it to the start of the list.
  n->next = head;

  // Now, the new node is our new head
  return n;
  
}

Node *sort_list(Node *head) {

    Node *tmpPtr = head;
    Node *tmpNxt = head->next;

    int tmp;

    //printf("outside: %d %d\n",tmpPtr->value, tmpNxt->value);

    while(tmpNxt != NULL){
      //printf("loop1: %d %d\n",tmpPtr->value, tmpNxt->value);
           while(tmpNxt != tmpPtr){
             //printf("loop2: %d %d\n",tmpPtr->value, tmpNxt->value);
                    if(tmpNxt->value < tmpPtr->value){
                      //printf("exchange\n");
                            tmp = tmpPtr->value;
                            tmpPtr->value = tmpNxt->value;
                            tmpNxt->value = tmp;
                    }
                    tmpPtr = tmpPtr->next;
                    //printf("loop2a: %d %d\n",tmpPtr->value, tmpNxt->value);
            }
            tmpPtr = head;
            tmpNxt = tmpNxt->next;
    }
    return tmpPtr; // Place holder
}

int main()
{
  // Pointer to the head node.
  Node *head = NULL;

  // Read a list of numbers from building a sorted list as we
  // read them.
  int x;
  int count;
  while ( scanf( "%d", &x ) == 1 ){
    // Insert the new value, and get back the (possibly updated) head pointer.       
    head = insert( head, x );   
    count++;     
  }

  head = sort_list(head);



  

  // Print out the list.
  for ( Node *n = head; n; n = n->next )
    printf( "%d ", n->value );
  
  for ( Node *n = head; n; n = n->next ) {
    free(n);
  }
  
  return 0;
}
