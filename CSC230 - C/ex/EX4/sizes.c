// Report the sizes and ranges of a few standard C types.

#include <stdio.h>
#include <limits.h>

int main()
{
  printf( "char bits:             %zd\n", 8 * sizeof( char ) );
  printf( "minimum char:          %d\n", CHAR_MIN );
  printf( "maximum char:          %d\n", CHAR_MAX );

  printf( "short bits:            %zd\n", 8 * sizeof( short ) );
  printf( "minimum short:         %d\n", SHRT_MIN );
  printf( "maximum short:         %d\n", SHRT_MAX );

  printf( "int bits:              %zd\n", 8 * sizeof( int ) );
  printf( "minimum int:           %d\n", INT_MIN );
  printf( "maximum int:           %d\n", INT_MAX );

  printf( "long bits:             %zd\n", 8 * sizeof( long ) );
  printf( "minimum long:           %ld\n", LONG_MIN );
  printf( "maximum long:           %ld\n", LONG_MAX );

  printf( "unsigned long bits:    %zd\n", 8 * sizeof( unsigned long ) );
  printf( "minimum unsigned long:           %d\n", 0 );
  printf( "maximum unsigned long:           %lu\n", ULONG_MAX );

  printf( "unsigned long long bits:    %zd\n", 8 * sizeof( long long ) );
  printf( "minimum long long:           %lld\n", LLONG_MIN );
  printf( "maximum long long:           %lld\n", LLONG_MAX );



  // ...

  return 0;
}
