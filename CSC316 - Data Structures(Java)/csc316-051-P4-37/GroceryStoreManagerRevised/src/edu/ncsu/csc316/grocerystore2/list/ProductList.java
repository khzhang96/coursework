/**
 *
 */
package edu.ncsu.csc316.grocerystore2.list;

import edu.ncsu.csc316.grocerystore2.order.Product;

/**
 * Product List class
 *
 * @author khzhang
 *
 */
public class ProductList {
    /**
     * Default capacity for list
     */
    public static final int DEFAULT_CAP = 1;

    /**
     * Maximum list capacity
     */
    private int capacity = ProductList.DEFAULT_CAP;
    /**
     * Product List
     */
    private Product[] productList;

    /**
     * Maximum list size
     */
    private int size = 0;

    /**
     * Constructor
     */
    public ProductList() { // creates a new product list
        this.productList = new Product[this.capacity];
    }

    /**
     * Adds an element
     *
     * @param element
     *            - element to be added
     */
    public void add(Product element) { // adds a product at the end of the list if product is not found, otherwise it
                                       // will match current product
        
        int index = this.lookUp(element.getBrand(), element.getDescription()); // finds the index of product
        if (index == -1) { // -1 means item not found
            this.ensureCapacity(); // makes sure list has the capacity
            this.productList[this.size] = element; // assigns last index to new element
            this.size++; // increments size
        } else {
            this.productList[index].inc(); // if item is found, increases product frequency
            this.transpose(index);
        }
        
    }

    /**
     * Ensure list has enough capacity to add new elements otherwise, create new
     * array with capacity * 2 + 1 elements
     */
    public void ensureCapacity() { // sets the capacity of the current list so there is always enough room
        if (this.size + 1 >= this.capacity) { // tests size of current array with capacity
            Product[] old = this.productList; // creates pointer for current list

            this.capacity = this.capacity * 2 + 1; // sets new capacity
            Product[] newArray = new Product[this.capacity]; // creates new array with larger capacity

            for (int i = 0; i < this.size; i++) {
                newArray[i] = old[i];
            }

            this.productList = newArray;
        }
    }

    /**
     * Gets the product
     *
     * @param index
     *            array index
     * @return returns Product element
     */
    public Product get(int index) { // gets an element by its index
        if (index < this.productList.length) { // makes sure index is within bounds
            return this.productList[index];
        } else {
            return null; // return null if index is out of bounds
        }
    }

    /**
     * Gets the size
     *
     * @return list size
     */
    public int getSize() { // gets the size of the list
        return this.size;
    }

    /**
     * Looks up Product by Brand and Description
     *
     * @param brand
     *            Product Brand
     * @param description
     *            Product Description
     * @return index of array element
     */
    public int lookUp(String brand, String description) {
        brand = brand.trim(); // trims the brand
        description = description.trim(); // trims the description
        Product compare = new Product(brand, description); // creates test product
        for (int i = 0; i < this.size; i++) { // sees if current product is idential to product in the list
            if (compare.equalsMod(this.productList[i])) {
                return i; // return index if identical
            }
        }

        return -1; // return -1 if product isn't found

    }
    
    /**
     * Look up a specific product rather than an index
     * 
     * @param brand
     *            brand to be lookedup
     * @param description
     *            product to be looked up
     * @return Product
     */
    public Product lookUpProduct(String brand, String description) {
        brand = brand.trim(); // trims the brand
        description = description.trim(); // trims the description
        Product compare = new Product(brand, description); // creates test product
        for (int i = 0; i < this.size; i++) { // sees if current product is idential to product in the list
            if (compare.equalsMod(this.productList[i])) {
                return this.productList[i]; // return index if identical
            }
        }

        return null; // return -1 if product isn't found

    }

    /**
     * Removes product from the list
     *
     * @param p
     *            Product p
     * @return Product that has been removed from the list
     */
    public Product remove(Product p) { // removes product from list - currently useless
        int index = this.lookUp(p.getBrand(), p.getDescription()); // looks up product by description
        if (index != -1) { // tests if product is in the list
            Product temp = this.productList[index]; // creates a temporary product

            if ((this.productList[index].dec()) <= 0) { // checks if new frequency is smaller or equal to zero
                for (int i = index; i < this.size; i++) { // remove that list entry
                    this.productList[i] = this.productList[i + 1]; // move products forward
                }
                this.size--; // decrements size
            }
            return temp;
        } else { // product is not in the list
            return null;
        }

    }

    /**
     * Creates a string representation of the entire list
     *
     * @return string representation of the entire list
     */
    @Override
    public String toString() {
        StringBuffer strBuf = new StringBuffer(); // creates a string buffer for easy concatenation

        for (int i = 0; i < this.size; i++) {
            strBuf.append(this.productList[i].toString()); // adds individual to string outputs into a much larger
                                                           // string
        }

        return strBuf.toString(); // returns the outcome
    }

    /**
     * Moves element to the front after lookup has been completed
     *
     * @param index
     *            index of element
     */
    public void transpose(int index) {
        if (index != 0) {
            Product temp = this.productList[index];
            this.productList[index] = this.productList[index - 1];
            this.productList[index - 1] = temp;
        }
    }

}
