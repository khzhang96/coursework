package edu.ncsu.csc316.grocerystore2.manager;

import org.junit.Assert;
import org.junit.Test;

import edu.ncsu.csc316.grocerystore2.manager.GroceryStoreManager;

/**
 * Grocery Manager Test
 * 
 * @author khzhang
 *
 */
public class GroceryStoreManagerTest {

    /**
     * Test 1
     */
    @Test
    public void test() {

        GroceryStoreManager test = new GroceryStoreManager("products.txt", "customers.txt");

        String output = test.getCustomers();
        String output2 = test.getAllProducts();

        // System.out.println(output);
        System.out.println(output2);

        System.out.println(test.getProduct("LG", "G7 Thinq"));

        Assert.assertEquals(output, test.getCustomers());

    }

}
