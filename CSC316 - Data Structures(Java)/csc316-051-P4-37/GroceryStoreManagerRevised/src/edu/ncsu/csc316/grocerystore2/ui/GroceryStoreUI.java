package edu.ncsu.csc316.grocerystore2.ui;

import java.util.Scanner;

import edu.ncsu.csc316.grocerystore2.manager.GroceryStoreManager;

/**
 * GroceryStoreUI class
 *
 * @author khzhang
 *
 */
public class GroceryStoreUI {
    /**
     * cmd line function for UI
     *
     * @param args
     *            product file path and customer file path
     */
    public static void main(String args[]) {
        GroceryStoreManager mgr;
        while (true) {
            if (args.length >= 2) {
                mgr = new GroceryStoreManager(args[0], args[1]);
                break;
            } else if (args.length < 2) {
                System.out.println("Please enter file paths for Product List and Customer List");
            }
        }

        while (true) {
            System.out.printf(
                    "Please choose an option below:\n1.Retrieve all customers\n2.Look up certain product\n3.Retrieve all Products\n4.Exit");
            Scanner scanIn = new Scanner(System.in);
            int i = scanIn.nextInt();
            
            if (i == 1) {
                System.out.println(mgr.getCustomers());
            } else if (i == 2) {
                System.out.println("Please enter a brand");
                String brand, description;

                brand = scanIn.nextLine();
                System.out.println("Please enter a description");
                description = scanIn.nextLine();
                
                System.out.println(mgr.getProduct(brand, description));
            } else if (i == 3) {
                System.out.println(mgr.getAllProducts());
            } else if (i == 4) {
                scanIn.close();
                System.exit(1);
            } else {
                System.out.println("Please enter a valid choice");
            }
        }
    }
}
