package edu.ncsu.csc316.transportation_manager.kruskal;

import edu.ncsu.csc316.transportation_manager.heap.MinHighwayHeap;
import edu.ncsu.csc316.transportation_manager.highway.Highway;
import edu.ncsu.csc316.transportation_manager.list.AdjList;
import edu.ncsu.csc316.transportation_manager.list.HighwayList;

/**
 * Kruskal's algo
 *
 * @author khzhang
 *
 */
public class Kruskal {
    
    HighwayList output;
    
    /**
     * Kruskal's algorithm
     *
     * @param heap
     *            heap
     * @param points
     *            adjancylist
     *
     *
     */
    
    public Kruskal(MinHighwayHeap heap, AdjList points) {
        int parent[] = new int[points.getSize()];
        this.output = new HighwayList();
        int i = 0;
        while (i != points.getSize()) {
            parent[i] = -1;
            i++;
        }

        while (heap.isEmpty() == 0) {
            this.kruskalHelper(heap.deleteMin(), parent);
        }
    }
    
    /**
     * Find function
     *
     * @param parent
     *            array
     * @param i
     *            finds point
     * @return root
     */
    int find(int parent[], int i) {
        if (parent[i] == -1) {
            return i;
        }
        
        return this.find(parent, parent[i]);
        
    }

    /**
     * Helper method
     *
     * @param newH
     *            new highway
     * @param parent
     *            array
     */
    public void kruskalHelper(Highway newH, int[] parent) {
        int x = this.find(parent, newH.getCity1());
        int y = this.find(parent, newH.getCity2());

        switch ((x != y) ? 1 : 0) {
            case 1:
                this.output.add(newH);
                this.union(parent, x, y);
                break;
            default:
                break;
        }
        
    }

    @Override
    public String toString() {
        this.output.msort();
        return this.output.toString();
    }
    
    /**
     * Attach smaller rank tree under root of high rank tree
     *
     * @param parent
     *            parent array
     * @param x
     *            source
     * @param y
     *            dest
     */
    void union(int parent[], int x, int y) {
        int xroot = this.find(parent, x);
        int yroot = this.find(parent, y);
        
        parent[xroot] = yroot;
    }
    
}
