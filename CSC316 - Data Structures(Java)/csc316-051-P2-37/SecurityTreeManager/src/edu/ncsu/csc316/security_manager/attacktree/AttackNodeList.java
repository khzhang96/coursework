/**
 *
 */

package edu.ncsu.csc316.security_manager.attacktree;

/**
 * Product List class
 *
 * @author khzhang
 *
 */
public class AttackNodeList {
    /**
     * Default capacity for list
     */
    public static final int DEFAULT_CAP = 10;

    /**
     * Maximum list capacity
     */
    private int capacity = AttackNodeList.DEFAULT_CAP;
    /**
     * Product List
     */
    private AttackNode[] attackNodeList;

    /**
     * Maximum list size
     */
    public int size = 0;

    /**
     * Constructor
     */
    public AttackNodeList() { // creates a new product list
        this.attackNodeList = new AttackNode[this.capacity];
    }

    /**
     * Adds an element
     *
     * @param element
     *            - element to be added
     */
    public void add(AttackNode element) { // adds a product at the end of the list if product is not found, otherwise it
                                          // will match current product
        this.ensureCapacity(); // makes sure list has the capacity
        this.attackNodeList[this.size] = element; // assigns last index to new element
        this.size++; // increments size

    }

    /**
     * Ensure list has enough capacity to add new elements otherwise, create new
     * array with capacity * 2 + 1 elements
     */
    public void ensureCapacity() { // sets the capacity of the current list so there is always enough room
        if (this.size + 1 >= this.capacity) { // tests size of current array with capacity
            AttackNode[] old = this.attackNodeList; // creates pointer for current list

            this.capacity = this.capacity * 2 + 1; // sets new capacity
            AttackNode[] newArray = new AttackNode[this.capacity]; // creates new array with larger capacity

            for (int i = 0; i < this.size; i++) {
                newArray[i] = old[i];
            }

            this.attackNodeList = newArray;
        }
    }
    
    public AttackNode[] getList() {
        return this.attackNodeList;
    }

    /**
     * Gets the size
     *
     * @return list size
     */
    public int getSize() { // gets the size of the list

        return this.size;
    }
    
}
