/* Cluster analysis code example */


*example 1 - one-stage cluster sampling with unequal cluster sizes;
PROC IMPORT OUT= work.measles
            DATAFILE= "\\tsclient\C\Users\khzhang\Google Drive\Year 4\ST432\measles.csv" 
            DBMS=CSV REPLACE;
     GETNAMES=YES;
     DATAROW=2; 
RUN;


data measles;
   set measles;
   sampwt = 9962/10; *sampling weight is equal to N/n;
run;

*find mean and SE;
proc surveymeans data=measles total = 9962 nobs mean sum clm clsum df; 
   cluster school;
   var consent;
   weight sampwt;
run;


proc glm data = measles; *this code gives us some nice side=by-side boxplots of the clusters;
   class school;
   model consent = school;
   means school; *Gives us the cluster level means; 
run;
quit;









*example 2 - two-stage cluster analysis;
/*PROC IMPORT OUT= work.coots
            DATAFILE= "C:\Users\baalhant\Google Drive\432 Spring 2018\Textbook Extras\Dataset\coots.csv" 
            DBMS=CSV REPLACE;
     GETNAMES=YES;
     DATAROW=2; 
RUN;

data coots;
   set coots;
   relwt = csize/2; *clusters are different sizes, 
   					relative weight is equal to M_i/2 bc 2 eggs sample from each nest;
run;


*side=by-side boxplots of the clusters;
proc glm data = coots; 
   class clutch;
   model volume = clutch;
run;
quit;



*plot cluster means;
symbol1 value= dot h = .5;
axis4 label=(angle=90 'Egg Volume') order=(0 to 5 by 1);
axis3 label=('Clutch Number') order =  (0 to 200 by 50);

proc gplot data=coots;
   plot volume * clutch/ haxis = axis3 vaxis = axis4;
run;



*find mean and SE;
proc surveymeans data=coots nobs mean sum clm clsum df;
   cluster clutch;
   var volume;
   weight relwt;
run;*/
