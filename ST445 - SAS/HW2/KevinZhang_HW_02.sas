libname HW2 '\\tsclient\D\ST445\HW2\';

filename Pres1 '\\tsclient\D\ST445\HW2\PresidentV1.txt';
filename Pres1 '\\tsclient\D\ST445\HW2\PresidentV2.txt';


data HW2.Pres1;

	infile Pres1 dlm = '09'x;
	input FullName $ City $ State $ ASP DOB Term Votes PrecVote;
	
	label ASP = "Age starting presidency"
		  DOB = "Date of Birth"
		  Perc = "Percent of Vote";
run;


data HW2.Pres2;
infile Pres2; /*previous data is tab limitited, not space delimited*/
input input FullName $ City $ State $ ASP DOB Term Votes PrecVote;
run;

data HW2.Pres2;
infile Pres2;
input @1-24     FullName $
	  @25-48    City     $
	  @49-72    State    $
	  @73-74    ASP
	  @81-96    DOB      mmddyy10.
	  @97-104   Term     
	  @105-120  Votes
	  @121      PrecVote;
run; 


proc format fmtlibrary=HW2;
value $region "Colorado", "..." = "West"
			  "Texas", "..." = "Southwest"
			  "Ohio", "..." = "Northeast"
			  "West Virginia", "..." = "Southeast"
value century 1700-1799 = "1700s"
			  1800-1899 = "1800s"
			  1900-1999 = "1900s"
			  2000-2099 = "2000s"
run;

proc contents data=HW2._all_nods;
title "Contents of HW2 Library";
footnote "Prepared by Kevin Zhang";
options nonumber nodate;

proc print data=HW1.bank_CSV_MLI;
title;
title2;






