 #include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#include <errno.h>

/** Port number used by my server */
#define PORT_NUMBER "26026"
FILE *fp;
//Your port numbers are: 26026 and 26027

pthread_mutex_t mutex;
pthread_mutex_t list;
pthread_cond_t cond;

char **names;
static int size = 0;
static int capacity = 999;


// Print out an error message and exit.
static void fail( char const *message ) {
  fprintf( stderr, "%s\n", message );
  exit( 1 );
}

int find( char *name) {
  for (int i = 0; i < size; i++) {
    if (strcmp(names[i], name)==0) {
      return i; //return index
    }
  }

  return -1;

  
}



void addItem(char *nameP) {
  char* name = malloc(42);
  strcpy(name, nameP);
  if (size++ == capacity) {
    capacity = capacity * 2 + 1;
    names = realloc(names, capacity * sizeof(char*));
  }
  names[size - 1] = malloc(42);
  strcpy(names[size - 1], name);
  //size++;
}

void removeItem(char *nameP) {
  char* name = malloc(42);
  strcpy(name, nameP);
  int index = find(name);

  for (int i = index; i < size - 1; i++) {
    names[i]=names[i + 1];
  }
  if (size > 1) {
    size--;
  } else {
    size = 0;
  }
  
  

}

void lock(char* nameP) {
  //pthread_mutex_lock(&list);
  pthread_mutex_lock(&mutex);
  char* name = malloc(42);
  strcpy(name, nameP);
  
  if (find(name) == -1) { //item not in list of names
    

    addItem(name);

    //pthread_cond_broadcast(&cond);  

  } else {// item aready found
    while (find(name) != -1) {
      pthread_cond_wait(&cond, &mutex);
    }

    addItem(name);
    
  }
  //pthread_mutex_unlock(&list);
  pthread_mutex_unlock(&mutex);

  
  //fprintf(fp, "locking\n");
}

void unlock( char* nameP) {
  //pthread_mutex_lock(&list);
  pthread_mutex_lock(&mutex);
  char* name = malloc(42);
  strcpy(name, nameP);
  
  if (find(name) == -1) {
    /*pthread_mutex_lock(&mutex);
    while (find(name) == -1) {
      pthread_cond_wait(&cond, &mutex);
    }
    removeItem(name);
    pthread_mutex_unlock(&mutex);*/
  } else { //item is found then remove it
    

    removeItem(name);
    pthread_cond_broadcast(&cond);    

    
  }
  //fprintf(fp, "unlocking\n");
  //pthread_mutex_unlock(&list);
  pthread_mutex_unlock(&mutex);
  

}

/** handle a client connection, close it when we're done. */
void handleClient( int sock ) {
  // Here's a nice trick, wrap a C standard IO FILE around the
  // socket, so we can communicate the same way we would read/write
  // a file.
  
  fp = fdopen( sock, "a+" );
  
  // Prompt the user for a command.
  fprintf( fp, "cmd> " );

  char cmd[ 420 ];
  int match;
  while ( ( match = fscanf( fp, "%42[^\n]", cmd ) ) == 1 &&
          strcmp( cmd, "quit" ) != 0 ) {
    // Just echo the command back to the client.
    char* res = strtok(cmd, " ");
    if (strcmp(cmd, "list") == 0) {
       //pthread_mutex_lock(&list);
       for (int i = 0; i < size; i++) {
         fprintf(fp, "%s\n", names[i]);
       }
       //pthread_mutex_unlock(&list);
    } else if ( ( strcmp(res, "lock") == 0 ) || strcmp(res, "unlock") == 0 ) {
      //char* res = strtok(cmd, " ");
      char *name = malloc(42);
      if (strcmp(res, "lock") == 0) {
        strcpy(name,strtok(NULL, " "));
        //fprintf(fp, "%s\n", name);
        lock(name);
      } else if (strcmp(res, "unlock") == 0) {
        strcpy(name,strtok(NULL, " "));
        //fprintf(fp, "%s\n", name);
        unlock(name);
      }
    } else {
      //do nothing
    }
    //fprintf( fp, "%s\n", cmd );

    // Prompt the user for the next command.
    fprintf( fp, "cmd> " );
    fflush( fp );
    fp = fdopen( sock, "a+" );
  }

  // Close the connection with this client.
  //fclose( fp );
}

void *clientThread(void *arg) {
  int sock = *((int *) arg);
  handleClient( sock );
  return NULL;
}

int main( int argc, char *argv[] ) {
  char *port;

  if (argc == 2) {
    port = argv[1];
  } else if (argc != 2) {
    port = PORT_NUMBER;
  }
  names = malloc(capacity * sizeof(char*));
  //printf("here");
  pthread_cond_init(&cond, NULL);
    //pthread_cond_init(&cond2, NULL);
    //pthread_cond_init(&listFull, NULL);
  pthread_mutex_init(&mutex, NULL);
  pthread_mutex_init(&list, NULL);

  // Prepare a description of server address criteria.
  struct addrinfo addrCriteria;
  memset(&addrCriteria, 0, sizeof(addrCriteria));
  addrCriteria.ai_family = AF_INET;
  addrCriteria.ai_flags = AI_PASSIVE;
  addrCriteria.ai_socktype = SOCK_STREAM;
  addrCriteria.ai_protocol = IPPROTO_TCP;

  // Lookup a list of matching addresses
  struct addrinfo *servAddr;
  if ( getaddrinfo( NULL, port, &addrCriteria, &servAddr) )
    fail( "Can't get address info" );

  // Try to just use the first one.
  if ( servAddr == NULL )
    fail( "Can't get address" );

  // Create a TCP socket
  int servSock = socket( servAddr->ai_family, servAddr->ai_socktype,
                         servAddr->ai_protocol);
  if ( servSock < 0 )
    fail( "Can't create socket" );

  // Bind to the local address
  if ( bind(servSock, servAddr->ai_addr, servAddr->ai_addrlen) != 0 )
    fail( "Can't bind socket" );
  
  // Tell the socket to listen for incoming connections.
  if ( listen( servSock, 5 ) != 0 )
    fail( "Can't listen on socket" );

  // Free address list allocated by getaddrinfo()
  freeaddrinfo(servAddr);

  // Fields for accepting a client connection.
  struct sockaddr_storage clntAddr; // Client address
  socklen_t clntAddrLen = sizeof(clntAddr);

  while ( true  ) {
    //start threads here
    // Accept a client connection.
    int sock = accept( servSock, (struct sockaddr *) &clntAddr, &clntAddrLen);

    // Talk to this client
    pthread_t thread;

    int *a = malloc(sizeof(*a));
    *a = sock;
    pthread_create( &thread, NULL, clientThread, a );
  }

  // Stop accepting client connections (never reached).
  close( servSock );
  
  return 0;
}
