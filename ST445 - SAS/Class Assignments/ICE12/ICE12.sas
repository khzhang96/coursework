libname "\\tsclient\ST\445\ICE11"
filename company "company.txt"
filename finance "finance.txt"
filename repertory "repertory.txt";

proc data company;
	infile "company.txt" firstobs=2;
	input  @1  name 36.
		   @37 age $ 5.
		   @42 gender
		   ;
run;

proc data finance;
	infile "finance.txt" firstobs=2;
	input @1  IdNumber 12.
		  @13 Name 29.
		  @42 Salary $dollar7.
		  ;
run;

proc data repertory;
	infile "repertory.txt" firstobs=2;
	input @1  Play 24.
		  @25 Role 24.
		  @50 IdNumber
		  ;
run;

proc data merged;
	set company;
	set finance;
	set repertory;
run;
