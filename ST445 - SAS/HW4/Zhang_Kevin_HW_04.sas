/*
Author: Kevin Zhang
Date: 2016-10-17
Purpose: 
Combines datasets using one to one merging
*/

libname HW4 '\\tsclient\D\ST445\HW4\';

filename aoD '\\tsclient\D\ST445\HW4\AO Division.dat';
filename corpD '\\tsclient\D\ST445\HW4\CORP Division.dat';
filename empdata '\\tsclient\D\ST445\HW4\empData.dat';
filename financeD '\\tsclient\D\ST445\HW4\FINANCE_IT Division.dat';
filename fltOpsD '\\tsclient\D\ST445\HW4\FLIGHT OPS Division.dat';
filename hrD '\\tsclient\D\ST445\HW4\HR Division.dat';
filename personal '\\tsclient\D\ST445\HW4\Personal.dat';
filename salesD '\\tsclient\D\ST445\HW4\SALES Division.dat';

data HW4.personal;
	infile personal firstobs = 2;
	input fName $19. lName $19. phone $5. empid $;
run;

data HW4.empdata;
	infile empdata firstobs = 2;
	input hire date9. empid $ jobCode $ salary dollar7.;
run;

data HW4.aoD;
	infile aoD firstobs = 2 dlm = '09'x;
	input division $20. jobCode $;
run;

data HW4.corpD;
	infile corpD firstobs = 2 dlm = '09'x;
	input division $20. jobCode $;
run;

data HW4.financeD;
	infile financeD firstobs = 2 dlm = '09'x;
	input division $20. jobCode $;
run;

data HW4.fltOpsD;
	infile fltOpsD firstobs = 2 dlm = '09'x;
	input division $20. jobCode $;
run;

data HW4.hrD;
	infile hrD firstobs = 2 dlm = '09'x;
	input division $20. jobCode $;
run;

data HW4.salesD;
	infile salesD firstobs = 2 dlm = '09'x;
	input division $20. jobCode $;
run;



proc sort data = HW4.Personal;
	by empID;
run;

proc sort data = HW4.EmpData;
	by empID;
run;

proc sort data = HW4.aoD;
	by jobCode;
run;

proc sort data = HW4.corpD;
	by jobCode;
run;

proc sort data = HW4.financeD;
	by jobCode;
run;
proc sort data = HW4.fltOpsD;
	by jobCode;
run;
proc sort data = HW4.hrD;
	by jobCode;
run;

proc sort data = HW4.salesD;
	by jobCode;
run;

/*sort starts here */
data HW4.DIVISION;

	merge HW4.aoD HW4.corpD HW4.financeD HW4.fltOpsD HW4.hrD HW4.salesD;
	by jobCode;
run ;

proc sort data = HW4.DIVISION;
	by jobCode;
run;

data HW4.Air_Emps;
	merge HW4.empdata 
		  HW4.personal;
	by empid;
run;

proc sort data = HW4.Air_Emps;
	by jobCode;
run;

proc sort data = HW4.DIVISION;
	by jobCode;
run;

data HW4.Air_Emps_Full;
	merge HW4.Air_Emps HW4.DIVISION ;
	by jobCode;	
	drop phone;
	rename salary = CurrentSalary;
run; 

data HW4.Air_Emps_Underpaid;
	set HW4.Air_Emps_Full;
	if CurrentSalary > 45000 then delete; 
	if hire > year(1990) then delete;
run;

proc print data = HW4.Air_Emps_Underpaid;
run;






