package edu.ncsu.csc316.security_manager.attacktree;

import org.junit.Assert;
import org.junit.Test;

public class AttackTreeTest {

    @Test
    public void test() {
        AttackNode goalNode = new AttackNode("GOAL", "Use DDoS Attack to Disrupt All Users");
        AttackNode methodNode = new AttackNode("O", "Attack Servers");
        AttackNode methodNode2 = new AttackNode("O", "Direct BOTNET against key Servers");
        AttackNode probNode = new AttackNode("O", .5, 6, 5000, "RENT existing BOTNET");
        AttackNode methodNode3 = new AttackNode("O", "Build a BOTNET");
        AttackNode probNode2 = new AttackNode("A", .5, 3, 2000, "Find vulnurable computers");
        AttackNode probNode3 = new AttackNode("A", .4, 4, 5000, "Infect Computer with BOT");
        AttackNode probNode4 = new AttackNode("A", .6, 4, 1000, "Remain Undetected");
        AttackNode probNode5 = new AttackNode("O", .3, 7, 30000, "Infect Servers with Worm/Virus");
        AttackNode methodNode4 = new AttackNode("O", "Attack Comm Infrastructure");
        AttackNode probNode6 = new AttackNode("O", .3, 7, 30000, "Attack Switches");
        AttackNode probNode7 = new AttackNode("O", .3, 7, 30000, "Attack Routers");
        AttackNode probNode8 = new AttackNode("O", .1, 8, 50000, "Attack DNS");
        AttackNode methodNode5 = new AttackNode("O", "Attack All Clients");
        AttackNode probNode9 = new AttackNode("O", .1, 5, 10000, "Infect Clients with Worm/Virus");
        
        AttackNodeList preOrder = new AttackNodeList();
        AttackNodeList postOrder = new AttackNodeList();

        preOrder.add(goalNode);
        preOrder.add(methodNode);
        preOrder.add(methodNode2);
        preOrder.add(probNode);
        preOrder.add(methodNode3);
        preOrder.add(probNode2);
        preOrder.add(probNode3);
        preOrder.add(probNode4);
        preOrder.add(probNode5);
        preOrder.add(methodNode4);
        preOrder.add(probNode6);
        preOrder.add(probNode7);
        preOrder.add(probNode8);
        preOrder.add(methodNode5);
        preOrder.add(probNode9);
        
        postOrder.add(probNode);
        postOrder.add(probNode2);
        postOrder.add(probNode3);
        postOrder.add(probNode4);
        postOrder.add(methodNode3);
        postOrder.add(methodNode2);
        postOrder.add(probNode5);
        postOrder.add(methodNode);
        postOrder.add(probNode6);
        postOrder.add(probNode7);
        postOrder.add(probNode8);
        postOrder.add(methodNode4);
        postOrder.add(probNode9);
        postOrder.add(methodNode5);
        postOrder.add(goalNode);

        AttackTree atkTree = new AttackTree(preOrder, postOrder);
        System.out.println("printed");
        
        atkTree.root.propogate();

        Assert.assertTrue(true);
    }
}
