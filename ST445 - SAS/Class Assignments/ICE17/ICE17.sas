libname ICE17 '\\tsclient\D\ST445\Class Assignments\ICE17';

filename Course4 '\\tsclient\D\ST445\Class Assignments\ICE17\Courses_v4.txt';
filename Course2 '\\tsclient\D\ST445\Class Assignments\ICE17\Courses_v2.txt';
filename Course3 '\\tsclient\D\ST445\Class Assignments\ICE17\Courses_v3.txt';
filename Course1 '\\tsclient\D\ST445\Class Assignments\ICE17\Courses_v1.txt';

data ICE17.Course1;

infile Course1;

input dept $ course $ section $ @@;

run;

data ICE17.Course2;

infile Course2;

input dept $ course @;
input section $ @;
output;
input section $ @;
output;
input section $ @;
output;
run;

data ICE17.Course3;

infile Course3;

input dept $ course $ instructor $ section $ @@;
run;


proc print data = ICE17.Course3;
run;


