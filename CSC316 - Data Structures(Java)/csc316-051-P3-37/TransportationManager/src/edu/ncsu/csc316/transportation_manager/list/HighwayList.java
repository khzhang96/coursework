/**
 *
 */
package edu.ncsu.csc316.transportation_manager.list;

import edu.ncsu.csc316.transportation_manager.highway.Highway;

/**
 * Customer List class
 *
 * @author khzhang
 *
 */
public class HighwayList {
    /**
     * Default capacity for list
     */
    public static final int DEFAULT_CAP = 1;
    /**
     * Current max capacity of list
     */
    private int capacity = HighwayList.DEFAULT_CAP;
    /**
     * List of customers
     */
    private Highway[] highwayList;
    /**
     * Current list size
     */
    private int size = 0;

    /**
     * Constructor
     */
    public HighwayList() {
        this.highwayList = new Highway[this.capacity];
    }

    /**
     * Adds a customer
     *
     * @param element
     *            Customer element
     */
    public void add(Highway element) {
        this.ensureCapacity();

        this.highwayList[this.size] = element;
        this.size++;
    }

    /**
     * Ensures that there is enough capacity in the array before adding Does not do
     * anything to save processing power if there is enough capacity
     */
    public void ensureCapacity() { // sets the capacity of the current list so there is no index out of bounds or
                                   // seg fault in the array
        if (this.size + 1 >= this.capacity) { // tests size of current array
            Highway[] old = this.highwayList; // creates pointer for current list
            this.capacity = this.capacity * 2 + 1; // sets new capacity
            Highway[] newArray = new Highway[this.capacity]; // creates new array with larger capacity

            for (int i = 0; i < this.size; i++) {
                newArray[i] = old[i];
            }

            this.highwayList = newArray;
        }
    }

    /**
     * Gets Customer
     *
     * @param index
     *            - array index
     * @return Customer object
     */
    public Highway get(int index) { // gets the index
        if (index < this.highwayList.length) {
            return this.highwayList[index];
        }
        
        return null;
    }

    /**
     * Gets the size
     *
     * @return list size
     */
    public int getSize() { // gets the list size
        return this.size;
    }
    
    /**
     * Merge sort recursive method Orders individual elements to rebuild the list
     *
     * @param array
     *            array for sorting
     * @param first
     *            first index
     * @param middle
     *            middle index
     * @param last
     *            last index
     */
    private void merge(Highway firstArray[], Highway secondArray[], int first, int middle, int last) { // merges 2
                                                                                                       // elements
                                                                                                       // into sorted
                                                                                                       // order
        
        // Initial indexes of first and second subarrays
        int index1; // indexes for copying
        int index2; // indexes for copying
        
        // Initial index of merged subarry array
        int completeIndex = first; // index for copying to into the finished array to return
        
        for (completeIndex = first, index1 = 0, index2 = 0; index1 < firstArray.length
                && index2 < secondArray.length; completeIndex++) {
            if (firstArray[index1].getCity1() > secondArray[index2].getCity1()) { // compares the id, and makes
                this.highwayList[completeIndex] = secondArray[index2++]; // copies second array element into complete
                                                                         // array
            } else if (firstArray[index1].getCity1() == secondArray[index2].getCity1()) {
                if (firstArray[index1].getCity2() > secondArray[index2].getCity2()) { // compares the id, and makes
                    this.highwayList[completeIndex] = secondArray[index2++]; // copies second array element into
                                                                             // complete
                                                                             // array
                } else if (firstArray[index1].getCity2() == secondArray[index2].getCity2()) {
                    if (firstArray[index1].getCost() > secondArray[index2].getCost()) { // compares the id, and makes
                        this.highwayList[completeIndex] = secondArray[index2++]; // copies second array element into
                                                                                 // complete
                                                                                 // array
                    } else if (firstArray[index1].getCost() == secondArray[index2].getCost()) {
                        if (firstArray[index1].getAsphalt() > secondArray[index2].getAsphalt()) { // compares the id,
                                                                                                  // and makes
                            this.highwayList[completeIndex] = secondArray[index2++]; // copies second array element into
                                                                                     // complete
                                                                                     // array
                        } else {
                            this.highwayList[completeIndex] = firstArray[index1++]; // copies first array element into
                                                                                    // complete
                                                                                    // array
                        }
                    } else {
                        this.highwayList[completeIndex] = firstArray[index1++]; // copies first array element into
                                                                                // complete
                                                                                // array
                    }
                } else {
                    this.highwayList[completeIndex] = firstArray[index1++]; // copies first array element into complete
                                                                            // array
                }
            } else {
                this.highwayList[completeIndex] = firstArray[index1++]; // copies first array element into complete
                                                                        // array
            }
            
        }
        
        for (int i = index1; i < firstArray.length; i++, completeIndex++) { // copies any remaining elements into the
                                                                            // complete array
            this.highwayList[completeIndex] = firstArray[i];
        }
        
        for (int i = index2; i < secondArray.length; i++, completeIndex++) { // copies any remaining elements into the
                                                                             // complete array
            this.highwayList[completeIndex] = secondArray[i];
        }
        
    }
    
    /**
     * Splits the array until each array has one element left, (first = last)
     *
     * @param first
     *            first element
     * @param last
     *            last element
     */
    private void mergeSort(int first, int last) { // helper function for merge sort. starts the splitting processes
                                                  // until array is of size 1
        if (first < last) { // checks if array is of size of 1 or greater
            // Sort first and second halves
            this.mergeSort(first, (first + last) / 2); // splits first array from first to middle
            this.mergeSort((first + last) / 2 + 1, last); // splits second array from middle + 1 to last
            
            // Merge the sorted halves
            
            Highway firstArray[] = new Highway[(first + last) / 2 - first + 1]; // creates the first array of specific
                                                                                // size
            Highway secondArray[] = new Highway[last - (first + last) / 2]; // creates the second array of specific
                                                                            // size
            int i = 0, j = 0;
            while (i < firstArray.length) { // creates an array of the first half of the list
                firstArray[i] = this.highwayList[first + i++];
            }
            while (j < secondArray.length) { // creates an array of the second half of the list
                secondArray[j] = this.highwayList[(first + last) / 2 + 1 + j++];
            }
            
            this.merge(firstArray, secondArray, first, (first + last) / 2, last); // merges new list from sorted arrays
        }
    }

    /**
     * Merge sort helper method to start the recursive sort
     */
    public void msort() { // starts the sort. can be used for whatever sort I choose, used to be a
                          // selection sort stub
        this.mergeSort(0, this.size - 1);
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append("List[\n   ");
        if (this.size != 0) {
            buf.append(this.highwayList[0].toString());
            for (int i = 1; i < this.size; i++) {
                buf.append(",\n   ");
                buf.append(this.highwayList[i].toString());
            }
            buf.append("\n");
        }

        buf.append("]");
        
        return buf.toString();
    }

}
