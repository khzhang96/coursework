#include <stdio.h>
#include <stdlib.h>
#include "print.h"

int main()
{
  int ch;
  
  // Read all the characters from standard input.
  while ( ( ch = getchar() ) != EOF )
    printChar( ch );

  // Report the number of spaces, as counted by the print component.
  printf( "That contained %d spaces\n", spaceCount );

  return EXIT_SUCCESS;
}
