#ifndef SCHEDULE_H
#define SCHEDULE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "activity.h"

/*defult size of string*/
#define DEFAULT_SIZE 24
/*default num of spaces*/
#define DEFAULT_SPACE 50
/*define hours*/
#define HOURS 24
/*define minutes*/
#define MINUTES 60

/*struct for each schedule*/
typedef struct ScheduleTag {
    /*activities array*/
    Activity **activities;
    /*max size of array*/
    int maxSize;
    /*current size of array*/
    int currSize;
    /*counter for IDs*/
    int IDCounter;
} Schedule;

/**
This will dynamically allocate space for a new,
empty Schedule instance and return a pointer to it.
This will include creating the resizable array of
activity pointers stored within the Schedule.
It will be part of the schedule component.
@return Schedule - empty schedule pointer
*/
Schedule *createSchedule();

/**
This will free all the dynamically allocated memory used by
a schedule, including the schedule object itself, its
resizable array of Activity pointers, and the Activity
instances themselves. It will be
part of the schedule component.
@param sched is the Schedule struct
*/
void freeSchedule( Schedule *sched );

/**
This will add the given activity to the given schedule,
growing the resizable array as needed and
returning true if successful. This function is responsible
for checking for scheduling conflicts;
it returns false if the given activity overlaps with an
activity the leader is already doing.
This is part of the schedule component.
@param sched schedule
@param activity act
*/
void addActivity( Schedule *sched, Activity *act );

/**
Removes an activity
@param sched schedule
@param activity act
@param index of removed activity
*/
void removeActivity( Schedule *sched, Activity *act, int index );

/*
Prints the item
@param act Activity
@param spaces numspaces
*/
void printItem( Activity *act, int spaces );

/*
Prints the schedule based on users needs
@param sched Schedule
@param bool function pointer to intended needed
@param arg for whatever the user needs
*/

void printSchedule( Schedule *sched, bool (*test)( Activity *, void *arg ), void *arg );

/**type def function pointer*/
typedef bool (*TESTFunc)( Activity *, void *arg );

#endif
