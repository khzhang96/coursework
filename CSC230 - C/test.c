

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

struct NodeTag {
    int value; //value
    Struct Nodetag *next; //creates pointer that will represent next (should be previous)
}; typedef struct NodeTag Node;



void addVal( Node *head, int Val ) {
    Node *n = (Node *)malloc(sizeof(Node)); //creates a pointer called n

    (*n).value = Val; //value
    (*n).next = head; //creates a next that goes to the previous one(head)
    head = n;
    //return n; //returns address of current one, which will be assigned to head.
}



void insert( Node *head, int Val, int loc) {
    int counter = 0;
    int size = 0;
    Node *n = head; //creates a pointer n and assigns that the address of head
    while ( n ) { //as long as N the current node isn't null
        n = n->next;
        size++;
    }
    n = head;
    while (n && counter < (size - loc)) { //as long as N the current node isn't null
        n = n->next;
        counter++;
    }

    Node *tempNextNode = n->next;
    Node *newNode = { Val, tempNextNode};
    n->next = *newNode;


}

void remove( Node *head, int Val) {
    int counter = 0;
    int size = 0;
    Node *n = head; //creates a pointer n and assigns that the address of head
    Node *tempPrevious = NULL;
    while ( n && n->value != Val ) {
        tempPrevious = n;
        n = n->next;
    }

    if (n) {
        Node *tempNextNode = n;
        tempPrevious->next = tempPrevious->next->next;
        free(*tempNextNode);
    }
    


}


int main( int argc, char *argv[] )
{
    int size;
    Node *head = NULL; //creates a head

    addVal(head, 1);
    addVal(head, 2);
    addVal(head, 3);
    addVal(head, 5);

    for (Node *n = head; n; n = (*n).next) {
        print("%d", n->value);
    }

    size += insert(head, 4, 4);
    
}

struct Point pt = { {3,4},5};

struct Point pts[3] {
    {.len = 6.32}
};

pts[0].coord[1]=6;
pts[2] = pt;

unsigned short interleave( unsigned char a, unsigned char b) {
    

    unsigned char m1 = a & 0xF0;
    unsigned char m2 = a & 0x0F;
    unsigned char m3 = b & 0xF0;
    unsigned char m4 = b & 0x0F;

    return (short) m1 << 8 | m2 << 4 | m3 >> 4 | m4;


   
}

int testEven( unsigned short val ) {
    if (val & 0x5555 == 0x5555) {
        return 2;
    } else if (val & 0x5555 != 0) {
        return 1;
    } else {
        return 0;
    }
    
}

typedef NoteTag {
    int value;
    char str[21];
    struct NodeTag *next;
} Node;

void insert( Node **head, int val, char *str ) {
    Node *n = (Node *) malloc(sizeof(Node));

    n->value = val;
    n->str = str;

    n->next = *head;
    *head = n;
}

void f(int x) {
    int *list = (int *) malloc(10000000 * sizeof(int));

    assert(list);

    free(list);
}

void squeeze( char *a, char *b ) {
    while (*b) {
        if (!(isspace(*b))) {
            *a = *b
            a++;
        }
        b++;
    }
    a = '\0';
}

typedef double (*WordScore)(char const *str);

WordScore = &WS;

WordScore *ptrList = (WordScore *) malloc(25 * sizeof(WordScore));

