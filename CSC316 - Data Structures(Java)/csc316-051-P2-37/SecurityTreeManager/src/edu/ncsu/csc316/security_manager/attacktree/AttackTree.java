/**
 *
 */
package edu.ncsu.csc316.security_manager.attacktree;

/**
 * @author khzhang
 *
 */
public class AttackTree {
    
    AttackNodeList pre;
    AttackNodeList post;
    AttackNode root;

    StringBuffer buf;

    /**
     * @param pre
     * @param post
     */

    // int preindex = 0;

    public AttackTree(AttackNodeList pre, AttackNodeList post) {
        this.pre = pre;
        this.post = post;
        this.root = this.constructTreeHelper(pre.getList(), post.getList(), pre.getSize());
    }
    
    /**
     * @param pre
     * @param post
     * @param minIndex
     *            low endex
     * @param maxIndex
     *            high index
     * @param size
     *            of list
     * @return
     */
    public AttackNode constructTree(AttackNode pre[], AttackNode post[], int size) {
        // Base case
        
        AttackNode root = pre[0];

        int preIndex = 1;

        int postIndex = 0;
        int searchIndex = 0;
        
        // The first node in preorder traversal is
        // root. So take the node at preIndex from
        // preorder and make it root, and increment
        // preIndex
        
        // If the current subarry has only one
        // element, no need to recur or
        // preIndex > size after incrementing
        if (preIndex >= size || size == 1) { // if list size is one
            return root;
        }
        
        // Search the next element of pre[] in post[]
        int counter = 0;
        while (searchIndex < size - 1) {
            if (pre[preIndex].equals(post[searchIndex])) {
                counter += 1;
                int newSize = searchIndex - postIndex + 1;
                AttackNode newPre[] = new AttackNode[newSize];
                AttackNode newPost[] = new AttackNode[newSize];
                
                for (int index = 0; index < newPre.length; index++) {
                    newPre[index] = pre[preIndex + index];
                }
                
                for (int index = 0; index < newPost.length; index++) {
                    newPost[index] = post[postIndex + index];
                }
                // add child here
                root.addChild(this.constructTree(newPre, newPost, newSize));
                // root.getChildren().size = counter;

                preIndex += newSize;
                postIndex += newSize;
            }
            searchIndex++;
        }
        return root;
        
    }
    
    /**
     * @param pre
     * @param post
     * @return
     */
    public AttackNode constructTreeHelper(AttackNode pre[], AttackNode post[], int size) {
        return this.constructTree(pre, post, size);
    }

    /**
     * @param node
     * @return
     */
    public int getHeight(AttackNode node) {
        if (node.getChildren().getSize() == 0) {
            return 1;
        }
        int max = Integer.MIN_VALUE;
        int height = 0;
        for (int index = 0; index < node.getChildren().getSize(); index++) {
            height = this.getHeight(node.getChildren().getList()[index]);
            max = Math.max(max, height);

        }
        
        return 1 + max;
    }

    /**
     * @return
     */
    public String levelOrder() {
        this.buf = new StringBuffer();
        this.buf.append("LevelOrder[\n");
        int height = this.getHeight(this.root);
        for (int index = 0; index < height; index++) {
            this.levelOrderHelper(this.root, index);
        }
        this.buf.append("]");
        
        return this.buf.toString();
    }
    
    /**
     * @param node
     * @param level
     * @param buf
     * @return
     */
    public void levelOrderHelper(AttackNode node, int level) {

        if (level == 0) {
            this.buf.append("   ");
            this.buf.append(node.toString());
            this.buf.append("\n");
        } else if (level > 0) {
            for (int index = 0; index < node.getChildren().getSize(); index++) {
                this.levelOrderHelper(node.getChildren().getList()[index], level - 1);
            }
        }

    }

    /**
     *
     */
    public void propogate() {
        this.root.propogate();
    }
}
