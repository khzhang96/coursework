package edu.ncsu.csc316.grocerystore2.order;

import org.junit.Assert;
import org.junit.Test;

import edu.ncsu.csc316.grocerystore2.order.Product;

/**
 * Product Test
 * 
 * @author khzhang
 *
 */
public class ProductTest {
    String brand;
    String description;

    /**
     * Test 1
     */
    @Test
    public void test() {
        this.brand = "EEKO";
        this.description = "Medical Devices";

        Product newProduct = new Product(this.brand, this.description);

        Assert.assertEquals(this.brand, newProduct.getBrand());
        Assert.assertEquals(this.description, newProduct.getDescription());
        Assert.assertEquals(1, newProduct.getFreq());

        newProduct.inc();
        this.brand = "EEK0";
        this.description = "Not Medical Devices";

        newProduct.setBrand(this.brand);
        newProduct.setDescription(this.description);

        Assert.assertEquals(this.brand, newProduct.getBrand());
        Assert.assertEquals(this.description, newProduct.getDescription());
        Assert.assertEquals(2, newProduct.getFreq());

        newProduct.dec();
        Assert.assertEquals(this.brand, newProduct.getBrand());
        Assert.assertEquals(this.description, newProduct.getDescription());
        Assert.assertEquals(1, newProduct.getFreq());

    }

}
