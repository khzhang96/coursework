/*
Author: Jonathan Stallings
Date: UNK
Purpose: Demonstrate DLM= usage

Updated by: Jonathan Duggins
Date: 2017-08-31
Purpose: Add header, update file locations
*/

data Employee;
   /*infile "S:\Documents\CURRENT COURSES\ST 445\Lectures\Lecture 5\Data\employee.csv";*/
   input FullName   $ $20. 
         City       $ $15.
         State      : $15.
         ASP        : $3.
         DOB        : $8.
         Term       : $MMDDYY10. 
         Votes      : DOLLAR8. 
		 PercVote  
         DLM = ","   /*Why won't this work?*/
   ;
RUN;

/*********************************************************/
