/**
    @file bits.h
    @author

    Header file for the bits.c component, with functions supporting
    reading and writing a file just a few bits at a time.
*/

#include <stdio.h>
#include <stdlib.h>

/** Number of bits per byte.  This isn't going to change, but it lets us give
    a good explanation instead of just the literal value, 8. */
#define BITS_PER_BYTE 8
#define FIVE 5
#define THREE 3
#define NEG1 -1
/** Buffer space for up to 8 bits that have been written by the
    application but haven't yet been written out to a file, or that
    have been read from a file but not yet used by the application.
    When this structure is initialized, zeros should be stored in both
    fields.
*/
typedef struct {
  /** Storage for up to 8 bits left over from an earlier read or waiting
      to be written in a subsequent write. */
  unsigned char bits;

  /** Number of bits currently buffered. */
  int bcount;
} BitBuffer;

/** Write the 5-bit code stored in the code parameter.  Temporarily
    store bits in the given buffer until we have 8 of them and can
    write them to the file.
    @param code to write out, a value betteen 0 and 31
    @param pointer to storage for bits that can't be written to the
    file yet.  When this function is called, this buffer may already
    contain some bits left-over from a previous write.
    @param fp file we're writing to, opened for writing in binary mode.
*/
void write5Bits( int code, BitBuffer *buffer, FILE *fp ) {

    if ( buffer->bcount > BITS_PER_BYTE ) {
        //printf("stop right there, bcount > 8");
        exit(EXIT_FAILURE);
    }
    // _ _ 1 0 0 1 1 >> empty space on left,
    //read in from the left, new stuff goes on the right
    if ( buffer->bcount == BITS_PER_BYTE ) {
        fputc((buffer->bits)&0xff, fp);
        buffer->bcount = 0;
        buffer->bits = 0;
    }
    if ( buffer->bcount == 0 ) {
        buffer->bits =  ( char ) code;
        buffer->bcount += FIVE;
    } else if ( buffer->bcount <= THREE ) {
        //buffer->bits = ( buffer->bits | (char) code >> ( 8 - ( 5 + buffer->bcount ) ) );
        buffer->bits = ( ( buffer->bits << FIVE ) & 0x00ff ) | (char) code;
        buffer->bcount += FIVE;
    } else if ( ( buffer->bcount ) > THREE ) {
        //int newBcount = buffer->bcount + 5 - 8;
        int spaceLeft = BITS_PER_BYTE - buffer->bcount; // space used on the next buffer
        buffer->bits = ( ( buffer->bits << spaceLeft ) & 0x00ff )
                     | ( (char) code >> (FIVE - spaceLeft ) );
        fputc(buffer->bits & 0xff, fp);

        //keeps ( 5 - spaceLeft ) number of bits in new buffer
        //printf("code=%02x\n",code);
        //printf("spaceLeft=%d\n", spaceLeft);
        buffer->bits = ( (char) code << ( BITS_PER_BYTE - ( FIVE - spaceLeft ) ) & 0x00ff ) >>
                       ( BITS_PER_BYTE - ( FIVE - spaceLeft ) );
        //printf("%02x\n", buffer->bits);
        buffer->bcount = FIVE - spaceLeft;
    } else if ( buffer->bcount > BITS_PER_BYTE ) {
        //printf("bcount is :%d", buffer->bcount);
        exit(EXIT_FAILURE);
    }
}

/** Just like write5Bits(), but writing 8 bits at a time.
    @param code to write out, a value betteen 0 and 255
    @param pointer to storage for bits that can't be written to the
    file yet.
    @param fp file we're writing to.
*/
void write8Bits( int code, BitBuffer *buffer, FILE *fp ) {
    if ( buffer->bcount > BITS_PER_BYTE ) {
        //printf("stop right there, bcount > 8");
        exit(EXIT_FAILURE);
    }

    int spaceLeft = BITS_PER_BYTE - buffer->bcount;
    if ( buffer->bcount == 0 ) {
        buffer->bits =  code;
        fputc(buffer->bits & 0xff, fp);
        buffer->bits = 0;
        buffer->bcount = 0;
    } else if ( buffer->bcount > 0 ) {
        //printf("%d\n", spaceLeft);
        //printf("1===%02x\n", ( buffer->bits << spaceLeft ) & 0x00ff );
        //printf("2===%02x\n", ( (char) code >> ( 5 - spaceLeft )));
        buffer->bits = ( ( buffer->bits << spaceLeft ) & 0x00ff )
                     | (  (code >> buffer->bcount) );


        fputc(buffer->bits & 0xff, fp);
        buffer->bits = ( (char) code << ( BITS_PER_BYTE - ( BITS_PER_BYTE - spaceLeft) ) & 0x00ff )
                    >> ( BITS_PER_BYTE - ( BITS_PER_BYTE - spaceLeft) );
    }
    //printf("write 8 Bits exit:  bcount= %d\t bits = %02x\n", buffer->bcount, buffer->bits);
}

/** If there are any bits buffered in buffer, write them out to the
    given file in the high-order bit positions of a byte, leaving zeros
    in the low-order bits.
    @param buffer pointer to storage for unwritten bits left over
    from a previous write.
    @param fp file these bits are to be written to, opened for writing.
*/
void flushBits( BitBuffer *buffer, FILE *fp ) {
    if ( buffer->bcount > 0 ) {
        buffer->bits = ( buffer->bits << ( BITS_PER_BYTE - buffer->bcount ) ) & 0x00ff;
        if ( buffer->bcount <= THREE ) {
            buffer->bits = buffer->bits | ( 0x1f << ( BITS_PER_BYTE - buffer->bcount - FIVE));
        }
        fputc(buffer->bits & 0xff, fp);
    }
}

/** Read and return the next 5-bit code from the given file.  The given
    buffer may contain some bits left over from the last read, and if this
    read has any left-over bits, it should leave them in that buffer.
    @param buffer pointer to storage for left-over from one read call to the
    next.
    @param fp file bits are being read from, opened for reading in binary.
    @return value of the 5-bit code read in, or -1 if we reach the
    end-of-file before getting all 5 bits.
*/
int read5Bits( BitBuffer *buffer, FILE *fp ) {
    int outByte=NEG1;

    //printf("bits=%02x\tbcount=%d\n", buffer->bits, buffer->bcount);

    if (buffer->bcount >= FIVE ) {
        // We already have enough in the buffer, do not need to read
        outByte = buffer->bits;
        outByte = (outByte>> (buffer->bcount - FIVE )) & 0x1f;
        buffer->bcount = buffer->bcount - FIVE;
    } else {
        int c = fgetc(fp);

        if (c!= NEG1) {
            unsigned int tmp = buffer->bits;
            // buffer->bits only has 8 bits.  We need 16 bits operation here

            tmp = tmp << BITS_PER_BYTE;
            // move the current to 8 bits high, since the new code is 8 bits

            tmp = tmp | c;  // combine together
            buffer->bcount = buffer->bcount + BITS_PER_BYTE;
            // combined, max is 12, min is 8

            outByte = (tmp >> (buffer->bcount - FIVE )) & 0x1f;
            // get top 5 bits and mask it with 0x1f
            buffer->bcount = buffer->bcount - FIVE;  // max is 7
            buffer->bits = tmp & 0xff;   // buffer->bit is only 8 bits. After it mask with 0xff,
            //it still may contains junk bits at high order.
            //but bcounts determin which bit counts.

        } else {
            outByte=NEG1;
        }
    }

    return outByte;
}

/** Just like read5bits() but reading 8 bits at a time.
    @param buffer storage for bits left over from one read to the next.
    @param fp file we're supposed to write to.
    @return value of the 8-bit code read in, or -1 if we can't read 8 bits.
*/
int read8Bits( BitBuffer *buffer, FILE *fp ) {
    int outByte=NEG1;

    if (buffer->bcount >= BITS_PER_BYTE ) {
        // We already have enough in the buffer, do not need to read
        outByte = buffer->bits;
        outByte = (outByte>> (buffer->bcount - BITS_PER_BYTE )) & 0x1f;
        buffer->bcount = buffer->bcount - BITS_PER_BYTE;
    } else {
        int c = fgetc(fp);

        if (c!= NEG1) {
            unsigned int tmp = buffer->bits;
            // buffer->bits only has 8 bits.
            //We need 16 bits operation here

            tmp = tmp << BITS_PER_BYTE;
            // move the current to 8 bits high,since the new code is 8 bits
            tmp = tmp | c;
            // combine together

            buffer->bcount = buffer->bcount + BITS_PER_BYTE;
            // combined, max is 15, min is 8

            outByte = (tmp >> (buffer->bcount - BITS_PER_BYTE)) & 0xff;
            // get top 5 bits and mask it with 0x1f
            buffer->bcount = buffer->bcount - BITS_PER_BYTE;  // max is 7
            buffer->bits = tmp & 0xff;

        } else {
            outByte= NEG1;
        }
    }

    return outByte;
}
