/*
Author: Jonathan Stallings
Date: UNK
Purpose: Demonstrate DLM= usage

Updated by: Jonathan Duggins
Date: 2017-08-31
Purpose: Add header, update file locations
*/

data Employee;
   infile "S:\Documents\CURRENT COURSES\ST 445\Lectures\Lecture 5\Data\employee.csv";
   input ID        : $3.    
         Name      & $QUOTE20.    
         Depart    : $8.
         DateHire  : $MMDDYY10. 
         Salary      DOLLAR8. 
         DLM = ","   /*Why won't this work?*/
   ;
RUN;

/*********************************************************/
