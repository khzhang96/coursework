#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "activity.h"

/*defult size of string*/
#define DEFAULT_SIZE 24
/*default num of spaces*/
#define DEFAULT_SPACE 50
/*define hours*/
#define HOURS 24
/*define minutes*/
#define MINUTES 60

/*struct for each schedule*/
typedef struct ScheduleTag {
    /*activities array*/
    Activity **activities;
    /*max size of array*/
    int maxSize;
    /*current size of array*/
    int currSize;
    /*counter for IDs*/
    int IDCounter;
} Schedule;

/**
This will dynamically allocate space for a new,
empty Schedule instance and return a pointer to it.
This will include creating the resizable array of
activity pointers stored within the Schedule.
It will be part of the schedule component.
@return Schedule - empty schedule pointer
*/
Schedule *createSchedule() {
    Schedule *newSchedule = (Schedule *) calloc( 1, sizeof( Schedule ) );
    newSchedule->activities = (Activity **) calloc( DEFAULT_SIZE, sizeof ( Activity) );
    newSchedule->maxSize = DEFAULT_SIZE;
    newSchedule->currSize = 0;
    return newSchedule;
}

/**
This will free all the dynamically allocated memory used by
a schedule, including the schedule object itself, its
resizable array of Activity pointers, and the Activity
instances themselves. It will be
part of the schedule component.
@param sched is the Schedule struct
*/
void freeSchedule( Schedule *sched ) {
    /*index of loop*/
    for (int i = 0; i < sched->currSize; i++ ) {
        freeActivity( sched->activities[i] );
    }
    free( sched->activities );
    free(sched);
}

/**
This will add the given activity to the given schedule,
growing the resizable array as needed and
returning true if successful. This function is responsible
for checking for scheduling conflicts;
it returns false if the given activity overlaps with an
activity the leader is already doing.
This is part of the schedule component.
@param sched schedule
@param activity act
*/
void addActivity( Schedule *sched, Activity *act ) {
    /* Resizes array if current size > Max size*/
    if ( ( sched->currSize ) > ( sched->maxSize ) ) {
        int newMaxSize = 2 * ( sched->currSize + 1 );

        /*creates a pointer to a pointer*/
        Activity **tempPointer = (Activity **)realloc( sched->activities,
                                                       newMaxSize *
                                                       sizeof( Activity ) );
        if (tempPointer) {
            sched->activities = tempPointer;
        }
        sched->maxSize = newMaxSize;
    }

    ( sched->activities )[ sched->currSize ] = act;
    ( sched->currSize )++;
}

/**
Removes an activity
@param sched schedule
@param activity act
@param index of removed activity
*/
void removeActivity( Schedule *sched, Activity *act, int index ) {
    //free(&( sched->activities[index] ) );
    //Activity *tempPointer = NULL;
    //printf("Index: %d", index);

    /*index*/
    int i;
    if ( ( index + 1 ) == sched->currSize - 1) {
        //freeActivity(&sched->activities[index]
    }

    //Activity *tempPointer = sched->activities[index];
    //freeActivity(tempPointer);

    for (i = index; i < ( sched->currSize - 1); i++ ) {
        //freeActivity(&sched->activities[i]);
        sched->activities[i] = sched->activities[i + 1];
    }

    ( sched->currSize )--;
    //freeActivity(act);

    //free(act);

}

/*
Prints the item
@param act Activity
@param spaces numspaces
*/
void printItem( Activity *act, int spaces ) {
    char fmt[DEFAULT_SPACE];
    sprintf(fmt, "%s%d%s", "%2d:%02d %2d:%02d (%03d) %-", ++spaces, "s%s\n");

    printf( fmt,
           act->startTime->hour, act->startTime->minute,
           act->endTime->hour, act->endTime->minute,
           act->ID, act->leader, act->title);
}

/*
Prints the schedule based on users needs
@param sched Schedule
@param bool function pointer to intended needed
@param arg for whatever the user needs
*/
void printSchedule( Schedule *sched, bool (*test)( Activity *, void *arg ), void *arg ) {
    if ( sched==NULL ) {
        return;
    }
    printf("\n");
    //sorts Schedule by ID number

    /*sorting algorithm*/
    for (int i = 0; i < sched->currSize; i++) {
        for (int j = 0; j < sched->currSize - i - 1; j++) {
            if ( ( sched->activities[j]->startTime->hour * MINUTES +
                   sched->activities[j]->startTime->minute ) >
                 ( sched->activities[j+1]->startTime->hour * MINUTES +
                   sched->activities[j+1]->startTime->minute) ) {
                     Activity *tempActivity;
                     tempActivity =  sched->activities[j];
                     sched->activities[j] = sched->activities[j+1];
                     sched->activities[j+1] = tempActivity;
                 } else if ( ( sched->activities[j]->startTime->hour * MINUTES +
                               sched->activities[j]->startTime->minute ) ==
                             ( sched->activities[j+1]->startTime->hour * MINUTES +
                               sched->activities[j+1]->startTime->minute) ) {
                              if ( strcmp( sched->activities[j]->leader,
                                   sched->activities[j+1]->leader) > 0 ) {
                                  Activity *tempActivity = sched->activities[j];
                                  sched->activities[j] = sched->activities[j+1];
                                  sched->activities[j+1] = tempActivity;
                              }
                          }
        }
    }

    /*sees how many spaces are needed*/
    int maxName = 0;
    for (int index = 0; index < sched->currSize; index++) {
        if (test(sched->activities[index], arg)==true) {
            if ( strlen(sched->activities[index]->leader) > maxName ) {
                maxName = strlen(sched->activities[index]->leader);
            }
        }

    }

    /*passes item to print*/
    for (int index = 0; index < sched->currSize; index++) {
        if (test(sched->activities[index], arg)==true) {
            printItem(sched->activities[index], maxName);
        }
    }

}
