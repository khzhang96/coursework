

data catapult;
input stHeight armAngle distance;
datalines;
1 2 39
2 2 49
2 2 52
2 1 47
1 1 37
3 1 41
3 1 44
1 1 32
3 2 63
1 2 39
2 1 43
3 2 61

proc print data=catapult;
run;

proc anova data=catapult;
    class stHeight armAngle distance;
    model distance = stHeight | armAngle;
run;

proc glm;
    class stHeight armAngle;
	model distance = stHeight armAngle stHeight*armAngle/solution;
	lsmeans stHeight armAngle stHeight*armAngle;
	estimate '0.3 vs 0.1' stHeight -1 0 1;
	estimate '16 vs 14' armAngle -1 0 1;
run;
