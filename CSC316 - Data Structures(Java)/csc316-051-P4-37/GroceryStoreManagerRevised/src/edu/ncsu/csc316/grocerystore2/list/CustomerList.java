/**
 *
 */
package edu.ncsu.csc316.grocerystore2.list;

import edu.ncsu.csc316.grocerystore2.customer.Customer;

/**
 * Customer List class
 *
 * @author khzhang
 *
 */
public class CustomerList {
    /**
     * Default capacity for list
     */
    public static final int DEFAULT_CAP = 200000;
    /**
     * Current max capacity of list
     */
    private int capacity = CustomerList.DEFAULT_CAP;
    /**
     * List of customers
     */
    private Customer[] customerList;
    /**
     * Current list size
     */
    private int size = 0;

    /**
     * Constructor
     */
    public CustomerList() {
        this.customerList = new Customer[this.capacity];
    }

    /**
     * Adds a customer
     *
     * @param element
     *            Customer element
     */
    public void add(Customer element) {
        this.ensureCapacity();

        this.customerList[this.size] = element;
        this.size++;
    }

    /**
     * Ensures that there is enough capacity in the array before adding Does not do
     * anything to save processing power if there is enough capacity
     */
    public void ensureCapacity() { // sets the capacity of the current list so there is no index out of bounds or
                                   // seg fault in the array
        if (this.size + 1 >= this.capacity) { // tests size of current array
            Customer[] old = this.customerList; // creates pointer for current list

            this.capacity = this.capacity * 2 + 1; // sets new capacity
            Customer[] newArray = new Customer[this.capacity]; // creates new array with larger capacity

            for (int i = 0; i < this.size; i++) {
                newArray[i] = old[i];
            }

            this.customerList = newArray;
        }
    }

    /**
     * Gets Customer
     *
     * @param index
     *            - array index
     * @return Customer object
     */
    public Customer get(int index) { // gets the index
        if (index < this.customerList.length) {
            return this.customerList[index];
        } else {
            return null;
        }
    }

    /**
     * Gets the size
     *
     * @return list size
     */
    public int getSize() { // gets the list size
        return this.size;
    }

    /**
     * Merge sort recursive method Orders individual elements to rebuild the list
     *
     * @param array
     *            array for sorting
     * @param first
     *            first index
     * @param middle
     *            middle index
     * @param last
     *            last index
     */
    private void merge(Customer firstArray[], Customer secondArray[], int first, int middle, int last) { // merges 2
                                                                                                       // elements
                                                                                                       // into sorted
                                                                                                       // order
        
        // Initial indexes of first and second subarrays
        int index1; // indexes for copying
        int index2; // indexes for copying
        
        // Initial index of merged subarry array
        int completeIndex = first; // index for copying to into the finished array to return
        
        for (completeIndex = first, index1 = 0, index2 = 0; index1 < firstArray.length
                && index2 < secondArray.length; completeIndex++) {
            if (firstArray[index1].getId().compareTo(secondArray[index2].getId()) > 0) { // compares the id, and makes
                this.customerList[completeIndex] = secondArray[index2++]; // copies second array element into complete
                                                                          // array
            } else {
                this.customerList[completeIndex] = firstArray[index1++]; // copies first array element into complete
                                                                         // array
            }
        }
        
        for (int i = index1; i < firstArray.length; i++, completeIndex++) { // copies any remaining elements into the
                                                                            // complete array
            this.customerList[completeIndex] = firstArray[i];
        }
        
        for (int i = index2; i < secondArray.length; i++, completeIndex++) { // copies any remaining elements into the
                                                                             // complete array
            this.customerList[completeIndex] = secondArray[i];
        }
        
    }

    /**
     * Splits the array until each array has one element left, (first = last)
     *
     * @param first
     *            first element
     * @param last
     *            last element
     */
    private void mergeSort(int first, int last) { // helper function for merge sort. starts the splitting processes
                                                  // until array is of size 1
        if (first < last) { // checks if array is of size of 1 or greater
            // Sort first and second halves
            this.mergeSort(first, (first + last) / 2); // splits first array from first to middle
            this.mergeSort((first + last) / 2 + 1, last); // splits second array from middle + 1 to last

            // Merge the sorted halves
            
            Customer firstArray[] = new Customer[(first + last) / 2 - first + 1]; // creates the first array of specific
                                                                                // size
            Customer secondArray[] = new Customer[last - (first + last) / 2]; // creates the second array of specific
                                                                            // size
            int i = 0, j = 0;
            while (i < firstArray.length) { // creates an array of the first half of the list
                firstArray[i] = this.customerList[first + i++];
            }
            while (j < secondArray.length) { // creates an array of the second half of the list
                secondArray[j] = this.customerList[(first + last) / 2 + 1 + j++];
            }
            
            this.merge(firstArray, secondArray, first, (first + last) / 2, last); // merges new list from sorted arrays
        }
    }

    /**
     * Merge sort helper method to start the recursive sort
     */
    public void msort() { // starts the sort. can be used for whatever sort I choose, used to be a
                          // selection sort stub
        this.mergeSort(0, this.size - 1);
    }

    /**
     * Removes a customer
     *
     * @param index
     *            Array index
     * @return Customer object
     */
    public Customer remove(int index) { // removes a customer from a given index, currently useless
        Customer temp = this.customerList[index]; // creates temp customer
        for (int i = index; i < this.size; i++) { // creates a for loop from index to size
            this.customerList[i] = this.customerList[i + 1]; // assigns each element that of the one ahead of it
        }
        this.size--; // subtracts one to size

        return temp;
    }

    /**
     * Sets a customer at a particular index
     *
     * @param index
     *            array index
     * @param element
     *            Customer element
     * @return 0 for success, -1 for failiure
     */
    public int set(int index, Customer element) { // sets the customer at a specific index, currently useless
        if (index < this.customerList.length) {
            this.customerList[index] = element; // assigns index to element
            return 0; // returns 0 if successful
        } else {
            return -1; // returns -1 if index is out of bounds
        }
    }

    /**
     * Returns a string for the customer
     *
     * @return returns a string representation of the entire list
     */
    @Override
    public String toString() {
        StringBuffer newBuf = new StringBuffer(); // creates a string buffer for easy concatenation
        for (int i = 0; i < this.size; i++) {
            newBuf.append(this.toString(i)); // adds individual to string outputs into a much larger
                                             // string
        }

        return newBuf.toString(); // returns the outcome
    }

    /**
     * Returns a string at the particular index
     *
     * @param index
     *            array index returns a string of the desired index in the array
     * @return String representation of list
     */
    public String toString(int index) { // duplicates to string functionality for specific index

        return this.customerList[index].toString();

    }

}
