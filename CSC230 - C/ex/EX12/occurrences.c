// Report each command-line argument, along with the number of times it occurs.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
    int wordCount[argc];
    for (int i = 0; i < argc - 1; i++) { 
        for (int j = i + 1; j < argc; j++) {
            if (*argv[i] == *argv[j]) {
                wordCount[i]++;
                wordCount[j]++;
            }
        }
    }


    for (int i = 0; i < argc; i++) {
        printf("%s (%d)\n", argv[i], wordCount[i]);
    }
    printf("\n");

    return EXIT_SUCCESS;
}
