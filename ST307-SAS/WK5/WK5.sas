proc import out=lyme datafile='lyme.xls' dbms=xls replace;
getnames=yes;
label in_out='Inside or outside dog';
label result='Result of Lyme disease test';
run;

proc print data=lyme (obs=10) label; *use label option;
run;

data lyme2;
set lyme;
keep sex in_out result;
run;

proc print data=lyme2 (obs=5) label;
run;

*this dataset will result in the same information as lyme2;
data lyme3;
set lyme;
drop name breed age; *use drop statement instead of keep;
run;

*note the labels will be used even without the label option;
proc freq data=lyme2;
tables result; *one-way table;
tables sex*result; *two-way table: row=sex/column=result;
tables in_out*result; *two-way table: row=in_out/column=result;
tables in_out*sex*result; *three-way table: separate tables for in_out with row=sex/column=result;
run;

data gpa;
infile 'C:\Users\stat\Documents\ST307\datasets\gpa.txt' firstobs=2;
input gender$ exam1 exam2 gpa;
final = (exam1+exam2)/2;
if final GE 0 and final LT 65 then grade='F';
else if final GE 65 and final LT 75 then grade='C';
else if final GE 75 and final LT 85 then grade='B';
else if final GE 85 then grade='A';
mean_score = mean(exam1,exam2);
run;

proc print data=gpa;
run;

proc plot data=gpa;
plot exam2*exam1;
run;

proc sort data=gpa;
by gender;
run;

proc plot data=gpa;
by gender;
plot exam2*exam1;
run;

proc plot data=gpa;
plot exam2*exam1=gender;
plot exam2*exam1='*';
plot final*(exam1 exam2);
run;

*symbol1 value=dot color=black;
*symbol2 value=circle color=black;
proc gplot data=gpa;
plot exam2*exam1;
plot exam2*exam1=gender;
run;

quit;


***ADDITIONAL PROBLEM;
data titanic;
infile 'C:\Users\stat\Documents\ST307\datasets\titanic.txt' firstobs=2;
input status$ fate$ count;
run;

proc freq data=titanic;
weight count;
tables fate*status;
run;

quit;
