/* @author: Kevin Zhang
@date: 2016-9-20

Read in President V1 & V2 text files and use formats and labels to sort the data.
*/



libname HW2 '\\tsclient\D\ST445\HW2\';

filename Pres1 '\\tsclient\D\ST445\HW2\President_V1.txt';
filename Pres2 '\\tsclient\D\ST445\HW2\President_V2.txt';


data HW2.Pres1;

	infile Pres1 dlm = '09'x;
	input FullName $ City $ State $ ASP DOB Term Votes PrecVote;
	
	label ASP = "Age starting presidency"
		  DOB = "Date of Birth"
		  Perc = "Percent of Vote";
run;


data HW2.Pres2;
infile Pres2; /*previous data is tab limitited, not space delimited*/
input input FullName $ City $ State $ ASP DOB Term Votes PrecVote;
run;

data HW2.Pres2;
infile Pres2;
input @1-24     FullName $
	  @25-48    City     $
	  @49-72    State    $
	  @73-74    ASP
	  @81-96    DOB      mmddyy10.
	  @97-104   Term     
	  @105-120  Votes
	  @121      PrecVote;

	  label ASP = "Age starting presidency"
		  DOB = "Date of Birth"
		  Perc = "Percent of Vote";
run; 


proc format fmtlibrary=HW2;
value $region "Colorado", "Wyoming", "Montana", "Idaho", "Washington", "Oregon", "Utah", "nevada", "California", "Alaska", "Hawaii" = "West"
			  "Texas", "Oklahoma", "New Mexico", "Arizona" = "Southwest"
			  "Ohio", "Indiana", "Michigan", "Illinois", "Missouri", "Wisconsin", "Minnesota", "Iowa", "Kansas", "Nebraska", "South Dakota", "North Dakota" = "Midwest"
			  "Maine", "Massachusetts", "Rhode Island", "Connecticut", "New Hampshire", "Vermont", "New York", "Pennsylvania", "New Jersey", "Delaware", "Maryland" = "Northeast"
			  "West Virginia", "Virginia", "Kentucky", "Tennessee", "North Carolina", "South Carolina", "Georgia", "Alabama", "Mississippi", "Arkansas", "Louisiana", "Florida" = "Southeast"
value century 1700-1799 = "1700s"
			  1800-1899 = "1800s"
			  1900-1999 = "1900s"
			  2000-2099 = "2000s"
run;

proc contents data=HW2._all_nods;
title "Contents of HW2 Library";
footnote "Prepared by Kevin Zhang";
options nonumber nodate;

proc print data=HW2.Pres1;
title "US Presidents";
title2 "Information";
footnote;

options nodate number pageno=1;
run;

proc print data=HW2.Pres2;
title "US Presidents";
title2 "Information";
footnote;
run;



