/*
Author: Kevin Zhang
Date: 2016-09-13
Purpose: 
Read in bankdata.txt using Simple List Input, Modified List Input, Column Input, and Formatted Input.
Read in bankdata.csv using Simple List Input and Modified List Input.

Print out all contents in HW1, and print out bankdata.txt and bankdata.csv in Modifed List Input.
*/
/*creates library*/
libname HW1 '\\tsclient\D\ST445\HW1\';

/*creates filerefs*/
filename Bank '\\tsclient\D\ST445\HW1\bankdata.txt';
filename BankCSV '\\tsclient\D\ST445\HW1\bankdata.csv';

/*reads in bankdata.txt using SLI*/
data HW1.Bank_SLI;
   infile Bank;
   input FNAME $ LNAME $ ACCTNUM $ BALANCE RATE;
run;

/*reads in bankdata.text using MLI*/
data HW1.Bank_MLI;
   infile Bank;
   input NAME &$ ACCTNUM $ BALANCE RATE;
run;

/*reads in bankdata.text using CI*/
data HW1.Bank_CI;
   infile Bank;
   input NAME    $ 1-16
         ACCTNUM $ 17-21
		 BALANCE   23-27
		 RATE      28-32;
run; 

/*reads in bankdata.text using FI*/
data HW1.Bank_FI;
   infile Bank;
   input @1  NAME    $16.
         @17 ACCTNUM $5.
		 @23 BALANCE 5.0
		 @29 RATE    4.2;
run;

/*reads in bankdata.csv using SLI*/
data HW1.Bank_csv_SLI;
   length NAME $ 20;
   infile BankCSV dsd;
   input NAME $ ACCTNUM $ BALANCE RATE;
run;

/*reads in bankdata.csv using MLI*/
data HW1.Bank_csv_MLI;
   infile BankCSV dsd;
   input NAME &$ 20.  ACCTNUM $ BALANCE RATE;
run;

/*prints out all contents of HW1 Library
sets title and footnote
removes page number and date
*/
proc contents data=HW1._all_ nods;
title "Contents of HW1 Library";
footnote "Prepared by Kevin Zhang";
options nonumber nodate;
run;

/*prints out data in HW1.Bank_MLI
sets title and subtitle
removes footnote
removes date
restores page number and sets it to 1
*/
proc print data = HW1.bank_MLI;
title1 "Printout of Banking Data using Modified List Input";
title2 "bankdata.txt";
footnote;
options nodate number pageno=1;

run;

/*prints out data in HW1.Bank_CSV_MLI
sets title and subtitle
removes date
restores page number and sets it to 2
*/
proc print data=HW1.bank_CSV_MLI;
title1 "Printout of Banking Data using Modified List Input";
title2 "bankdata.csv";

options nodate number pageno=2;
run;
