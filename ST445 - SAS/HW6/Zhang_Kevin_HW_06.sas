/*
Author: Kevin Zhang
Date: 2016-10-31
Purpose: 
Uses arrays and loops
*/


libname HW6 '\\tsclient\D\ST445\HW6\';
filename FISHDATA '\\tsclient\D\ST445\HW6\fish.sas7bdat';

data HW6.DESIGN;

do Block = 1 to 5;
	do Treatment = 'A', 'B', 'C', 'D';
		do Replicate = 1 to 3;
			output;
		end;
	end;
end;

run;

data HW6.DESIGN_RAND_0;

do Block = 1 to 5;
	do Treatment = 'A', 'B', 'C', 'D';
		do Replicate = 1 to 3;
			rannum = rand('Uniform');
			output;
		end;
	end;
end;

run;
data HW6.RCBD_0;
	set HW6.DESIGN_RAND_0;
run;
proc sort data = HW6.RCBD_0;
	by rannum;
run;

/*
The order and the random numbers are different because the seen of rand is different. If you use the same seed for the pseudorandom numbers, the order will be the same
*/

data HW6.DESIGN_RAND_SEED;
call streaminit(12345);
do Block = 1 to 5;
	do Treatment = 'A', 'B', 'C', 'D';
		do Replicate = 1 to 3;
			rannum = rand('Uniform');
			output;
		end;
	end;
end;

run;
data HW6.RCBD_SEED;
	set HW6.DESIGN_RAND_SEED;
run;

proc sort data = HW6.RCBD_SEED;
	by rannum;
run;

/*
The streaminit command sets a consistent random number seed or the rand function. As a result, everytime you open sas, the stream of pseudorandom numbers will be identical
*/

/*----------section 2----------*/

data HW6.FISH;
	set '\\tsclient\D\ST445\HW6\fish.sas7bdat';
	drop lat1 lat2 lat3 long1 long2 long3;
	
run;

proc sort data = HW6.FISH;
	by lt dam; 
run;



proc means data = HW6.FISH mean median;
	by lt dam;
	ods output summary = HW6.statz(drop = vname: label_:);
run;



data HW6.ALL;
	options pagesize = 99;
	**linesize = 99;

	merge HW6.FISH HW6.statz;
	by lt dam;
	
	array hgdiffmean[120] hgdiffmean1 - hgdiffmean120;
	array hgdiffmedian[120] hgdiffmedian1 - hgdiffmedian120;

	array ndiffmean[120] ndiffmean1 - ndiffmean120;
	array ndiffmedian[120] ndiffmedian1 - ndiffmedian120;

	array elvdiffmean[120] elvdiffmean1 - elvdiffmean120;
	array elvdiffmedian[120] elvdiffmedian1 - elvdiffmedian120;

	array sadiffmean[120] sadiffmean1 - sadiffmean120;
	array sadiffmedian[120] sadiffmedian1 - sadiffmedian120;

	array zdiffmean[120] zdiffmean1 - zdiffmean120;
	array zdiffmedian[120] zdiffmedian1 - zdiffmedian120;

	array ltdiffmean[120] zdiffmedian1 - zdiffmedian120;
	array ltdiffmedian[120] ltdiffmedian1 - ltdiffmedian120;

	array stdiffmean[120] stdiffmean1 - stdiffmean120;
	array stdiffmedian[120] stdiffmedian1 - stdiffmedian120;

	array dadiffmean[120] dadiffmean1 - dadiffmean120;
	array dadiffmedian[120] dadiffmedian1 - dadiffmedian120;

	array rfdiffmean[120] rfdiffmean1 - rfdiffmean120;
	array rfdiffmedian[120] rfdiffmedian1 - rfdiffmedian120;

	array frdiffmean[120] frdiffmean1 - frdiffmean120;
	array frdiffmedian[120] frdiffmedian1 - frdiffmedian120;


	hg_diff_mean = hg_Mean - hg;
	hg_pct_diff_mean = hg_diff_mean/hg_mean;
	hg_diff_median = hg_Median - hg;
	hg_pct_diff_median = hg_diff_median/hg_median;

	n_diff_mean = n_Mean - n;
	n_diff_mean = n_Median - n;
	n_pct_diff_mean = n_diff_mean/n_mean;
	n_pct_diff_mean = n_diff_median/n_median;

	elv_diff_mean = elv_Mean - elv;
	elv_diff_mean = elv_Median - elv;
	elv_pct_diff_mean = elv_diff_mean/elv_mean;
	elv_pct_diff_mean = elv_diff_median/elv_median;

	sa_diff_mean = sa_Mean - sa;
	sa_diff_mean = sa_Median - sa;
	sa_pct_diff_mean = sa_diff_mean/sa_mean;
	sa_pct_diff_mean = sa_diff_median/sa_median;

	z_diff_mean = z_Mean - z;
	z_diff_mean = z_Median - z;
	z_pct_diff_mean = z_diff_mean/z_mean;
	z_pct_diff_mean = z_diff_median/z_median;

	st_diff_mean = st_Mean - st;
	st_diff_mean = st_Median - st;
	st_pct_diff_mean = st_diff_mean/st_mean;
	st_pct_diff_mean = st_diff_median/st_median;

	da_diff_mean = da_Mean - da;
	da_diff_mean = da_Median - da;
	da_pct_diff_mean = da_diff_mean/da_mean;
	da_pct_diff_mean = da_diff_median/da_median;

	rf_diff_mean = rf_Mean - rf;
	rf_diff_mean = rf_Median - rf;
	rf_pct_diff_mean = rf_diff_mean/z_mean;
	rf_pct_diff_mean = rf_diff_median/z_median;

	fr_diff_mean = fr_Mean - fr;
	fr_diff_mean = fr_Median - fr;
	fr_pct_diff_mean = fr_diff_mean/fr_mean;
	fr_pct_diff_mean = fr_diff_median/fr_median;
	**drop obs;
run;

proc print data = HW6.ALL;
run;





