proc import out=info datafile= 'bodydata.xls' dbms=xls replace;
getnames=yes;
run;

proc sort data=info;
by gender;
run;

proc univariate data=info plot;
var weight;
histogram weight;
by gender;
title 'Histogram of Weight by Gender'

run;

proc univariate data=info plot;
var height;
histogram height;
by gender;
title 'Histogram of Height by Gender'
run;

proc univariate data=info plot;
var weight;
histogram weight;
title 'Histogram of Weight'

run;

proc univariate data=info plot;
var height;
histogram height;
title 'Histogram of Height'
run;
